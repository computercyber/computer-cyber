-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 20, 2020 at 08:39 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cl_cc`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `name_admin` varchar(25) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile` varchar(128) DEFAULT NULL,
  `role_id` smallint(3) DEFAULT NULL,
  `is_active` smallint(1) DEFAULT NULL,
  `date_created` bigint(20) DEFAULT NULL,
  `update_password_at` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `name_admin`, `email`, `password`, `profile`, `role_id`, `is_active`, `date_created`, `update_password_at`) VALUES
(1, 'Super Admin', 'superadmin@gmail.com', '$2y$10$UltMtmjqA0YX1E1/frF4Y.uiF2PLcWctH43dZWsWpf3tP44AKgi2K', NULL, 99, 1, 1580113792, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id_agenda` int(11) NOT NULL,
  `nama_agenda` varchar(200) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `lokasi` varchar(200) NOT NULL,
  `undangan` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`id_agenda`, `nama_agenda`, `tanggal_mulai`, `tanggal_selesai`, `lokasi`, `undangan`, `status`, `keterangan`) VALUES
(5, 'Musyawarah Besar Computer Cyber 2018', '2018-10-20', '2018-10-20', 'Jl. Ahmad Yani Km 6 Kodim 0315', 'Semua Anggota Aktif dan Purna', 'tunda', ''),
(6, 'Pameran FestX Fakultas Teknik', '2018-11-03', '2018-11-04', 'Gedung Daerah Kepulauan Riau', 'Umum', 'telah usai', ''),
(7, 'Pameran Enterpreneur Expo 2018 Fakultas Ekonomi', '2018-11-23', '2018-11-25', 'Lapangan Pamedan Ahmad Yani', 'Umum', 'tunda', ''),
(8, 'Pengumuman Anggota Terpilih Computer Cyber 2019', '2019-10-03', '2019-10-03', '-', '-', 'telah usai', 'Calon anggota dapat mengakses pengumuman di <a>http://www.register.computer-cyber.org/announcement</a>'),
(9, 'Rapat Pembahasan Penyambutan Anggota Baru Computer Cyber 2019', '2019-10-29', '2019-10-29', 'Terminal Bintan Center', 'Semua Anggota Aktif Tahun 2018/2019', 'telah usai', ''),
(11, 'sa', '2020-01-01', '2020-01-06', 'ds', 'sad', 'telah usai', 'sad'),
(12, 'LIburan', '2019-12-25', '2020-02-28', 'rumah', 'Semua Anggota Aktif Tahun 2018/2019', 'telah usai', ''),
(13, '1', '2020-01-14', '2019-12-31', 'rumah', 'asd', 'telah usai', 'asd'),
(17, 'Liburan bareng', '2020-01-29', '2020-02-26', 'Rumah masing masing', 'Semua Anggota Aktif Tahun 2018/2019', 'telah usai', 'Hahahaha'),
(18, 'ad', '2020-01-29', '2020-01-08', 'asd', 'asd', 'telah usai', 'asd'),
(19, 'dsdf', '2020-01-08', '2020-01-01', 'sdf', 'sdf', 'telah usai', 'sdf'),
(20, 'ad', '2020-01-22', '2020-01-15', 'd', 'ad', 'telah usai', 'ads');

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` bigint(20) NOT NULL,
  `nama_anggota` varchar(255) NOT NULL,
  `nim` varchar(25) NOT NULL,
  `tahun_angkatan_anggota` int(4) DEFAULT NULL,
  `tahun_masuk_anggota` int(4) DEFAULT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin_anggota` enum('pria','wanita') DEFAULT NULL,
  `jurusan` varchar(255) NOT NULL,
  `email_anggota` varchar(255) NOT NULL,
  `alamat_anggota` text NOT NULL,
  `no_hp` varchar(14) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `status_anggota` varchar(128) DEFAULT NULL COMMENT 'anggota aktif, purna, atau berhenti',
  `id_divisi` int(11) NOT NULL,
  `id_jabatan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `nama_anggota`, `nim`, `tahun_angkatan_anggota`, `tahun_masuk_anggota`, `tanggal_lahir`, `jenis_kelamin_anggota`, `jurusan`, `email_anggota`, `alamat_anggota`, `no_hp`, `gambar`, `status_anggota`, `id_divisi`, `id_jabatan`) VALUES
(45, 'Dimas Nugroho Putro', '170155201005', NULL, 2017, '1999-11-28', 'pria', '1', 'dimasnugroho673@gmail.com', 'Mantrust Km. 18 Topsel', '082285592029', 'dimas2.jpg', 'aktif', 2, 12),
(46, 'Ika Putri Yuniati', '213311423', NULL, 2017, '2020-01-22', 'wanita', '3', 'dadang4@gmail.com', 'dsf', '214', 'dimas2.jpg', 'aktif', 5, 8),
(47, 'Sulthan SHP', '170155201013', NULL, 2017, '2020-01-26', 'pria', '1', 'sulthan.shp.13@gmail.com', 'wqeq', '2147483647', '400x400.png', 'purna', 1, 11),
(51, 'Zayus Rifan Zafarani', '170155201009', NULL, 2017, '2020-01-22', 'pria', '1', 'zrz@gmail.com', 'ew', '21231', '400x4001.png', 'aktif', 1, 8),
(52, 'Arizal Akbar', '170155201035', NULL, 2017, '2020-01-14', 'pria', '1', 'arizal@gmail.com', 'Kawal Kec. Gunung Kijang', '0822843422', '400x40010.png', 'purna', 2, NULL),
(53, 'Muhammad Zaini', '170155201008', NULL, 2017, '2020-03-17', 'pria', '1', 'zaini@gmail.com', 'sdaasda', '08228574623', '400x40011.png', 'aktif', 2, 14),
(54, 'Sahrul Ramadhan Hakim', '1701552010076', NULL, 2017, '2020-03-17', 'pria', '1', 'sahrul@gmail.com', 'asdas', '2312413', '400x40012.png', 'aktif', 3, 9),
(55, 'Fernando Mardi Nurzaman', '1701552010077', NULL, 2017, '2020-03-09', 'pria', '1', 'fernando@gmail.com', 'ewrwer', '32423523', '400x40013.png', 'aktif', 4, 10);

-- --------------------------------------------------------

--
-- Table structure for table `anggota_diterima`
--

CREATE TABLE `anggota_diterima` (
  `id_anggota` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nim` varchar(13) NOT NULL,
  `divisi` enum('Programming','Web Programming','Robotik','Networking','Multimedia') NOT NULL,
  `status` enum('diterima','ditolak') NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota_diterima`
--

INSERT INTO `anggota_diterima` (`id_anggota`, `nama`, `nim`, `divisi`, `status`, `date_created`) VALUES
(6, 'Lukman Hamdani ', '180120201008', 'Robotik', 'diterima', '2019-10-02 12:01:02'),
(7, 'Dicky Irianto', '180155201004', 'Programming', 'diterima', '2019-10-02 12:02:24'),
(8, 'Al Fikri', '180120201018', 'Robotik', 'diterima', '2019-10-02 12:03:23'),
(9, 'Muhammad Abyan Fadillah', '180120201022', 'Robotik', 'diterima', '2019-10-02 12:04:22'),
(10, 'Abdillah Husaini', '180120201014', 'Programming', 'diterima', '2019-10-02 12:05:31'),
(12, 'Novriandi Ramadan', '190155201064', 'Multimedia', 'diterima', '2019-10-02 12:40:32'),
(13, 'ASEP SAIFUL MIFTAH', '19015520170', 'Web Programming', 'diterima', '2019-10-02 12:42:05'),
(14, 'Rendriyan Oktaviadi Saputra', '190155201048', 'Web Programming', 'diterima', '2019-10-02 12:44:35'),
(16, 'Manahan Lubis', '190155201006', 'Web Programming', 'diterima', '2019-10-02 12:46:44'),
(17, 'Grace Novita Priskila', '190155201046', 'Networking', 'diterima', '2019-10-02 12:47:57'),
(18, 'Mohammad Ramadhoni', '190155201028', 'Web Programming', 'diterima', '2019-10-02 12:49:20'),
(19, 'Muhammad Hidayad', '190155201068', 'Multimedia', 'diterima', '2019-10-02 12:50:17'),
(20, 'Ramadhan taufiq', '190155201054', 'Web Programming', 'diterima', '2019-10-02 12:51:09'),
(21, 'Muhammad Rizky Fathur Rahman', '190155201049', 'Web Programming', 'diterima', '2019-10-02 12:52:01'),
(22, 'Silvia Erika Candra', '190155201040', 'Programming', 'diterima', '2019-10-02 12:53:05'),
(24, 'Thaariq Satrio Wibisono', '190155201027', 'Programming', 'diterima', '2019-10-02 12:54:50'),
(25, 'Rifandy Karunia Ilahi', '190155201067', 'Programming', 'diterima', '2019-10-02 12:55:35'),
(26, 'Muhammad Yazhid Taufiqullah', '190155201019', 'Programming', 'diterima', '2019-10-02 12:56:21'),
(27, ' Danu Prasetio', '190155201071', 'Multimedia', 'diterima', '2019-10-02 12:57:21'),
(28, 'Tinito Herdijo', '190155201025', 'Multimedia', 'diterima', '2019-10-02 12:58:16'),
(29, 'Muhammad Nazrul Hery', '190155201063', 'Programming', 'diterima', '2019-10-02 12:58:58'),
(30, 'Rezki Juliando Putra', '190155201012', 'Programming', 'diterima', '2019-10-02 12:59:44'),
(31, 'Aldi Alfa Jeremy Nainggolan Parhusip', '1901552010', 'Multimedia', 'diterima', '2019-10-02 13:01:33'),
(32, 'Triandi', '190155201003', 'Web Programming', 'diterima', '2019-10-02 13:02:17'),
(34, 'MOHAMMAD KAHFI', '190155201047', 'Programming', 'diterima', '2019-10-02 13:04:17'),
(35, 'Annisa Ramadani', '190155201010', 'Networking', 'diterima', '2019-10-02 13:05:01'),
(36, 'JULITINUS ZEGA', '190155201001', 'Programming', 'diterima', '2019-10-02 13:05:57'),
(37, 'Annisa fadillah fani ', '190155201056', 'Web Programming', 'diterima', '2019-10-02 13:06:37'),
(38, 'Mohd.Raul Gusti Ananda', '190155201053', 'Programming', 'diterima', '2019-10-02 13:07:20'),
(39, 'Raken Rizki Eka Putra', '190155201034', 'Programming', 'diterima', '2019-10-02 13:08:08'),
(40, 'Maria Cinta agustina wulandari odang', '190155201004', 'Multimedia', 'diterima', '2019-10-02 13:08:48'),
(41, 'Natanael Gea', '190155201020', 'Web Programming', 'diterima', '2019-10-02 13:09:40'),
(42, 'Aullia Three Bintan', '190155201059', 'Multimedia', 'diterima', '2019-10-02 13:10:24'),
(45, 'Ahmad Haris Zulfikar', '190155201033', 'Programming', 'diterima', '2019-10-02 13:16:18'),
(46, 'Said afrizal ', '180155201019', 'Networking', 'diterima', '2019-10-02 14:06:14'),
(47, 'Doni Muhamad Fachry', '190155201062', 'Programming', 'diterima', '2019-10-02 14:08:41'),
(48, 'LUCKY PRADANA', '190120201043', 'Programming', 'diterima', '2019-10-02 14:09:24'),
(49, 'Patricia Angelina Tampubolon', '190155201038', 'Multimedia', 'diterima', '2019-10-02 14:10:09'),
(50, 'Ganda Bagus Wibisono', '170155201039', 'Programming', 'diterima', '2019-10-02 14:11:29'),
(51, 'Hutami Sukma Hayati', '180384202008', 'Multimedia', 'diterima', '2019-10-02 14:12:11'),
(52, 'Johan Jeques Junior', '190120201055', 'Robotik', 'diterima', '2019-10-02 14:12:49'),
(53, 'Yusi Yosep', '190120201012', 'Robotik', 'diterima', '2019-10-02 14:13:25'),
(54, 'Surahman', '190120201010', 'Robotik', 'diterima', '2019-10-02 14:14:23'),
(55, 'Rizky Fajariansyah', '190155201057', 'Web Programming', 'diterima', '2019-10-02 14:15:28'),
(56, 'Riani Fitri Ibnul malik', '1901552010', 'Web Programming', 'diterima', '2019-10-02 14:16:11'),
(57, 'Dedi Novrianto Ramadhan', '190155201052', 'Multimedia', 'diterima', '2019-10-02 14:16:50'),
(58, 'Reynaldi', '190155201041', 'Web Programming', 'diterima', '2019-10-02 14:17:33'),
(59, 'Gema Suryanda', '190155201030', 'Programming', 'diterima', '2019-10-02 14:18:13'),
(60, 'Puja Lestari Sitopu', '170563201050', 'Multimedia', 'diterima', '2019-10-02 14:19:48'),
(61, 'ANNISA FITRI', '170563201045', 'Multimedia', 'diterima', '2019-10-02 14:20:44'),
(62, 'BELA SAPITRI', '190155201035', 'Web Programming', 'diterima', '2019-10-02 14:21:24'),
(63, 'Nurul Septianti', '170563201053', 'Web Programming', 'diterima', '2019-10-02 14:22:05'),
(64, 'Yhoga Dewa Nata', '1901552023', 'Programming', 'diterima', '2019-10-02 14:23:34'),
(65, 'Singgih Prasetyo', '190155201032', 'Programming', 'diterima', '2019-10-02 14:24:44'),
(66, 'Rofiyan Rozikin', '170155201029', 'Networking', 'diterima', '2019-10-02 14:25:33'),
(67, 'BAYU MAULANA', '190155201018', 'Web Programming', 'diterima', '2019-10-02 14:26:19'),
(68, 'BUDI SEJATI', '190120201039', 'Multimedia', 'diterima', '2019-10-02 14:27:05'),
(69, 'Fatimah Nur Azzahra', '190155201044', 'Multimedia', 'diterima', '2019-10-02 14:27:45'),
(70, 'Aria Kurniati', '190155201045', 'Web Programming', 'diterima', '2019-10-02 14:28:25'),
(71, 'FARID KHAIRUDDIN', '190120201049', 'Robotik', 'diterima', '2019-10-02 14:29:03'),
(72, 'Indra Kurniawan', '190155201051', 'Web Programming', 'diterima', '2019-10-02 14:29:40'),
(73, 'Nadia', '', 'Multimedia', 'diterima', '2019-10-02 14:30:10'),
(74, 'Nuriani', '', 'Multimedia', 'diterima', '2019-10-02 14:30:41'),
(75, 'Umi Nurhanifah', '', 'Programming', 'diterima', '2019-10-02 14:31:09'),
(76, 'Agung Ayub Ramadhan', '', 'Robotik', 'diterima', '2019-10-02 14:31:36'),
(77, 'Efni Gunawan', '190120201014', 'Networking', 'diterima', '2019-10-02 15:08:17');

-- --------------------------------------------------------

--
-- Table structure for table `anggota_karya`
--

CREATE TABLE `anggota_karya` (
  `id_anggota_karya` int(11) NOT NULL,
  `id_anggota` int(11) DEFAULT NULL,
  `id_karya` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota_karya`
--

INSERT INTO `anggota_karya` (`id_anggota_karya`, `id_anggota`, `id_karya`, `date_created`) VALUES
(1, 45, 11, '2020-03-24 16:06:07'),
(2, 47, 11, '2020-03-24 16:06:07'),
(3, 52, 11, '2020-03-24 16:06:23');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `gambar_berita` varchar(255) DEFAULT NULL,
  `dokumen1` varchar(255) DEFAULT NULL,
  `dokumen2` varchar(255) DEFAULT NULL,
  `dokumen3` varchar(255) DEFAULT NULL,
  `tanggal` bigint(20) DEFAULT NULL,
  `update_berita` bigint(20) DEFAULT NULL,
  `user_update_berita` int(11) DEFAULT NULL,
  `status_berita` varchar(20) NOT NULL,
  `jenis_berita` varchar(20) NOT NULL,
  `visitor` bigint(20) DEFAULT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `id_user`, `judul`, `url`, `isi`, `gambar`, `gambar_berita`, `dokumen1`, `dokumen2`, `dokumen3`, `tanggal`, `update_berita`, `user_update_berita`, `status_berita`, `jenis_berita`, `visitor`, `log_time`) VALUES
(12, 25, 'timetobreak', NULL, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>dicoba dulu.</p>\r\n</body>\r\n</html>', '6luas_segitiga4.png', NULL, NULL, NULL, NULL, 1583154772, NULL, NULL, 'Draft', 'Berita', NULL, '2020-03-02 12:40:14'),
(13, 25, 'hello', NULL, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>DOMPAK &ndash; Pelaksanaan</strong> Pemilihan Kepala Daerah (Pilkada) tahun 2020 mendatang, Pemerintah Provinsi Kepri mengalokasikan anggaran senilai Rp 130 miliar. Hal ini disampaikan Sekretaris Daerah Provinsi Kepri, H TS Arif Fadillah di Tanjungpinang, kemarin.</p>\r\n<p>&ldquo;Untuk pelaksanaan Pilkada sudah kita masukan anggaran sekitar Rp 130 miliar,&rdquo; ujar Arif kepada wartawan.</p>\r\n<p>Dikatakannya, anggaran tersebut meliputi pelaksanaan Pilkada mulai dari di Komisi Pemilihan Umum (KPU), Badan Pengawas Pemilu (Bawaslu) serta pengamanan Pilkada.</p>\r\n<p>&ldquo;Kita sudah beberapa kali melaksanakan rapat dengan teman-teman di KPU dan Bawaslu Kepri. Terjadi pengurangan anggaran dari usulan awal,&rdquo; tegas Sekda.</p>\r\n<p>&ldquo;Telah disepakati anggarannya. Untuk rinciannya saya lupa, tetapi intinya yang paling penting alokasi anggaran untuk Pengamanan Pilkada sudha dimasukkan di APBD Murni 2020,&rdquo; tambah Sekda kembali.</p>\r\n<p>Sebelumnya, KPU Kepri mengusulkan anggaran Rp80 miliar lebih untuk persiapan dana pemilihan kepala daerah tersebut. Pemprov Kepri sempat mempertimbangkan penawaran tersebut karena dinilai terlalu berlebihan.</p>\r\n<p>Anggota DPRD Provinsi Kepri Rudy Chua, mengatakan, kebutuhan Pilkada belum dibahas bersama DPRD, meski demikian mungkin sudah dibahas bersama pemerintah melalui OPD terkait.</p>\r\n<p>Politisi Hanura ini menuturkan, jika tahapan Pilkada dimulai September 2019, harusnya di APBD 2019 ini sebagian alokasi sudah dianggarkan sesuai kebutuhan.</p>\r\n<p>&rdquo;Artinya bila baru diajukan KPU, maka pembahasan paling cepat pada APBD Perubahan atau paling lama di APBD murni 2020,&rdquo; ujar Rudy kemarin.</p>\r\n<p>Anggota DPRD Kepri lainnya, Iskandarsyah menilai KPU harusnya mengajukan anggaran Pilkada 2020 di tahun 2018 sehingga disahkan di APBD 2019. <br /> &rdquo;Meski demikian bisa dibahas kembali di APBD Perubahan paling cepat dan di Murni 2020 nantinya,&rdquo; kata Iskandarsyah.</p>\r\n<p>Dikatakan, KPU sebagai pihak penyelanggara harusnya sudah melaporkan dan mengabarkan kepada Pemprov sebelum 2019 ini.</p>\r\n<p>&rdquo;Dana yang disiapkan juga tidak sedikit. Dan yang disiapkan bukan hanya KPU, tetapi juga Bawaslu dan Polda. Jadi ini persoalan serius yang harus dibahas segera dicarikan cara agar dana Pilkada bisa disiapkan,&rdquo; kata Iskandarsyah. <strong>(ais)</strong></p>\r\n</body>\r\n</html>', 'cosmic-cuttlefish_wallpaper2.jpg', NULL, NULL, NULL, NULL, 1583154772, NULL, NULL, 'Publish', 'Berita', NULL, '2020-03-02 12:40:14'),
(15, 25, 'HMm', NULL, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p><strong><img src=\"https://d2zvxgfo5lha7k.cloudfront.net/original/commons/home-hero-new.jpg\" alt=\"\" width=\"459\" height=\"227\" /></strong></p>\n<p><strong>DOMPAK &ndash; Pelaksanaan</strong> Pemilihan Kepala Daerah (Pilkada) tahun 2020 mendatang, Pemerintah Provinsi Kepri mengalokasikan anggaran senilai Rp 130 miliar. Hal ini disampaikan Sekretaris Daerah Provinsi Kepri, H TS Arif Fadillah di Tanjungpinang, kemarin.</p>\n<p>&ldquo;Untuk pelaksanaan Pilkada sudah kita masukan anggaran sekitar Rp 130 miliar,&rdquo; ujar Arif kepada wartawan.</p>\n<p>Dikatakannya, anggaran tersebut meliputi pelaksanaan Pilkada mulai dari di Komisi Pemilihan Umum (KPU), Badan Pengawas Pemilu (Bawaslu) serta pengamanan Pilkada.</p>\n<p>&ldquo;Kita sudah beberapa kali melaksanakan rapat dengan teman-teman di KPU dan Bawaslu Kepri. Terjadi pengurangan anggaran dari usulan awal,&rdquo; tegas Sekda.</p>\n<p>&ldquo;Telah disepakati anggarannya. Untuk rinciannya saya lupa, tetapi intinya yang paling penting alokasi anggaran untuk Pengamanan Pilkada sudha dimasukkan di APBD Murni 2020,&rdquo; tambah Sekda kembali.</p>\n<p>Sebelumnya, KPU Kepri mengusulkan anggaran Rp80 miliar lebih untuk persiapan dana pemilihan kepala daerah tersebut. Pemprov Kepri sempat mempertimbangkan penawaran tersebut karena dinilai terlalu berlebihan.</p>\n<p>Anggota DPRD Provinsi Kepri Rudy Chua, mengatakan, kebutuhan Pilkada belum dibahas bersama DPRD, meski demikian mungkin sudah dibahas bersama pemerintah melalui OPD terkait.</p>\n<p>Politisi Hanura ini menuturkan, jika tahapan Pilkada dimulai September 2019, harusnya di APBD 2019 ini sebagian alokasi sudah dianggarkan sesuai kebutuhan.</p>\n<p>&rdquo;Artinya bila baru diajukan KPU, maka pembahasan paling cepat pada APBD Perubahan atau paling lama di APBD murni 2020,&rdquo; ujar Rudy kemarin.</p>\n<p>Anggota DPRD Kepri lainnya, Iskandarsyah menilai KPU harusnya mengajukan anggaran Pilkada 2020 di tahun 2018 sehingga disahkan di APBD 2019. <br /> &rdquo;Meski demikian bisa dibahas kembali di APBD Perubahan paling cepat dan di Murni 2020 nantinya,&rdquo; kata Iskandarsyah.</p>\n<p>Dikatakan, KPU sebagai pihak penyelanggara harusnya sudah melaporkan dan mengabarkan kepada Pemprov sebelum 2019 ini.</p>\n<p>&rdquo;Dana yang disiapkan juga tidak sedikit. Dan yang disiapkan bukan hanya KPU, tetapi juga Bawaslu dan Polda. Jadi ini persoalan serius yang harus dibahas segera dicarikan cara agar dana Pilkada bisa disiapkan,&rdquo; kata Iskandarsyah. <strong>(ais)</strong></p>\n</body>\n</html>', 'cosmic-cuttlefish_wallpaper.jpg', NULL, NULL, NULL, NULL, 1583154772, NULL, NULL, 'Publish', 'Berita', NULL, '2020-03-02 12:40:14'),
(17, 25, 'Pengumuman Anggota Terpilih Computer Cyber Study Club 2019', NULL, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p><span style=\"color: #212529; font-family: Quicksand, sans-serif; font-size: 16px; background-color: #ffffff;\">Calon anggota dapat mengakses pengumuman di <strong><a style=\"font-family: Quicksand, sans-serif; color: #212529; background-color: #ffffff; font-size: 16px;\">http://www.register.computer-cyber.org/announcement</a></strong></span></p>\n</body>\n</html>', 'CC5.png', NULL, NULL, NULL, NULL, 1583154772, NULL, NULL, 'Publish', 'Informasi', NULL, '2020-03-02 12:40:14'),
(18, 25, 'Selamat Kepada Anggota Terpilih Computer Cyber Study Club 2019', 'anggota-terpilih-computer-cyber-study-club-2019 ', '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p><span style=\"color: #212529; font-family: Quicksand, sans-serif; font-size: 16px; background-color: #ffffff;\">Calon anggota dapat mengakses pengumuman di <strong><a style=\"font-family: Quicksand, sans-serif; color: #212529; background-color: #ffffff; font-size: 16px;\">http://www.register.computer-cyber.org/announcement</a></strong></span></p>\n</body>\n</html>', 'announcement_03-10-2019.png', 'announcement_03-10-2019.png', NULL, NULL, NULL, 1551532007, 1583154772, 25, 'Publish', 'Berita', 14, '2020-03-02 12:40:14'),
(19, 21, 'Hello Dunia', 'hello-dunia', '<blockquote class=\"blockquote\"><p>sdsadas</p></blockquote><p><br></p><p><br></p>', '', NULL, NULL, NULL, NULL, 1583155611, 1584609107, 25, 'Publish', 'Artikel', NULL, '2020-03-02 13:27:38'),
(20, 25, 'Hari ini telah dibuat web kelurahan tembeling tanjung', 'Hari-ini-telah-dibuat-web-kelurahan-tembeling-tanjung', '<p>hello guys</p><p><br><img style=\"width: 338px;\" src=\"http://localhost/computer-cyber/assets/upload/image/original/artikel/cover_contoh.jpg\"></p>', '', NULL, NULL, NULL, NULL, 1584425414, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-03-17 06:10:14'),
(21, 25, 'sdfs', NULL, '<p>sdfsdf</p>', '', NULL, NULL, NULL, NULL, 1583429520, NULL, NULL, 'Publish', 'Informasi', NULL, '2020-03-19 16:49:35'),
(24, 25, 'Tes validasi', NULL, '<p>hhelo guys</p>', '', NULL, NULL, NULL, NULL, 1584671963, NULL, NULL, 'Draft', 'Informasi', NULL, '2020-03-20 02:39:23'),
(25, 25, 'asd', NULL, '<p>asd<br></p>', '', NULL, NULL, NULL, NULL, 1584672282, NULL, NULL, 'Publish', 'Informasi', NULL, '2020-03-20 02:44:42'),
(26, 25, 'asd', NULL, '<p>sad<br></p>', '', NULL, NULL, NULL, NULL, 1584672290, NULL, NULL, 'Publish', 'Informasi', NULL, '2020-03-20 02:44:50'),
(27, 25, 'tes', 'asdas', '<p>asda<br></p>', '', 'dimas2.jpg', NULL, NULL, NULL, 1585927177, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:19:37'),
(28, 25, 'tes', 'asdas', '<p>asda<br></p>', '', 'dimas3.jpg', NULL, NULL, NULL, 1585927225, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:20:25'),
(29, 25, 'tes', 'asdas', '<p>asda<br></p>', '', 'dimas4.jpg', NULL, NULL, NULL, 1585927251, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:20:51'),
(30, 25, 'Coba Tag', 'Coba-Tag', '<p>asda<br></p>', '', 'gce_bg_doni.jpg', NULL, NULL, NULL, 1585927348, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:22:28'),
(31, 25, 'tag new', 'tag-new', '<p>dsfweq<br></p>', '', 'Logo_Umrah.png', NULL, NULL, NULL, 1585927560, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:26:00'),
(32, 25, 'new tag', 'new-tag', '<p>hello guys<br></p>', '', 'Screenshot_from_2019-10-06_20-20-43.png', NULL, NULL, NULL, 1585927625, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:27:05'),
(33, 25, 'new tag', 'new-tag', '<p>hello guys<br></p>', '', 'Screenshot_from_2019-10-06_20-20-431.png', NULL, NULL, NULL, 1585927793, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:29:53'),
(34, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:9.jpg', NULL, NULL, NULL, 1585927819, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:30:19'),
(35, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:91.jpg', NULL, NULL, NULL, 1585927846, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:30:46'),
(36, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:92.jpg', NULL, NULL, NULL, 1585928052, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:34:12'),
(37, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:93.jpg', NULL, NULL, NULL, 1585928071, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:34:31'),
(38, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:94.jpg', NULL, NULL, NULL, 1585928078, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:34:38'),
(39, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:95.jpg', NULL, NULL, NULL, 1585928154, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:35:54'),
(40, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:96.jpg', NULL, NULL, NULL, 1585928186, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:36:26'),
(41, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:97.jpg', NULL, NULL, NULL, 1585928230, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:37:10'),
(42, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:98.jpg', NULL, NULL, NULL, 1585928242, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:37:22'),
(43, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:99.jpg', NULL, NULL, NULL, 1585928248, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:37:28'),
(44, 25, 'new aads', 'new-aads', '<p>asdas<br></p>', '', 'dummy16:910.jpg', NULL, NULL, NULL, 1585928364, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:39:24'),
(45, 25, 'ha,o', 'ho', '<p>dsfsd<br></p>', '', 'avatar-profile.jpg', NULL, NULL, NULL, 1585928435, NULL, NULL, 'Publish', 'Artikel', NULL, '2020-04-03 15:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `berita_anggota_diterima`
--

CREATE TABLE `berita_anggota_diterima` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `pra_konten` text NOT NULL,
  `post_konten` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita_anggota_diterima`
--

INSERT INTO `berita_anggota_diterima` (`id_berita`, `judul`, `pra_konten`, `post_konten`) VALUES
(1, 'Selamat Kepada Anggota Terpilih Computer Cyber Study Club 2019 ', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Computer Cyber Study Club adalah suatu<em> study club</em> yang ada di Fakultas Teknik UMRAH dan dibentuk oleh Mahasiswa Teknik Informatika angkatan 2014. Tujuan dari dibentuknya Computer Cyber Study Club adalah sebagai wadah mengasah kreatifitas dan skill di bidang teknologi. Computer Cyber Study Club memiliki 5 (lima) divisi, yaitu <em>Divisi Programming, Divisi Networking, Divisi Web Programming, Divisi Multimedia, dan Divisi Robotik</em>.&nbsp;</p>\r\n<p>Setiap tahunnya Computer Cyber selalu mengadakan<strong> Open Recruitment</strong> untuk mencari generasi baru yang semangat untuk menggali ilmu dan membawa perubahan untuk diri sendiri, organisasi Computer Cyber, serta Universitas Maritim Raja Ali Haji. Untuk tahun 2019 Computer Cyber juga kembali mengadakan Open Recruitment, dan setelah melewati proses <em>Interview,&nbsp;</em>dan&nbsp;<em>Seleksi internal,&nbsp;</em>maka hari ini kami secara resmi mengumumkan anggota Computer Cyber&nbsp; Study Club&nbsp;<span style=\"background-color: #ffffff; color: #626262;\">2019&nbsp;</span>yang telah diterima yaitu sebagai berikut :&nbsp;</p>\r\n</body>\r\n</html>', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Selamat kepada anggota yang telah diterima di Computer Cyber Study Club. Semoga dengan diterimanya saudara/i dapat memberikan kesempatan untuk mengembangkan skill dan keterampilannya di bidang IT. Dan untuk yang belum diterima jangan berkecil hati, karena kesempatan di tahun depan masih ada .<span style=\"background-color: #ffffff; color: #555555; font-family: \'Source Sans Pro\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 18px;\">&nbsp;</span>Jika ada pertanyaan, silakan tanya admin cc via WhatsApp (085658553529) atau email ke cc.umrah@gmail.com. Berikan inspirasi pada orang di sekelilingmu agar mampu berkarya dan bersama tingkatkan kualitas teknologi di Indonesia.</p>\r\n<p>Salam,</p>\r\n<p>Computer Cyber Study Club</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>');

-- --------------------------------------------------------

--
-- Table structure for table `calon_anggota`
--

CREATE TABLE `calon_anggota` (
  `id_peserta` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nim` varchar(13) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `jenis_kelamin` enum('pria','wanita') DEFAULT NULL,
  `tanggal_lahir` date NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `asal_sekolah` varchar(255) DEFAULT NULL,
  `pengalaman_organisasi` text,
  `alamat` text NOT NULL,
  `alasan_masuk` text NOT NULL,
  `divisi` enum('Programming','Web programming','Networking','Robotik','Multimedia') DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calon_anggota`
--

INSERT INTO `calon_anggota` (`id_peserta`, `nama`, `nim`, `email`, `no_hp`, `jenis_kelamin`, `tanggal_lahir`, `jurusan`, `gambar`, `link`, `asal_sekolah`, `pengalaman_organisasi`, `alamat`, `alasan_masuk`, `divisi`, `date_created`) VALUES
(3, 'Lukman Hamdani ', '180120201008', 'lukmanhamdan7@gmail.com', '+628126160978', 'pria', '2001-05-28', 'Teknik Elektro ', 'LRM_EXPORT_232834231072013_20190718_184059824.jpeg', 'https://drive.google.com/folderview?id=1-oiPDeTEso9lpv9z-ARGn1JZOZY6O3bX', NULL, '1. Ketua Osis MTSN 1 Midai', 'Jl. W.R SUPRATMAN INTAN KOS KOSAN NO. 19', '1. Ingin Menambah Pengalaman Berorganisasi \r\n2. Ingin menambah Teman\r\n3. Ingin Mencari dan Berbagi Ilmu terutama Di bidang Komputer', 'Robotik', '2019-09-01 18:00:12'),
(4, 'MARZUKI', '180120201011', 'marzukiali084@gmail.com', '+628238654894', 'pria', '1999-08-11', 'teknik elektro', 'IMG-20190830-WA0000.jpeg', '', NULL, 'ketua ambalan gugus depan.', 'jl.wr supratman', 'menempa diri untuk maju di era modern', 'Web programming', '2019-09-01 18:00:48'),
(5, 'Novriandi Ramadan', '190155201064', 'novriandiramdhan9955@gmail.com', '085265881598', 'pria', '2001-11-27', 'Teknik Informatika', 'IMG_20190902_010347.jpg', 'https://drive.google.com/file/d/1AaGMpJ7nV77E-gCFddrXQTAUAcASMZei/view?usp=drivesdk', NULL, 'Pernah menjadi waki rohis di SMA.\r\n', 'Pancur,kabupaten lingga', 'Alsan saya ingin bergabung di  computer cyber,karna saya ingin lebih banyak tahu tentang komputer yang pastinya,tentang perangkat lunak,programming,editing,cara membuat website,dll,dan juga saya tertarik sekali ingin bergabung di organisasi ini', 'Multimedia', '2019-09-01 20:58:02'),
(13, 'Dicky Irianto', '180155201004', 'dikipro156@gmail.com', '082391207983', 'pria', '2000-09-19', 'Teknik Informatika', '', '', NULL, '', 'Kp. Sidodadi Utara', 'Saya ingin memperdalam pengetahuan saya tentang informatika di salah satu cabang divisi tsb.', 'Programming', '2019-09-01 22:13:44'),
(14, 'Al Fikri', '180120201018', 'al.fikri3333@gmail.com', '081365667986', 'pria', '1999-08-24', 'Teknik Elektro', 'P_20181117_110804.jpg', 'https://drive.google.com/file/d/1-L7M71RvL9ViS7xwZ8wluBqdiPXDDK2C/view?usp=drivesdk', NULL, 'Kmm Al Fatih\r\nKreativitas penelitian mahasiswa\r\nUKM Ips UMRAH\r\nPanitia LKTIN', 'Jl. Ir. Sutami (Dokabu) ', 'Ingin mengasah kemampuan saya dalam bidang desain. \r\nIngin menambah relasi yang dapat membantu saya dalam memahami teknologi. \r\nIngin menambah kegiatan yang positif. ', 'Robotik', '2019-09-02 00:03:04'),
(15, 'ASEP SAIFUL MIFTAH', '19015520170', 'asepsaiful996@gmail.com', '082126562689', 'pria', '2001-06-14', 'Teknik Informatika', '', '', NULL, 'Ketua Rohis\' Ketua Kesantrian\'Wakil Ketua 1 Osis\'', 'Jl singkonh 14 km7', 'Ingin mengembangkan dan meningkatkan kualitas dan kuantitas dalam penggunaan komputer', 'Web programming', '2019-09-02 00:52:51'),
(16, 'Muhammad Reza Saputra', '190563201072', 'reza.r6751@gmail.com', '085265335426', 'pria', '2000-02-02', 'Ilmu administrasi negara', 'IMG_20190825_155207_772.jpg', '', NULL, 'Ketua marching band,ketua mpk \r\nPradana Pramuka', 'Dompak lama batu 8 atas', 'Karena saya menyukai teknologi', 'Programming', '2019-09-02 01:18:33'),
(17, 'Rendriyan Oktaviadi Saputra', '190155201048', 'rendriyan32@gmail.com', '085722798124', 'pria', '2001-10-18', 'Teknik informatika', 'IMG-20190206-WA0045.jpg', '', NULL, '', 'Tanjung pinang', 'Saya dari SMA , saya tidak tau apa-apa mengenai komputer atau program-program lain nya , tujuan saya ingin gabung ke computer cyber ingin menambah wawasan saya untuk mengenal lebih jauh tentang komputer maupun program-program nya . Saya juga ingin mengenal lebih jauh tentang web programming', 'Web programming', '2019-09-02 01:40:56'),
(18, 'Yoshi nurmansyah', '190120201026', 'yoshinurmansyah2019@gmail.com', '083183915461', 'pria', '2000-11-16', 'Teknik elektro', 'IMG_5546.jpg', 'Juara 2 futsal se kabupaten Bintan, mengikuti turnamen sepak bola soeratin 2017', NULL, 'Futsal, rohis, sepak bola', 'Topaya bt. 25', 'Karena saya penasaran dan ingin mengetahui lebih dalam tentang programming, yg mana bisa bermanfaat untuk kedepannya', 'Programming', '2019-09-02 01:42:37'),
(19, 'Muhammad Abyan Fadillah', '180120201022', 'abyanfadillah12@gmail.com', '081930610795', 'pria', '2000-10-13', 'Teknik Elektro', '', '', NULL, 'HMTE,  KPM,  LDF', 'Kelurahan kijang kota kecamatan bintan timur kabupaten bintan', 'Ingin menambah pengalaman organisasi dalam bidang teknologi', 'Robotik', '2019-09-02 01:53:55'),
(20, 'Abdillah Husaini', '180120201014', 'abdillahhusaini1906@gmail.com', '082134498510', 'pria', '2000-06-19', 'Teknik Elektro', '', '', NULL, 'KPM, LDF', 'jl.ganet bintan permata indah', 'Mau belajar pemograman', 'Programming', '2019-09-02 01:58:05'),
(21, 'Manahan Lubis', '190155201006', 'manahanlubis01@gmail.com', '+628136063340', 'pria', '2001-04-16', 'Teknik Informatika', 'cutout-1566096813.png', '', NULL, 'Divisi Bela negara osis', 'Jl. Seraya1, Air Raja,  Tanjungpinang Timur', 'Ingin ahli dalam dunia IT, Dapat menjadi Fullstack Developer, Cyber Security analyst.', 'Web programming', '2019-09-02 02:05:39'),
(23, 'ZAKI AULIA PUTRA', '190155201043', 'zakiaulia041@gmail.com', '+628527434445', 'pria', '2002-08-04', 'TEKNIK INFORMATIKA', 'IMG-20190825-WA0069.jpg', '', 'SMA NEGERI 2 KARIMUN', 'Anggota paskib', 'Jl.basuki rahmapat ,Gg tempinis v no 31', 'Ingin memperluas wawasan tentang komputer dan jaringan ', 'Programming', '2019-09-02 02:24:32'),
(24, 'Grace Novita Priskila', '190155201046', 'gnovita02@gmail.com', '+628153386366', 'wanita', '1999-11-16', 'Teknik Informatika', 'IMG_20190828_092530_618.jpg', '', 'SMK N 1 BINTAN UTARA', 'Sekretaris Osis\r\nAnggota Pramuka\r\nAnggota PIK.R\r\n', 'Jl.seraya 1 blok B no.10', '-Meningkatkan wawasan x pengetahuan dalam bidang keahlian networking\r\n-memperbanyak teman\r\n-melatih kepemimpinan', 'Networking', '2019-09-02 02:28:52'),
(25, 'Mohammad Ramadhoni', '190155201028', 'ra249394@gmail.com', '+628316798242', 'pria', '2000-12-26', 'Teknik Informatika', '4_x_6.JPG', '', 'SMAN 1 Kundur', 'Ketua Sekbid 4 OSIS (Bidang Olimpiade)\r\nKetua LCC\r\nKetua PMR\r\nAnggota PIK-R', 'Komplek Bintan Center Blok R No.22 RT.03/RW.03 Kelurahan Air Raja Kec Tanjung Pinang Timur', 'Ingin lebih memperdalam mengenai dunia perkomputeran terutama di bidang web programming supaya dapat mengembangkan aplikasi lebih baik lagi dan bisa bersaing dalam perkembangan teknolog dimasa yang semakin canggih/modern.', 'Web programming', '2019-09-02 02:29:38'),
(26, 'Efni Gunawan', '190120201014', 'efni.gunawan03@gmail.com', '085348276610', 'pria', '2001-09-03', 'Teknik Elektro', 'FB_IMG_1565613513664.jpg', '', 'SMK N 1 KINALI', 'Pernah bergabung di ekskul IT ', 'Jln.pramuka lorong pulau raja 4', 'Ingin menjadi seseorang yang sangat ahli dalam bidang IT ', 'Networking', '2019-09-02 02:51:22'),
(27, 'Muhammad Hidayad', '190155201068', 'wirdayad08@gmail.com', '081275033012', 'pria', '2001-02-22', 'Teknik informatika', 'IMG-20190411-WA0046.jpg', '', 'SMAN 1 KUNDUR', '', 'Jl. Lingga, Sei Jang, Bukit Bestari ', 'Ingin mengetahui dan belajar tentang teknologi', 'Multimedia', '2019-09-02 02:56:05'),
(28, 'Ramadhan taufiq', '190155201054', 'ramadhantaufiq098@gmail.com', '+628126874031', 'pria', '2000-12-15', 'Teknik informatika', '', '', 'Sman 1 toapaya', 'Tidak ada', 'Jalan wisata bahari', 'Ingin berorganisasi, memperbanyak teman dan  belajar bersama', 'Web programming', '2019-09-02 03:10:13'),
(29, 'Muhammad Rizky Fathur Rahman', '190155201049', 'mrizkyfr07@gmail.com', '+628316162586', 'pria', '2001-11-07', 'Teknik Informatika', 'IMG-20190702-WA02101.jpg', '', 'SMK N 1 TANJUNGPINANG', '', 'Perum. BukitRaya Blok Merbabu no 9', 'Ingin mengembangkan bakat dalam menciptakan program\" Baru di bidang IT', 'Web programming', '2019-09-02 04:35:51'),
(31, 'Silvia Erika Candra', '190155201040', 'silviaerikaa95@gmail.com', '081378158681', 'wanita', '2001-01-10', 'Teknik Informatika', '', '-', 'SMK Negeri 1 Tanjungpinang', 'Anggota pramuka, pasus, basket', 'Jln. R. H. Fisabilillah Gg. Menur No. 85', 'Ingin bisa membuat program, ', 'Programming', '2019-09-02 05:15:20'),
(32, 'Gandhi Rizky mahendro putra', '190120201051', 'gandhiputra25@gmail.com', '081270144326', 'pria', '2000-03-25', 'Teknik elektro', 'IMG20190606094940.jpg', '', 'SMKN 3 tanjung pinang ', '', 'Kampung bolang', 'Menambah ilmu pengetahuan ', 'Robotik', '2019-09-02 05:17:17'),
(33, 'Thaariq Satrio Wibisono', '190155201027', 'tio96.jpn@gmail.com', '+628228487689', 'pria', '2001-02-07', 'Teknik Informatika', 'IMG-20190831-WA0017.jpg', '', 'SMAIT IMAM SYAFI\'I', 'Sekretaris OSIS', 'Jl. Kuantan', 'Belajar lebih teratur dan Belajar berorganisasi', 'Programming', '2019-09-02 06:11:10'),
(35, 'Rifandy Karunia Ilahi', '190155201067', 'rifandykarunia@gmail.com', '+628526578515', 'pria', '2002-01-16', 'Teknik Informatika', 'IMG201908220922051.jpg', '', 'SMAN 1 Jemaja', '', 'Jl. kijang lama, Rt 02 Rw 03, no 37,  kelurahan melayu kota piring, KOTA TANJUNG PINANG, TANJUNG PINANG TIMUR, KEPULAUAN RIAU, ID, 29123', 'Ingin bergabung untuk belajar dan juga menambah wawasan untuk belajar programing', 'Programming', '2019-09-02 06:14:28'),
(36, 'Muhammad Yazhid Taufiqullah', '190155201019', 'yazidhyuuga@gmail.com', '081275952594', 'pria', '2001-04-27', 'Teknik Informatika', 'IMG-20190822-WA0038.jpg', '', 'SMAN 1 LINGGA', '', 'Batu 10, Air Raja', 'Saya Ingin Membuat Game Yang Lebih Menarik Dan Mempelajari Hal-Hal Yang Berkaitan Dengan Pembuatan Game Dan Animasi.', 'Programming', '2019-09-02 06:20:15'),
(38, ' Danu Prasetio', '190155201071', 'prasetiodanu26@gmail.com', '083875036299', 'pria', '2000-10-13', 'Teknik informatika', 'IMG_20190902_131401.JPG', '', 'SMKN 12 BEKASI', 'Smk menjadi anggota osis', 'Jl. HJ. UNGAR LR.SAPARUA NO 11', 'Menjadi orang yang kreatif dan inovatif', 'Multimedia', '2019-09-02 06:23:07'),
(39, 'Tinito Herdijo', '190155201025', 'dijogokilz@gmail.com', '+628535624864', 'pria', '2001-10-11', 'Teknik Informatika', 'IMG20190622144252-1.jpg', '', 'SMAN 4 BATAM', '- Menjadi anggota rohis 2 periode (SMP)\r\n- Menjadi anggota rohis 1 periode (SMA)\r\n- Menjadi wakil ketua rohis 1 periode (SMA)', 'Perumahan anggrek hill batu 13', 'Ingin mempelajari hal-hal dasar seputar cinematography', 'Multimedia', '2019-09-02 06:23:13'),
(40, 'Muhammad Nazrul Hery', '190155201063', 'insnazrul66@gmail.com', '+628954011049', 'pria', '2001-08-24', 'Teknik Informatika', 'IMG20190723074916.jpg', '', 'SMK Negri 1 Karimun', '', 'Jl.H.Ungar, Lr Mursallah', 'Ingin Mendalami dan Memahami Programming', 'Programming', '2019-09-02 06:28:45'),
(41, 'Rezki Juliando Putra', '190155201012', 'rezkiajja23@gmail.com', '+628526417751', 'pria', '2000-07-21', 'Teknik Informatika', 'PicsArt_02-08-10_20_53.jpg', '', 'SMAN 7 SIJUNJUNG', 'Koordinator Sekbid UKS', 'Jl.bukit barisan no 53A', 'Karna ingin belajar tentang pemograman dan merasa tertarik dengan CC ini', 'Programming', '2019-09-02 06:43:53'),
(42, 'Aldi Alfa Jeremy Nainggolan Parhusip', '1901552010', 'aldijeremy2010@gmail.com', '081371028402', 'pria', '2001-10-20', 'Teknik Informatika', 'IMG_2661.JPG', 'https://drive.google.com/drive/folders/1Cokso7VcM7mfmw1Er9GDjP5vVRfLbU-P?usp=sharing', 'SMKN 1 BIntan Utara', 'Pernah menjadi anggota osis di smk selama 2 tahun ', 'Perumahan Bumi Indah Blok E No.2', 'Pengen menambah pengalaman dan pengetahuan selama kuliah', 'Multimedia', '2019-09-02 07:01:17'),
(43, 'Rahmawati hasanah', '19038420420', 'rahmawatihasanah12@gmail.com', '+628318346026', 'wanita', '2000-12-05', 'Pendidikan kimia', 'received_387103548655297.jpeg', '', 'SMAN1 bintan pesisir ', '', 'Tanjung pinang, batu 4 jalan kartika', 'karena ingin mendalam kan kembali komputer dan ingin mencapai kan cita - cita saya menjadi programmer ', 'Programming', '2019-09-02 07:44:15'),
(44, 'Triandi', '190155201003', 'andit4856@gmail.com', '+628527152232', 'pria', '2000-10-05', 'Teknik informatika', 'IMG_20190619_095740_030.jpg', '', 'Smk Negeri 1 bunguran barat', 'Wakil ketua osis periode 2017/2018', 'Teluk keriting jlan usman harun', 'Karena ingin belajar dalam soal pembuatan web', 'Web programming', '2019-09-02 08:19:19'),
(45, 'MOHAMMAD KAHFI', '190155201047', 'kahfie@jagoankode.com', '+628212121849', 'pria', '2000-04-08', 'Teknik Informatika', '3.JPG', '', 'SMKN 4 Tanjungpinang', 'Belum ada.', 'Jl. Kp. Sidomukti Km.12 Arah Uban', 'Basic dasar saya adalah pemrograman berbasis desktop. Saya memiliki banyak pengalaman dalam pembuatan program, khususnya saya sudah banyak menjual produk program saya hampir ribuan orang di seluruh indonesia.. Saya ingin berkontribusi untuk UMRAH dan Komunitas ini agar bisa memajukan komunitas CC ini menjadi yang terbaik. Untuk alasan saya, adalah hanya ingin memperdalam ilmu saya di bidang pemrograman android dan desktop. Semoga kakak bisa menerima saya sebagai bagian dari komunitas ini.', 'Programming', '2019-09-02 08:26:11'),
(46, 'Annisa Ramadani', '190155201010', 'annisyahrahmadani21@gmail.com', '+628137650509', 'wanita', '2000-11-27', 'Teknik Informatika', 'IMG_20190728_192145.jpg', '', 'SMKN 1 Panyabungan', '', 'Taman seraya 2 Blok B No 4 RT 4 RW 4 Jln.Di panjaitan', 'Saya ingin belajar lebih dalam khususnya di bidang jaringan dan saya ingin menambah pengalaman saya sekaligus untuk memperbanyak teman.', 'Networking', '2019-09-02 10:03:30'),
(47, 'Harvan Pratama', '170565201002', 'pshtharvan@gmail.com', '081995716815', 'pria', '1999-07-07', 'Ilmu Pemerintahan', 'IMG_20190512_140527_541.jpg', 'https://drive.google.com/file/d/1dQEXkXcV_AcvfZtaCJKGju6qq1TA2jQD/view?usp=drivesdk', 'SMAN 3 Tanjungpinang', 'Belum ada, baru mau berorganisasi', 'JL.Puncak indah No.41 B', 'Saya ingin belajar dan menggali serta menambah ilmu di organisasi ini yang basic nya lebih ke perkembangan teknologi IT , dan saya juga tertarik dengan organisasi ini', 'Web programming', '2019-09-02 11:44:07'),
(48, 'JULITINUS ZEGA', '190155201001', 'tinuszega@gmail.com', '+628527537057', 'pria', '2000-07-30', 'Teknik Informatika', 'DSC_0019.JPG', '', 'SMAN 1 GUNUNGSITOLI UTARA-NIAS,SUMATERA UTARA', '', 'Jln.sei jang No.12 kota tanjung pinang', 'Ingin Menjadi Programmer yang handal', 'Programming', '2019-09-02 11:50:38'),
(49, 'Indah Dwi Putri', '180461201082', 'indahdwiputri45@gmail.com', '081275109440', 'wanita', '1999-12-25', 'Manajemen', 'IMG_20190630_175514.jpg', '', 'SMAN 5 Batam ', 'Kepramukaan dan rohis', 'Jln. WR Supratman no 100 batu 9 tanjung pinang ', 'Karna saya ingin sekali menpunyai keahlian khusus dalam komputer ', 'Programming', '2019-09-02 11:56:36'),
(50, 'Deni ahmad', '180461201060', 'blackhorse112211@gmail.com', '082287510481', 'pria', '2000-11-06', 'Manajemen', 'IMG_20190831_214607.jpg', '', 'SMA Negeri 1 mantang', 'PIK-R, PMR ', 'Kijang batu 23', 'Ingin menambah wawasan dan mengenal lbh dalm tntang computer', 'Networking', '2019-09-02 11:58:49'),
(53, 'Alan suparlan', '190462201073', 'Alansuparlan89@gmail.com', '083183232188', 'pria', '2001-05-18', 'Akuntasi', 'IMG_20190626_173154_132.jpg', '', 'Sma n 1 TELUK BINTAN', 'Anggota osis\r\nPik r', 'Toapaya', 'Saya ingin lebih mengetahui tentang teknologi saat ini yang memang suatu kebutuhan di masa depan', 'Networking', '2019-09-02 11:59:55'),
(54, 'M. Siddiq Ar Rasyid', '180461201017', 'msiddiqarrasyid@gmail.com', '+628228440861', 'pria', '1999-05-27', 'Manajemen', 'IMG-20190901-WA0025__01.jpg', '', 'SMAN 21 BATAM', 'Pengurus OSIS, Pengurus Rohis, Pengurus Mpk, Pengurus Dewan Ambalan', 'Kantin disamping Fakultas Teknik Umrah', 'Mengetahui lebih lanjut tentang Komputer, karena pada masa sekolah sering main warnet', 'Multimedia', '2019-09-02 12:06:59'),
(55, 'Annisa fadillah fani ', '190155201056', 'annisafadillahfani83@gmail.com', '08984002622', 'wanita', '2001-11-02', 'Teknik informatika', '', '', 'SMA N 4 TANJUNGPINANG ', 'Pramuka,PMR', 'Kp. Sei carang rt 03 rw 05 no.18', 'Ingin belajar dan menambah wawasan sekaligus memperbanyak teman', 'Web programming', '2019-09-02 13:37:41'),
(56, 'Mohd.Raul Gusti Ananda', '190155201053', 'mohdraulgustiananda942@gmail.com', '+628238400605', 'pria', '2000-11-05', 'Teknik Informatika', '', '', 'SMAN 4 KARIMUN', 'Pramuka', 'Jl.Batu kucing', 'Ingin mempelajari tentang pemrograman', 'Programming', '2019-09-02 13:55:46'),
(57, 'Raken Rizki Eka Putra', '190155201034', 'azkat1602@gmail.com', '081378082114', 'pria', '2002-02-16', 'Teknik Informatika', '20190902_073052.jpg', '', 'SMAN 2 SINGKEP', 'Anggota PMR\r\nAnggota Pramuka', 'Jalan menur', 'Mendalami lebih rumit tentang pembelajarang di prodi TI', 'Programming', '2019-09-02 15:53:23'),
(58, 'Maria Cinta agustina wulandari odang', '190155201004', 'cintamaria055@gmail.com', '081390197789', 'wanita', '2001-08-14', 'Teknik informatika', '', '', 'Smk yapim biru biru', '', 'Perumahan bumi Indah \r\nBlok e no. 07\r\nJl. Asoka', 'Ingin mendalami computer cyber ', 'Multimedia', '2019-09-03 04:57:11'),
(59, 'Natanael Gea', '190155201020', 'naelgea44@gmail.com', '082267123454', 'pria', '2001-12-23', 'Teknik Informatika', 'IMG_5270.JPG', '', 'SMA N 1 TUHEMBERUA', '', 'Jln. Perumahan griya srnggarang', 'Ingin cari pengalaman dalam berorganisasi dan ingin mendalami dunia teknologi', 'Web programming', '2019-09-03 06:20:30'),
(60, 'Aullia Three Bintan', '190155201059', 'aulliathree05@gmail.com', '083161566310', 'wanita', '2002-03-05', 'Teknik Informatika ', 'IMG-20190423-WA0093.jpg', 'https://drive.google.com/folderview?id=1-vKcXDSzNlD8rC9K4GOwMz9fkVI12Xsq', 'SMAN 1 TELUK BINTAN', 'Ketua Pradani Pramuka\r\nMayoret Drumband\r\nPaskibraka Provinsi Kepri', 'Jl.  Tok sadek, tembeling tanjung,  kp. Guntung', 'Ingin menambah wawasan dibilang teknologi serta menambah ilmu', 'Multimedia', '2019-09-03 07:22:55'),
(61, 'Maisarah Salsabila Fitriana', '190462201047', 'maisarahsalsabilafitriana@gmail.com', '+628994888427', 'wanita', '2000-01-10', 'Akuntansi', '', '', 'SMKN 2 Tanjungpinang', 'Pramuka', 'Km 8 jl.cendrawasih kp.wonoyoso gg rukun ', 'Salah satu alasan saya ingin bergabung di computer cyber adalah karena saya ingin mengetahui lebih jelas dan lebih dalam untuk memahami dunia IT. Karena ketika saya mengikuti organisasi ini, saya mempunyai kesempatan besar dalam penerimaan pekerjaan yang saya impikan, yang juga sesuai dengan jurusan saya(akuntansi).  Saya sangat senang apabila diterima di organisasi ini. Dan akan saya manfaatkan ilmu ilmu tersebut dengan sebaik-baik nya.', 'Programming', '2019-09-03 08:40:30'),
(62, 'Efni Gunawan', '190120201014', 'efni.gunawan03@gmail.com', '085348276610', 'pria', '2001-09-03', 'Teknik Elektro', '_IMG_000000_000000_1549528608170.jpg', '', 'SMK N 1 KINALI', 'Pernah ikut dalam ekstrakulikuler IT', 'Jl.pramuka lorong pulau raja 4 ', 'Ingin memperdalam pengetahuan saya tentang bahasa-bahasa pemrograman dan menjadi programer yang handal', 'Programming', '2019-09-03 13:11:34'),
(63, 'Ahmad Haris Zulfikar', '190155201033', 'ah3510446@gmail.com', '083184368525', 'pria', '2001-02-17', 'Teknik Informatika', 'Recovered_jpg_file(1406).jpg', 'https://drive.google.com/folderview?id=1-eLWvSPnanv3VvMHNLKpDO46kOLsDQjV', 'SMKN 1 BINTAN UTARA', 'Anggota Buletin Sekolah, Silat & Volly', 'Kampung Bugis', 'Ingin memperdalam ilmu pemrograman & ingin melatih lebih kerjasama dalam tim & juga menambah kawan', 'Programming', '2019-09-03 14:21:56'),
(64, 'Andi ika zuhaini', '190462201017', 'ika62333@gmail.com', '085834144950', 'wanita', '2002-02-26', 'Akuntansi', 'IMG_20190808_104607.jpg', '', 'SMAN 1 MORO', 'pramuka', 'Jl.kartika,gang mayangsari 4 no.4', 'Alasan saya ingin bergabung di computer cyber karna ingin menambah wawasan,pengetahuan,pengalaman,melatih hard skill dan soft skill dan ingin mengisi waktu luang dengan kegiatan yg bermanfaat. ', 'Programming', '2019-09-04 00:11:21'),
(65, 'Rohima rahmawati', '190462201058', 'RahmawatiRohima@gmail.Com', '081372339886', 'wanita', '2001-08-11', 'Akuntansi', '', '', 'MA Fatahillah tarempa', 'Pramuka', 'Jalan batu kucing ,gang putri duyung ,no 114 ', 'Alasan Saya ,Saya ingin lebih mempelajari tentang dunia teknologi komputer', 'Programming', '2019-09-04 02:13:37'),
(67, 'Rizki Pratama', '190254242018', 'rizkiipratama45@gmail.com', '081261233844', 'pria', '2001-08-01', 'Manajemen Sumberdaya Perairan', '', '', 'SMAN 2 Tanjungpinang', 'Rohis ', 'Jl.pramuka \r\nLorong Sumba no.49', 'Belajar lebih jauh tentang ilmu computer', 'Web programming', '2019-09-04 03:59:06'),
(68, 'Rahul Rumapea', '190254242027', 'rahulnaun26@gmail.com', '+628217060807', 'pria', '2001-02-26', 'Manajemen sumberdaya perairan', 'PhotoGridLite_1563985703122.jpg', 'Ada', 'SMAN 1 MANDAU', 'Organisasi Kerohanian', 'Pamedan bt 4', 'Ingin belajar mengenai sistem software serta perangkat lunak.\r\nKarna saya suka bagian editing video', 'Multimedia', '2019-09-04 05:06:09'),
(69, 'Harry Agung Gultom', '190254242025', 'harryagunggultom7@gmail.com', '082273336366', 'pria', '2001-11-25', 'Management Sumberdaya Perairan', '15675739570701650187635.jpg', '', 'SMA N 1 LINTONGNIHUTA', 'Anggota Pramuka\r\nAnggota Paskibra\r\nAnggota OSIS', 'Bintan Center', 'Ingin lebih tahu tentang programing', 'Programming', '2019-09-04 05:16:47'),
(70, 'Doni Muhamad Fachry', '190155201062', 'donifachry35@gmail.com', '082284013894', 'pria', '2002-07-01', 'Teknik Informatika ', 'IMG20190904091413.jpg', '', 'SMAN 2 SINGKEP', 'Anggota Paskibraka\r\n', 'Kampung Air Raja, Perumahan Vista Indah', 'Ingin mempelajari lebih banyak ilmu tentang computer dan tertarik dengan organisasi ini', 'Programming', '2019-09-04 08:14:09'),
(71, 'LUCKY PRADANA', '190120201043', 'pradanalucky168@gmail.com', '+628127604057', 'pria', '2000-06-02', 'TEKNIK ELEKTRO', 'FOTO.jpg', 'https://drive.google.com/file/d/10ama_PNtlL5ydgEiFaub2_edY5G-nEBa/view?usp=sharing', 'SMA NEGERI 1 BINTAN UTARA', 'Patroli keamanan sekolah', 'Jl Lembah Merpati Km 13, dekat akademi kebidanan anugerah bintan', 'Ingin belajar programming serta bisa mendapatkan pengalaman berorganisasi', 'Programming', '2019-09-04 10:41:38'),
(72, 'Patricia Angelina Tampubolon', '190155201038', 'tampubolonpatricia@gmail.com', '082237331821', 'wanita', '2001-06-01', 'Teknik Informatika', '', '', 'SMAN 5 Merangin', '', 'JL. Asoka dalam Blok E No. 7, Perum Bumi Indah, ', 'Karena saya ingin mencoba hal baru dalam hal per komputeran ', 'Multimedia', '2019-09-04 11:11:13'),
(74, 'wan siti fadhilah azmii hasyari', '190155201022', 'harfianiluthan21@gmail.com', '+628121818289', 'wanita', '2003-03-28', 'teknik informatika', '', '', 'SMAN 3 Tanjungpinang', 'tidak pernah ikut organisasi selama sma', 'Jalan datuk pakau, senggarang', 'ingin mempelajari lebih dalam mengenai ilmu komputer ', 'Web programming', '2019-09-05 07:20:00'),
(76, 'Ganda Bagus Wibisono', '170155201039', 'gandabaguswibisono123@gmail.com', '+628127915248', 'pria', '2001-03-14', 'Teknik Informatika', '1554886425902.jpg', '', 'SMAN 2 TANJUNGPINANG', '- Dewan Ambalan Pramuka SMAN2 \r\n  Tanjungpinang', 'Jalan Kota Piring No.31 pulau Malem Dewa RT 03 RW 07, kelurahan Melayu Kota Piring, kecamatan Tanjungpinang Timur, Tanjungpinang, Kepulauan Riau.', 'Alasan saya ingin bergabung dengan Study Club CC adalah karena saya ingin belajar tentang dunia progamming yang merupakan salah satu divisi di CC selain itu saya ingin aktif dalam Kegiatan mahasiswa UMRAH', 'Programming', '2019-09-05 12:20:26'),
(77, 'Hutami Sukma Hayati', '180384202008', 'hutamisukma0520@gmail.com', '+628536319423', 'wanita', '2000-05-20', 'Pendidikan matematika', '', '', 'Sman 2 tg pinang', '', 'Jalan ir sutami', 'Pengen mengetahui lebih tentang IT', 'Multimedia', '2019-09-06 03:01:59'),
(78, 'Nur Asikin', '180384202001', 'nurasikinasniarty@gmail.com', '081533215589', 'wanita', '2000-04-11', 'Pendidikan matematika', '', '', 'Sman 1 moro', '', 'Jalan engkau putri Gg cendana 2 blok B no 39', 'Ingin mendalami komputer', 'Multimedia', '2019-09-06 03:05:57'),
(79, 'Johan Jeques Junior', '190120201055', 'johanjunior75@gmail.com', '+628127502735', 'pria', '2001-07-02', 'Teknik Elektro', 'DSCF3471.JPG', '', 'SMAN 8 Batam', 'Wakil Divisi Iman dan Taqwa OSIS', 'Batu 8 bawah', 'Saya ingin belajar mengenai robotik, mengenali bahan-bahan untuk membuat robot, dan juga bisa mengenali lebih dalam bagaimana cara membuat sesuatu benda, contohnya seperti robot.', 'Robotik', '2019-09-06 08:24:48'),
(81, 'Said afrizal', '180155201019', 'afrizalsaid406@gmail.com', '+628132713949', 'pria', '2000-04-01', 'Teknik informatika', 'IMG_20190906_154552.jpg', 'https://drive.google.com/folderview?id=1-tqMwawzonomEo-q07wFkafoUiASS_HK', 'MAN 2 NATUNA', 'Ketua pramuka organisasi madrasah', 'Jl. Sei jang Gg. Gatra D\'sakira lantai 3 No. 7b', 'Ingin mempelajari tentang jaringan dan menambah pengalaman berorganisasi serta menambah teman', 'Networking', '2019-09-06 11:56:00'),
(82, 'Yusi Yosep', '190120201012', 'yusiyosep1469@gmail.com', '083183418707', 'pria', '2001-08-25', 'Teknik Elektro', 'IMG-20190420-WA0035.jpg', 'https://drive.google.com/open?id=1nxB4MHhdvSaHz6nf18QYAiKSNwZOvF_n', 'SMKN 1 Bintan Utara', 'Anggota Drumband dan Anggota Mading', 'Kampung Bugis', 'Ingin lebih mengembangkan kemampuan diri', 'Robotik', '2019-09-06 16:17:45'),
(84, 'MUHAMMAD FARMA', '180461201014', 'ffarma356@gmail.com', '+628153658257', 'pria', '1999-07-04', 'MANAJEMEN', '', '', 'MAN 2 KOTA PAYAKUMBUH', 'Anggota osis', 'Bintan centre', 'Ingin lebih mengetahui segala tendang dunia internet', 'Networking', '2019-09-07 04:48:23'),
(85, 'Surahman', '190120201010', 'surahman1465@gmail.com', '083167651529', 'pria', '2001-03-31', 'Teknik Elektro', 'IMG_4706.JPG', 'https://drive.google.com/open?id=10JuUmWoFMFeekVzYSZoLkvGQc74z5Z2b dan https://drive.google.com/open?id=17LNVsnv_cUQaiOLB4o-3C_euGz1_67-u', 'SMKN 1 Bintan Utara', 'Anggota Mading & Buletin', 'Kp. Bugis', 'Belajar berorganisasi dan mengembangkan dasar yang telah diperoleh', 'Robotik', '2019-09-07 07:19:24'),
(86, 'Deni ahmad', '180461201060', 'blackhorse112211@gmail.com', '082287510481', 'pria', '2000-11-06', 'Manajemen', '', '', 'SMA Negeri 1 mantang', 'Pik r, pramuka', 'Bintan center', 'Ingin menambah ilmu dan juga pengalaman', 'Networking', '2019-09-07 09:38:46'),
(88, 'Rizky Fajariansyah', '190155201057', 'remoderf@gmail.com', '+628771912485', 'pria', '2001-09-09', 'Teknik Informatika', 'Tkj3_(36)_Rizky_Fajariansyah.jpg', '', 'SMKN 1 Tanjungpinang', '-', 'Jln. Nuri Indah Gg. Bahagia No. 72 ', 'Alasan saya masuk ke computer cyber adalah untuk menambah pengalaman berorganisasi dan menambah ilmu dalam bidang IT', 'Web programming', '2019-09-07 23:52:41'),
(89, 'Riani Fitri Ibnul malik', '1901552010', 'rianifitri53@gmail.com', '083162047642', 'wanita', '2000-12-18', 'Teknik informatika', '', '', 'SMA 4 Tanjungpinang', 'Sekertaris saja Wirakartika kodim', 'Jalan Adisucipto perumahan mahkota alam permai blok ruko no 23', 'Ingin lebih belajar tentang web pemrogramming ', 'Web programming', '2019-09-08 03:16:40'),
(90, 'Ica santika', '190155201011', 'icasantika02@gmail.com', '+628527280652', 'wanita', '2001-04-02', 'Teknik informatika', '', '', 'SMAN 1 subi', 'Anggota osis', 'Km 11', 'Ingin belajar dan mengasah kemampuan tentang computer cyber', 'Web programming', '2019-09-08 03:58:23'),
(91, 'Emiliza Pane', '190155201065', 'emilizapane@gmail.com', '081361055381', 'wanita', '2001-09-02', 'Teknik informatika', '', '', 'Man lima puluh', '', 'Perum geriya permata kharisma', 'Karena cc sebagi sarana saya untuk belajar dan sharing walaupun gak tau apapun tentang dunia pemrograman khususnya di web progamming tapi saya tertarik untuk mendalami nya selain itu saya juga bisa menambah wawasan dan relasi teman dari beda jurusan', 'Web programming', '2019-09-08 04:02:15'),
(92, 'Dedi Novrianto Ramadhan', '190155201052', 'dedinovrianto60@gmail.com', '+628316181143', 'pria', '2001-11-26', 'Teknik Informatika', 'LRM_EXPORT_20190514_224142.jpg', '', 'SMKN 4 Tanjungpinang', 'Eskul Desain Grafis', 'Perum. Kijang Kencana 3 Blok A no.95', 'karena saya memilih Multimedia , saya ingin mendalami ilmu desain dan mengembangkan hasil karya yang sudah saya buat', 'Multimedia', '2019-09-08 04:39:22'),
(93, 'Noby syahputra', '190155201055', 'nobysyahputra2603@gmail.com', '081277826494', 'pria', '2000-03-26', 'Teknik informatika', '42573010_902453673276472_5017743354851491840_o.jpg', '', 'Smk negeri 3 tanjungpinang', '', 'Jalan sultan mahmud no 28 tanjungunggat', 'Karena saya tertarik daninginbelajar membuatwebsite ', 'Web programming', '2019-09-08 14:07:07'),
(94, 'Raihan Zhaky Al-hafizh', '190155201037', 'zachalhafzh@gmail.com', '+628536354643', 'pria', '2001-12-04', 'Teknik Informatika', 'IMG_9838.JPG', '-', 'SMAN 4Tanjungpinang', 'Pernah mengikuti organisasi pramuka', 'perumnas sei jang,jalan pengibu no.39', '1. Tertarik dengan organisasi\r\n2. Ingin menambah ilmu, wawasan, dan juga teman.', 'Web programming', '2019-09-08 14:32:48'),
(95, 'Reynaldi', '190155201041', 'reynalddi93@gmail.com', '089668300111', 'pria', '2001-07-05', 'Teknik informatika ', 'IMG-20190803-WA0193.jpg', '', 'SMAN 3 TanjungPinang ', 'Pks', 'Jalan kuantan gnag sejahtera lorong kutai ', 'saya tertarik dengan computer cyber. saya ingin melatih dan membangun skill di computer cyber', 'Web programming', '2019-09-08 14:52:13'),
(97, 'Gema Suryanda', '190155201030', 'gemagmsuryanda@gmail.com', '+628228398748', 'pria', '2001-02-25', 'Teknik Informatika', 'IMG_2299.jpg', 'https://drive.google.com/drive/folders/1bZcu0LYP0YnJJdkLCLLnqinpkc-G2ixy?usp=sharing', 'SMKN 3 TanjungPinang', 'Ketua Kelas', 'Perumahan Pelita Indah 2 Batu 9', 'computer cyber adalah tempat yang tepat bagi saya untuk menyalurkan minat dan bakat. Apalagi sejak kecil saya sudah menyukai dunia komputer, saya harap jika di terima saya dapat menciptakan inovasi inovasi yang bermanfaat  bagi banyak orang ', 'Programming', '2019-09-09 09:45:19'),
(98, 'Puja Lestari Sitopu', '170563201050', 'pujaejj225@gmail.com', '08566609788', 'wanita', '1998-11-29', 'Ilmu Administrasi Negara', 'B612_20170909_162619-1.jpg', '-', 'SMA N 1 RAYA', 'Anghota PERS RISALLAH UMRAH', 'Jl.Bukit Cermin', 'Alasan saya ingin mengikuti organisasi ini yaitu karena saya masih belum memahami betul cara menggubakan perangkat komputer. Dan juga seprti yg kit ketahui komputet sudah merupakan barang wajib yg kita gunakan dalam kehidupan sehari hari untuk mempermudah pekerjaan kita.Jadi saya ingin belajar dalam organisasi ini agar dapat memanfaatkan perangkat komputer dengan baik .', 'Multimedia', '2019-09-09 09:49:45'),
(99, 'ANNISA FITRI', '170563201045', 'anfiannisaf20@gmail.com', '+628136499510', 'wanita', '2000-01-20', 'Ilmu administrasi negara', 'IMG_20180213_142541.JPG', '', 'SMAN4 TANJUNG PINANG', 'Pers risalah maritim', 'Jl. Pemuda gg sagu', '1. Ingin manambah ilmu pegetahuan, skill dan pengalaman\r\n2. Agar dapat menggetahui bagaimana caranya mengakses sesuatu atau membuat sesuatu dengan benar di bidang teknologi ', 'Web programming', '2019-09-09 10:27:10'),
(101, 'BELA SAPITRI', '190155201035', 'bellasftrr@gmail.com', '+628228399024', 'wanita', '2001-09-19', 'Teknik informatika', 'IMG-20190729-WA0027.jpg', '', 'SMAN 1 Bintan Timur', 'Anggota OSIS, anggota paskibraka', 'Jl.Pasar Berdikari No.040 RT.01 RW 018 Kel.Kijang Kota Kec.Bintan Timur Kab.Bintan', 'Ingin belajar tentang pemrograman komputer karena saya tidak tahu apa-apa dari sebelumnya tentang komputer', 'Web programming', '2019-09-09 10:31:52'),
(103, 'Meliya Astuti', '170573201059', 'astutimeliya@gmail.com', '081267475044', 'wanita', '1999-07-18', 'Ilmu Administrasi Negara', 'IMG-20190909-WA0024.jpg', '-', 'SMA 1 BINTAN', 'HIMANEGARA', 'Jl.tanjung ayun sakti', 'Ingin menambah pengetahuan di bidang Multimedia.', 'Multimedia', '2019-09-09 10:41:15'),
(104, 'Muhammad Ade', '180574201051', 'adewardana98@gmail.com', '081277508008', 'pria', '1999-05-17', 'Ilmu Hukum', 'FaceApp_1563523877540.jpg', 'belum ada ', 'Smk Putra Satria', 'Pengurus Osis\r\nPeriode 2016-2017\r\nPeriode 2017-2018', 'Jalan Cendrawasih', 'saya ingin belajar tentang we programming', 'Web programming', '2019-09-09 11:13:55'),
(105, 'Sri Astuti', '170563201074', 'tutiikk8@gmail.com', '081276460980', 'wanita', '2002-03-06', 'Ilmu Administrasi Negara', 'IMG-20190909-WA0028.jpg', '-', 'SMA 1 BINTAN', 'HIMANEGARA', 'Jl.Batu Kucing', 'Ingin memahami lebih dalam mengenai Bidang Programming', 'Web programming', '2019-09-09 11:16:37'),
(106, 'Nurul Septianti', '170563201053', 'nseptianti095@gmail.com', '+628316181188', 'wanita', '1999-09-10', 'Administrasi Publik', 'IMG_20190906_124814.jpg', '', 'MA darul falah', 'Anggota Ldf ', 'Jl dua saudara', 'Ingin menambah ilmu dan wawasan sehingga bisa membagikan kpd org lain', 'Web programming', '2019-09-09 11:18:55'),
(107, 'Zaharatul Aini', '170563201052', 'zaharatulaini1297@gmail.com', '+628228530756', 'wanita', '1997-12-15', 'Ilmu Administrasi Negara', '20190822_133246.jpg', '', 'SMAN 2 KUNDUR', '1. Anggota syiar dan media LDF FSIRI FISIP UMRAH\r\n2. Anggota kaderisasi KAMMI Bulang Linggi KEPRI\r\n3. Bendahara Umum LDF FSIRI FISIP UMRAH\r\n4. MPK SMAN 2 KUNDUR', 'Jl. Merapas Sei Jang km. 4 ', 'Karena ingin mengasah bakat khususnya di bidang multimedia. Dan keinginan untuk menjadikan bakat tersebut sebagai sebuah hobi dan pekerjaan', 'Multimedia', '2019-09-09 11:51:38'),
(108, 'Yhoga Dewa Nata', '1901552023', 'yhogadewa@gmail.com', '+628964433359', 'pria', '2001-08-16', 'Teknik Informatika', 'imgcache0_2230276.jpg', '', 'SMAN 4 Tanjungpinang', '', 'Jln.H.Ungar Lr.Bangka No.26', 'Menjadi lebih baik dalam mengerjakan tugas', 'Programming', '2019-09-09 12:11:20'),
(109, 'Singgih Prasetyo', '190155201032', 'kak.sing2904@gmail.com', '+628318371599', 'pria', '2001-04-29', 'Teknik Informatika', '', '', 'SMA N 1 BINTAN UTARA', 'Kepramukaan', 'Perum. Griya Nusa, batu 8', 'Ingin belajar dan mendapatkan keahlian dari computer cyber', 'Programming', '2019-09-09 12:15:03'),
(111, 'Rofiyan Rozikin', '170155201029', 'rofian12@gmail.com', '+628566403278', 'pria', '2001-12-12', 'Teknik Informatika', '', '', 'SMAN4 Tanjungpinang', 'Wakil Pradana SMAN4 Tanjungpinang\r\nAnggota DKD Provinsi Kepri', 'Jalan Pemuda Gang Asoka Nomor 18', 'Karena saya ingin belajar ', 'Networking', '2019-09-09 13:03:36'),
(112, 'BAYU MAULANA', '190155201018', 'bmaulana988@gmail.com', '+629560952108', 'pria', '2001-03-09', 'Teknik informatika', 'LRM_EXPORT_3635998852466_20190708_094022488.jpeg', '', 'SMK 3 Tanjungpinang', '', 'Jalan Yos Sudarso batu hitam', 'Agar punya kegiatan yang bermanfaat dan bisa dikembangkan dengan pembelajaran di sekolah. Biar bisa kenal dengan senior yang lain.', 'Web programming', '2019-09-09 13:28:08'),
(113, 'BUDI SEJATI', '190120201039', 'bud3728@gmail.com', '0895603795448', 'pria', '2000-05-21', 'Teknik Elektro', '8dd8314009d3754180197361ae6a7958ee18535b_(1).jpg', '', 'SMAN 5 TANJUNGPINANG', 'SEKRETARIS PRAMUKA', 'JL. GATOT SUBROTO KM. 5 BAWAH NO. 23', 'Alasan saya ikut di CC karena saya memiliki minat dibagian design, terutama dalam editing video menggunakan adobe premiere', 'Multimedia', '2019-09-09 13:52:00'),
(114, 'Fatimah Nur Azzahra', '190155201044', 'azzahraputri2307@gmail.com', '+628537427207', 'wanita', '2001-07-23', 'Teknik Informatikal', '', '', 'SMKN 1 Tembilahan', '', 'Jalan air raja, bintan center', 'Ingin belajar dan memperdalami keahlian ', 'Multimedia', '2019-09-09 13:55:47'),
(116, 'Aria Kurniati', '190155201045', 'ariakurniati01@gmail.com', '+628229928040', 'wanita', '2001-05-18', 'Teknik Informatika', 'IMG-20190909-WA0054.jpg', '', 'SMK Negeri 1 Tembilahan', '', 'Perum. Taman Harapan Indah Blok B1 No.12 RT.03 RW.02 Kel.Air Raja Kec.Tg.pinang timur', 'Ingin belajar mengenali banyak hal tentang komputer', 'Web programming', '2019-09-09 14:51:51'),
(117, 'FARID KHAIRUDDIN', '190120201049', 'doniapril2804@gmail.com', '085365037688', 'pria', '2000-06-27', 'Teknik elektro', 'IMG-20190909-WA0045.jpg', '', 'SMA N 1 SINGKEP', 'Olahraga', 'Taman harapan indah', 'Ingin belajar dari hal yang menantang dari organisasi computer cyber', 'Robotik', '2019-09-09 15:12:18'),
(118, 'Indra Kurniawan', '190155201051', 'kurniawan.indra0727@gmail.com', '+628238629482', 'pria', '2001-07-27', 'Teknik Informatika', 'DSC_1074.JPG', 'https://drive.google.com/open?id=1GTLe4sqWeVqcG9FOXv7etWpue9fDPfiX', 'SMAN 4 Tanjungpinang', 'OSIS Seksi Dokumentasi Komunikasi dan Teknologi, Pramuka, Musik', 'Jalan Hanjoyo Putro, Kampung Bukit Asri, KM 9 Tanjungpinang', 'ingin menambah wawasan dan mengembangkan  lebih dalam lagi tentang dunia web programming. Karena saya memiliki sebuah blog yang sedang dalam tahap perkembangan', 'Web programming', '2019-09-09 16:11:52'),
(119, 'Ema', '190155201013', 'alga@kuatic.com', '12314', 'wanita', '2020-04-02', 'dadang1', 'dimas.jpg', 'ersdf', 'dsfsdfssdfs', 'dsf', 'sdf', 'sdf', 'Networking', '2020-03-03 02:00:33'),
(120, 'Admin CC', '1901552010129', 'dadang@gmail.com', '3456346', 'pria', '2020-03-17', 'dadang4', 'avatar-profile.jpg', 'dfs', 'sads', 'dgf', 'fdg', 'fdg', 'Programming', '2020-03-03 02:10:37'),
(121, 'Harun', '1901552010129', 'nateriverx0@gmail.com', '2342', 'wanita', '2020-03-17', 'dadang3', 'CC.png', 'sad', 'sads', 'fdgt', 'gfh', 'gfh', 'Web programming', '2020-03-03 02:13:51');

-- --------------------------------------------------------

--
-- Table structure for table `developer_account`
--

CREATE TABLE `developer_account` (
  `id_developer` int(11) NOT NULL,
  `name_developer` varchar(255) DEFAULT NULL,
  `password_developer` varchar(255) DEFAULT NULL,
  `email_developer` varchar(255) DEFAULT NULL,
  `divisi` varchar(255) DEFAULT NULL,
  `as_` enum('Frontend Web Developer','Backend Web Developer','Game Developer','Android Developer','UI/UX','Data Science','Network Engineer','Embeded Software Engineer','DevOps','Other') DEFAULT NULL,
  `reason` text,
  `is_active` tinyint(1) DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `date_joined` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `developer_account`
--

INSERT INTO `developer_account` (`id_developer`, `name_developer`, `password_developer`, `email_developer`, `divisi`, `as_`, `reason`, `is_active`, `date_end`, `date_joined`) VALUES
(1, 'Pramudia Pangestu', '$2y$10$B5Xp4qwPem02qIvZH4iCjeReFHN53yU3CLz/EsnkDPW4MC/klDEXS', 'pramush@gmail.com', NULL, 'Backend Web Developer', 'coba coba aja', 1, '2020-08-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE `divisi` (
  `id_divisi` int(11) NOT NULL,
  `nama_divisi` varchar(255) NOT NULL,
  `url` varchar(128) DEFAULT NULL,
  `keterangan_divisi` text NOT NULL,
  `gambar_divisi` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`id_divisi`, `nama_divisi`, `url`, `keterangan_divisi`, `gambar_divisi`, `date_created`) VALUES
(1, 'Programming', 'programming', '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p>Adalah divisi yang mendalami tentang coding pada sebuah program.&nbsp; Program yang dimaksud adalah Mobile Application dan Game Development. Program tersebut dikembangkan dengan bahasa Java, C#, Kotlin, dll. Dipelajari untuk mengembangkan program yang tepat guna dan efisien bagi pengguna.</p>\n<p>Ketua Divisi :</p>\n<ul>\n<li>Zayus Rifan Zafarani</li>\n</ul>\n<p>Koordinator Mobile&nbsp; Application :</p>\n<ul>\n<li>&nbsp;M. Wahyu Irgan Agustino</li>\n</ul>\n<p>Koordinator Game :</p>\n<ul>\n<li>Sulthan Syarif Hani Setya Putra</li>\n</ul>\n</body>\n</html>', 'programming.png', '2020-02-24 16:14:35'),
(2, 'Web Programming', 'web-programming', '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p>Web Programming adlaah divisi yang befokus pada perancangan dan pembuatn sebuah website. Di dalam divisi ini dibagi oleh dua bidang, yaitu Frontend yang mengatur sebuah tampilan website dan Backend yang mengatur alur kerja sistem pada sebuah website.</p>\n<p>Ketua Divisi :</p>\n<ul>\n<li>Muhammad Zaini</li>\n</ul>\n<p>Koordinator Backend :</p>\n<ul>\n<li>&nbsp;Raja Azian</li>\n</ul>\n</body>\n</html>', 'web.png', '2020-02-24 16:14:47'),
(3, 'Robotik', 'robotik', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Robotik adalah divisi yang memperlajari penerapan pemrograman pada mikrokontroller maupun komputer kecil (Rasberry Pi) yang dapat berinteraksi dengan lingkungan sekitar dengan memanfaatkan sensor-sensor yang ada.</p>\r\n</body>\r\n</html>', 'robotik.png', '2020-01-27 06:26:43'),
(4, 'Networking', 'networking', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Networking adalah divisi yang mempelajari tentang bagaimana mendesign suatu jaringan yang baik dan beraturan dengan UTP dan FIBER. Sistem fiber diterapkan di jaringan lokal agar mudah digunakan mengikuti perkembangan zaman yang mengharuskan konektivitas yang cepat antara client dan server.</p>\r\n</body>\r\n</html>', 'networking.png', '2020-01-27 06:26:53'),
(5, 'Multimedia', 'multimedia', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Multimedia adalah divisi yang bergerak dibidang Desain Grafis, 3D Modelling, serta Perancangan UI dan UX.</p>\r\n</body>\r\n</html>', 'multimedia.png', '2020-01-27 06:27:04');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id_gallery` int(11) NOT NULL,
  `judul_gallery` varchar(255) NOT NULL,
  `keterangan_gallery` text NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `tanggal_acara` bigint(20) DEFAULT NULL,
  `gallery_divisi` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `status_gallery` varchar(20) NOT NULL,
  `tanggal_gallery` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id_gallery`, `judul_gallery`, `keterangan_gallery`, `gambar`, `tanggal_acara`, `gallery_divisi`, `id_user`, `status_gallery`, `tanggal_gallery`) VALUES
(1, 'coba1', 'motivasi', 'cc_placeholder.png', 1519268640, 2, 25, 'Draft', '2020-03-26 04:13:28'),
(2, 'Gambar dimas', 'motivasi juga', 'cc_placeholder.png', 1519268640, 1, 25, 'Publish', '2020-03-26 04:13:27'),
(3, 'coba3', 'motivasi juga juga', 'cc_placeholder.png', 11519268640, 2, 9, 'Publish', '2020-03-26 04:13:32'),
(4, 'coba4', 'hello world', 'cc_placeholder.png', 1519268640, 3, 9, 'Publish', '2020-03-26 04:13:31'),
(5, 'funny', 'bertho erizal', 'cc_placeholder.png', 1519268640, 2, 9, 'Publish', '2020-03-26 04:13:34'),
(6, 'coba6', 'i love coding', 'cc_placeholder.png', 1519268640, 3, 15, 'Publish', '2020-03-26 04:13:29'),
(7, 'Rumah Istana', 'Tamadun ', 'cc_placeholder.png', 1519268640, 5, 15, 'Publish', '2020-03-26 04:13:37'),
(8, 'Placeholder CC', 'Placeholder untuk web cc', 'cc_placeholder.png', 1519268640, 4, 25, 'Draft', '2020-03-26 04:13:40'),
(9, 'asd', 'as', 'cc_placeholder.png', 1519268640, 1, 25, 'Publish', '2020-03-26 04:13:35'),
(10, 'sa', 'sdfsd', 'cc_placeholder.png', 1519268640, 5, 25, 'Publish', '2020-03-26 04:13:42'),
(11, 'da', 'asd', 'cc_placeholder.png', 1584966584, 4, 25, 'Publish', '2020-03-26 04:13:45'),
(12, 'asa', 'asd', '400x400.png', 1584966584, 5, 25, 'Publish', '2020-03-26 04:13:44');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_divisi`
--

CREATE TABLE `gallery_divisi` (
  `id_gallery_divisi` int(11) NOT NULL,
  `judul_gallery_divisi` varchar(255) DEFAULT NULL,
  `keterangan_gallery_divisi` text,
  `gambar` varchar(255) DEFAULT NULL,
  `tanggal_acara` bigint(20) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `status_gallery` varchar(255) DEFAULT NULL,
  `tanggal` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `tahun_jabatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`, `tahun_jabatan`) VALUES
(5, 'Sekretaris', 2017),
(6, 'Bendahara', 2017),
(7, 'Ketua Divisi Web Programming', 2018),
(8, 'Ketua Divisi Multimedia', 2018),
(9, 'Ketua Divisi Robotik', 2018),
(10, 'Ketua Divisi Networking', 2018),
(11, 'Ketua Umum', 2018),
(12, 'Wakil Ketua', 2018),
(14, 'Ketua Divisi Programming', 2018),
(15, 'Ketua Umum', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `jetskill_class`
--

CREATE TABLE `jetskill_class` (
  `id_class` int(11) NOT NULL,
  `name_class` varchar(128) NOT NULL,
  `url_class` varchar(255) DEFAULT NULL,
  `description_class` text NOT NULL,
  `attachment_rule_class` varchar(128) DEFAULT NULL,
  `logo` varchar(128) DEFAULT NULL,
  `available_at` bigint(20) DEFAULT NULL,
  `date_created` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jetskill_class`
--

INSERT INTO `jetskill_class` (`id_class`, `name_class`, `url_class`, `description_class`, `attachment_rule_class`, `logo`, `available_at`, `date_created`) VALUES
(1, '3D Class Modelling', '3d-class-modelling', '<p><span style=\"background-color: inherit;\">3D Class di computer<span style=\"background-color: rgb(255, 255, 0);\"> cyber</span></span> <br></p><p>\"Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.\"</p><p>\"Sed\r\n ut perspiciatis unde omnis iste natus error sit voluptatem accusantium \r\ndoloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo \r\ninventore veritatis et quasi architecto beatae vitae dicta sunt \r\nexplicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut \r\nodit aut fugit, sed quia consequuntur magni dolores eos qui ratione \r\nvoluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum \r\nquia dolor sit amet, consectetur, adipisci velit, sed quia non numquam \r\neius modi tempora incidunt ut labore et dolore magnam aliquam quaerat \r\nvoluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam \r\ncorporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?\r\n Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse \r\nquam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo \r\nvoluptas nulla pariatur?\"</p><p>\"But I must explain to you how all this mistaken idea of denouncing \r\npleasure and praising pain was born and I will give you a complete \r\naccount of the system, and expound the actual teachings of the great \r\nexplorer of the truth, the master-builder of human happiness. No one \r\nrejects, dislikes, or avoids pleasure itself, because it is pleasure, \r\nbut because those who do not know how to pursue pleasure rationally \r\nencounter consequences that are extremely painful. Nor again is there \r\nanyone who loves or pursues or desires to obtain pain of itself, because\r\n it is pain, but because occasionally circumstances occur in which toil \r\nand pain can procure him some great pleasure. To take a trivial example,\r\n which of us ever undertakes laborious physical exercise, except to \r\nobtain some advantage from it? But who has any right to find fault with a\r\n man who chooses to enjoy a pleasure that has no annoying consequences, \r\nor one who avoids a pain that produces no resultant pleasure?\"</p><p>\"At vero eos et accusamus et iusto odio dignissimos ducimus qui \r\nblanditiis praesentium voluptatum deleniti atque corrupti quos dolores \r\net quas molestias excepturi sint occaecati cupiditate non provident, \r\nsimilique sunt in culpa qui officia deserunt mollitia animi, id est \r\nlaborum et dolorum fuga. Et harum quidem rerum facilis est et expedita \r\ndistinctio. Nam libero tempore, cum soluta nobis est eligendi optio \r\ncumque nihil impedit quo minus id quod maxime placeat facere possimus, \r\nomnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem \r\nquibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet\r\n ut et voluptates repudiandae sint et molestiae non recusandae. Itaque \r\nearum rerum hic tenetur a sapiente delectus, ut aut reiciendis \r\nvoluptatibus maiores alias consequatur aut perferendis doloribus \r\nasperiores repellat.\"</p>', NULL, '3dmodelling.png', 1578934800, 1579539600),
(2, 'Web Toko Online', 'web-toko-online', '<p>sadasdasdasdasd<br></p>', NULL, '', 1576083600, 1579539600),
(3, 'Membuat Game', 'membuat-game', 'asasasas', NULL, '', 1639242000, 1579539600);

-- --------------------------------------------------------

--
-- Table structure for table `jetskill_member`
--

CREATE TABLE `jetskill_member` (
  `id_member` int(11) NOT NULL,
  `name_member` varchar(255) DEFAULT NULL,
  `email_member` varchar(255) DEFAULT NULL,
  `divisi_member` int(11) DEFAULT NULL,
  `token` varchar(4) DEFAULT NULL COMMENT 'token adalah pengganti password agar lebih mudah untuk ditulis',
  `date_activation` bigint(20) DEFAULT NULL,
  `avatar` varchar(128) DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jetskill_member`
--

INSERT INTO `jetskill_member` (`id_member`, `name_member`, `email_member`, `divisi_member`, `token`, `date_activation`, `avatar`, `date_end`, `is_active`, `date_created`) VALUES
(78, 'Dimas Nugroho Putro', 'dimasnugroho673@gmail.com', NULL, 'c9Rp', 1580160605, 'pria.png', NULL, '0', NULL),
(73, 'Wahyu Irgan Agustino', 'wahyuirgan12@gmail.com', NULL, 'PdLI', 1580160605, 'wanita.png', NULL, '0', NULL),
(70, 'Sulthan SHP', 'sulthan.shp.13@gmail.com', NULL, 'J1Y6', 1567510925, NULL, NULL, '1', NULL),
(84, 'Arizal Akbar', 'arizal@gmail.com', 2, 'KZg7', NULL, 'pria.png', '2021-01-03', '1', '2020-01-30 07:12:47'),
(85, 'zayus', 'zrz@gmail.com', 1, 'bNVB', NULL, 'pria.png', '2023-01-15', '1', '2020-01-31 14:53:34');

-- --------------------------------------------------------

--
-- Table structure for table `jetskill_member_allowed_class`
--

CREATE TABLE `jetskill_member_allowed_class` (
  `id_member_allowed_class` int(11) NOT NULL,
  `email_allowed` varchar(255) NOT NULL,
  `id_class` int(11) NOT NULL,
  `date_created` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jetskill_member_allowed_class`
--

INSERT INTO `jetskill_member_allowed_class` (`id_member_allowed_class`, `email_allowed`, `id_class`, `date_created`) VALUES
(2, 'arizal@gmail.com', 1, 1580152878),
(3, 'zrz@gmail.com', 1, 1),
(7, 'zrz@gmail.com', 2, 1585235436),
(8, 'dimasnugroho673@gmail.com', 1, 1585235466),
(10, 'arizal@gmail.com', 2, 1585238172),
(11, 'dimasnugroho673@gmail.com', 2, 1585281217);

-- --------------------------------------------------------

--
-- Table structure for table `jetskill_member_dropout_class`
--

CREATE TABLE `jetskill_member_dropout_class` (
  `id_member_dropout_class` int(11) NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `email_dropout` varchar(255) NOT NULL,
  `id_class` int(11) NOT NULL,
  `reason` text,
  `date_dropout` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jetskill_member_join_class`
--

CREATE TABLE `jetskill_member_join_class` (
  `id` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_class` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_complete` tinyint(1) NOT NULL,
  `joined_at` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jetskill_member_join_class`
--

INSERT INTO `jetskill_member_join_class` (`id`, `id_member`, `id_class`, `is_active`, `is_complete`, `joined_at`) VALUES
(4, 84, 1, 0, 0, 1580381808),
(5, 85, 1, 0, 0, 1585117462);

-- --------------------------------------------------------

--
-- Table structure for table `jetskill_member_login_class`
--

CREATE TABLE `jetskill_member_login_class` (
  `id_member_login_class` int(11) NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_class` int(11) DEFAULT NULL,
  `token_class` int(11) DEFAULT NULL,
  `date_login` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jetskill_submission_class`
--

CREATE TABLE `jetskill_submission_class` (
  `id_submission` int(11) NOT NULL,
  `id_class` int(11) DEFAULT NULL,
  `title_submission` varchar(255) DEFAULT NULL,
  `description_submission` text,
  `deadline` bigint(20) DEFAULT NULL,
  `submit_on_late` int(11) DEFAULT NULL,
  `date_created` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jetskill_submission_class`
--

INSERT INTO `jetskill_submission_class` (`id_submission`, `id_class`, `title_submission`, `description_submission`, `deadline`, `submit_on_late`, `date_created`) VALUES
(1, 1, 'Buat objek 3D sederhana tanpa texture', 'Buat objek 3D sederhana tanpa texture menggunakan blender selain objek contoh yang ada di dalam video', 1585132552, 1, 1580540086),
(2, 2, 'Insert data produk dengan gambar', 'Insert data produk dengan gambar menggunakan framework codeigniter. Jika anda menggunakan ajax tentu akan lebih baik lagi', 1581292800, 0, 1580541477);

-- --------------------------------------------------------

--
-- Table structure for table `jetskill_submission_member`
--

CREATE TABLE `jetskill_submission_member` (
  `id_submission_member` int(11) NOT NULL,
  `id_submission` int(11) DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_class` int(11) DEFAULT NULL,
  `file_submission_member` varchar(255) DEFAULT NULL,
  `link_submission_member` varchar(255) DEFAULT NULL,
  `description_by_member` text,
  `status` varchar(128) DEFAULT NULL,
  `date_submitting` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jetskill_submission_member`
--

INSERT INTO `jetskill_submission_member` (`id_submission_member`, `id_submission`, `id_member`, `id_class`, `file_submission_member`, `link_submission_member`, `description_by_member`, `status`, `date_submitting`) VALUES
(1, 1, 84, 1, '', NULL, '', 'ditolak', 1580554818),
(2, 1, 84, 1, 'Realterm_1-99-271.zip', NULL, '<p>dfsdfsdf<br></p>', 'diterima', 1580713218),
(3, 1, 84, 1, 'amatic.zip', NULL, '<p>asdadad<br></p>', 'proses', 1585132771),
(4, 1, 84, 1, 'jdk-11.0.4_linux-x64_bin.tar.gz', NULL, '<p>tes file gede</p><p><br></p>', 'proses', 1585133361),
(5, 1, 84, 1, 'UTF-8animusicsbrADAMAS.zip', NULL, '<p>tes lagu adamas</p><p><br></p>', 'proses', 1585133456),
(6, 1, 84, 1, 'pycharm-community-2019.2.3.tar.gz', NULL, '<p>tes pycharm<br></p>', 'proses', 1585133539),
(7, 1, 84, 1, 'pycharm-community-2019.2.3.tar.gz', NULL, '<p>halo hal<br></p>', 'proses', 1585133586),
(8, 1, 84, 1, 'Download-on-the-App-Store.zip', NULL, '<p>harusnya sih bisa ini tu<br></p>', 'proses', 1585133717),
(9, 1, 85, 1, 'back-school-background-blackboard.zip', NULL, '<p>tes deadline berjalan apa tidak<br></p>', 'diterima', 1585136990);

-- --------------------------------------------------------

--
-- Table structure for table `jurusan_in_umrah`
--

CREATE TABLE `jurusan_in_umrah` (
  `id_jurusan` int(11) NOT NULL,
  `judul_jurusan` varchar(128) DEFAULT NULL,
  `fakultas` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan_in_umrah`
--

INSERT INTO `jurusan_in_umrah` (`id_jurusan`, `judul_jurusan`, `fakultas`) VALUES
(1, 'Teknik Informatika', 'Teknik'),
(2, 'Teknik Informatika', 'Teknik'),
(3, 'Akuntansi', 'Ekonomi'),
(4, 'Manajemen', 'Ekonomi'),
(5, 'Ilmu Kelautan', 'Fakultas Ilmu Kelautan dan Perikanan'),
(6, 'Manajemen Sumberdaya Perairan', 'Fakultas Ilmu Kelautan dan Perikanan'),
(7, 'Budidaya Perairan', 'Fakultas Ilmu Kelautan dan Perikanan'),
(8, 'Teknologi Hasil Perikanan', 'Fakultas Ilmu Kelautan dan Perikanan'),
(9, 'Sosial Ekonomi Perikanan', 'Fakultas Ilmu Kelautan dan Perikanan'),
(10, 'Pendidikan Bahasa dan Sastra Indonesia', 'Fakultas Keguruan dan Ilmu Pendidikan'),
(11, 'Pendidikan Bahasa Inggris', 'Fakultas Keguruan dan Ilmu Pendidikan'),
(12, 'Pendidikan Kimia', 'Fakultas Keguruan dan Ilmu Pendidikan'),
(13, 'Pendidikan Bioogi', 'Fakultas Keguruan dan Ilmu Pendidikan'),
(14, 'Pendidikan Matematika', 'Fakultas Keguruan dan Ilmu Pendidikan'),
(15, 'Ilmu Administrasi Negara', 'Fakultas Ilmu Sosial dan Ilmu Politik'),
(16, 'Ilmu Pemerintahan', 'Fakultas Ilmu Sosial dan Ilmu Politik'),
(17, 'Sosiologi', 'Fakultas Ilmu Sosial dan Ilmu Politik'),
(18, 'Ilmu Hukum', 'Fakultas Ilmu Sosial dan Ilmu Politik'),
(19, 'Hubungan Internasional', 'Fakultas Ilmu Sosial dan Ilmu Politik');

-- --------------------------------------------------------

--
-- Table structure for table `karya`
--

CREATE TABLE `karya` (
  `id_karya` int(11) NOT NULL,
  `judul_karya` varchar(128) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `detail_karya` text NOT NULL,
  `user_karya` int(11) NOT NULL,
  `jenis_karya` varchar(255) DEFAULT NULL,
  `karya_divisi` int(11) DEFAULT NULL,
  `anggota_tim` text,
  `gambar_karya` varchar(255) DEFAULT NULL,
  `status_karya` varchar(255) DEFAULT NULL,
  `link_playstore` varchar(255) DEFAULT NULL,
  `link_appstore` varchar(255) DEFAULT NULL,
  `link_dribbble` varchar(255) DEFAULT NULL,
  `link_adobexd` varchar(255) DEFAULT NULL,
  `link_github` varchar(255) DEFAULT NULL,
  `link_youtube` varchar(255) DEFAULT NULL,
  `link_lain` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karya`
--

INSERT INTO `karya` (`id_karya`, `judul_karya`, `url`, `detail_karya`, `user_karya`, `jenis_karya`, `karya_divisi`, `anggota_tim`, `gambar_karya`, `status_karya`, `link_playstore`, `link_appstore`, `link_dribbble`, `link_adobexd`, `link_github`, `link_youtube`, `link_lain`, `date_created`) VALUES
(11, 'Merombak thema ubuntu 20.04', 'karya-1', 'It’s not called “Dark Mode” rather “Window Colours”. This is (arguably) a more descriptive change given that only window colours are affected by the setting (the overall ‘shell’ theme isn’t).\n\nBut regardless of title the new setting gives folks a super-duper, easy-peasy way to enable a dark mode on Ubuntu — and that’s awesome.\n\nAdditionally, the new Window Colours setting makes it just as easy to switch to a fully ‘light’ look, or mix things up (literally) by sticking with the standard Yaru theme (which pairs some dark elements with some lighter ones) that ships as default.\n\nWhile this isn’t the most earth shattering of changes it’s one I think is sorely needed (not to mention currently MIA in upstream GNOME Shell).\n\nThe only real question I have now is which window colour setting with most people will use — let me know which one you’ll be rocking in the comments section below!', 45, 'Tim', 5, NULL, 'dimas.jpg', 'Publish', 'https://dribbble.com/shots/popular', 'ad', 'asdas', 'asda', 'sada', 'sadf', 'playstore', '2020-03-25 17:21:14'),
(12, 'Karya2', 'karya-2', 'ini adalah karya 1', 45, 'Tim', 2, NULL, NULL, 'Publish', NULL, NULL, NULL, 'werwe', NULL, NULL, '', '2020-03-25 17:14:11'),
(13, 'Karya3', 'karya-3', 'ini adalah karya 1', 45, NULL, 1, NULL, NULL, 'Draft', NULL, NULL, NULL, NULL, NULL, 'sdsds', 'playstore', '2020-03-25 17:15:00'),
(14, 'Karya4', 'karya-4', 'ini adalah karya 1', 45, NULL, 3, NULL, NULL, 'Publish', NULL, NULL, NULL, NULL, NULL, NULL, 'playstore', '2020-03-23 10:25:17'),
(15, 'Karya5', 'karya-5', 'ini adalah karya 1', 45, NULL, 4, NULL, NULL, 'Publish', NULL, NULL, NULL, NULL, NULL, NULL, 'playstore', '2020-03-23 10:25:18');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_artikel`
--

CREATE TABLE `kategori_artikel` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_artikel`
--

INSERT INTO `kategori_artikel` (`id_kategori`, `nama_kategori`, `date_created`) VALUES
(1, 'Web', '2019-12-22 16:52:50');

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi`
--

CREATE TABLE `konfigurasi` (
  `id_konfigurasi` int(11) NOT NULL,
  `namaweb` varchar(100) NOT NULL,
  `tagline` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat` text,
  `telepon` varchar(50) DEFAULT NULL,
  `keywords` varchar(300) DEFAULT NULL,
  `description` text,
  `google_map` text,
  `metatext` text,
  `logo` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `about` text,
  `gambar_about` varchar(255) DEFAULT NULL,
  `auto_emailer_agenda` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi`
--

INSERT INTO `konfigurasi` (`id_konfigurasi`, `namaweb`, `tagline`, `website`, `email`, `alamat`, `telepon`, `keywords`, `description`, `google_map`, `metatext`, `logo`, `facebook`, `twitter`, `instagram`, `about`, `gambar_about`, `auto_emailer_agenda`) VALUES
(1, 'Computer Cyber Study Club', 'Cinta Kami Abadi Pada Teknologi', 'https://umrah.ac.id/', 'cc.umrah@gmail.com', 'Jl. Politeknik, KM. 24, Senggarang, Tanjung Pinang, Kota Tanjung Pinang, Kepulauan Riau 29115', '+6285658553529', 'Computer Cyber Study Club', '\"MENJADIKAN COMPUTER CYBER SEBAGAI \"PUSAT STUDY CLUB TEKNOLOGI\" DALAM PEMBELAJARAN, PENELITIAN DAN PELATIHAN BERBASIS INFORMASI DAN MEKANIK DALAM RANGKA MENINGKATKAN MUTU PENDIDIKAN SERTA KUALITAS ANGGOTA\"', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63828.974656282386!2d104.42369986313024!3d0.9137823293447456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x17bbdc25ac6cf462!2sUniversitas+Maritim+Raja+Ali+Haji!5e0!3m2!1sid!2sid!4v1515928298536\" width=\"100%\" height=\"500\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'Komunitas yang bergerak dibidang Teknlologi di ruang lingkup Universitas yang mewadahi karya, minat dan bakat mahasiswa', 'CC.png', 'https://www.facebook.com/groups/1103441939714171/', 'https://twitter.com/ccumrah', 'https://www.instagram.com/computercyber/', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Organisasi Computer Cyber adalah organisasi bidang teknologi informasi dan komunikasi.</p>\r\n</body>\r\n</html>', '01_cover-.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi_kepengurusan`
--

CREATE TABLE `konfigurasi_kepengurusan` (
  `id_konfigurasi_kepengurusan` int(11) NOT NULL,
  `tahun_kepengurusan_aktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi_kepengurusan`
--

INSERT INTO `konfigurasi_kepengurusan` (`id_konfigurasi_kepengurusan`, `tahun_kepengurusan_aktif`) VALUES
(1, 2018);

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi_pendaftaran`
--

CREATE TABLE `konfigurasi_pendaftaran` (
  `id_konfigurasi` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi_pendaftaran`
--

INSERT INTO `konfigurasi_pendaftaran` (`id_konfigurasi`, `tanggal_mulai`, `tanggal_selesai`, `is_active`) VALUES
(1, '2019-08-28', '2020-07-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi_pengumuman`
--

CREATE TABLE `konfigurasi_pengumuman` (
  `id_konfigurasi` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi_pengumuman`
--

INSERT INTO `konfigurasi_pengumuman` (`id_konfigurasi`, `tanggal_mulai`, `tanggal_selesai`, `is_active`) VALUES
(1, '2019-10-03', '2019-11-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembina`
--

CREATE TABLE `pembina` (
  `id_pembina` int(11) NOT NULL,
  `nama_pembina` varchar(255) NOT NULL,
  `dosen` varchar(255) DEFAULT NULL,
  `email_pembina` varchar(255) NOT NULL,
  `jenis_pembina` varchar(64) NOT NULL,
  `status_pembina` varchar(255) DEFAULT NULL,
  `gambar_pembina` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembina`
--

INSERT INTO `pembina` (`id_pembina`, `nama_pembina`, `dosen`, `email_pembina`, `jenis_pembina`, `status_pembina`, `gambar_pembina`, `date_created`) VALUES
(1, 'Sulfikar Salu, S.Kom, M.Kom', 'Teknik Informatika', 'sulfikarsallu@umrah.ac.id', 'Pembina 1', 'Pindah', 'avatar-profile2.jpg', '2020-03-20 14:48:28'),
(2, 'Tekad', 'Teknik Informatika', 'sulfikarsallu@umrah.ac.id', 'Pembina 2', 'Aktif', 'avatar-profile5.jpg', '2020-03-20 14:49:04');

-- --------------------------------------------------------

--
-- Table structure for table `survery_member_cc`
--

CREATE TABLE `survery_member_cc` (
  `id` int(11) NOT NULL,
  `tingkat_kepuasan` varchar(128) DEFAULT NULL,
  `tingkat_keterbantuan` varchar(128) DEFAULT NULL,
  `tingkat_penilaian` varchar(128) DEFAULT NULL,
  `kelemahan_sistem` text,
  `saran_pengembangan` text,
  `as_` varchar(128) DEFAULT NULL,
  `date_submitting` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id_tag` int(11) NOT NULL,
  `nama_tag` varchar(128) NOT NULL,
  `id_berita` int(11) DEFAULT NULL,
  `id_blog` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id_tag`, `nama_tag`, `id_berita`, `id_blog`, `date_created`) VALUES
(1, 'anggota baru', 18, 18, '2020-03-02 16:21:38'),
(2, 'computer-cyber', 18, 18, '2020-03-02 16:21:44'),
(3, 'anggota baru', 17, 17, '2020-03-02 16:21:47'),
(4, '27', 0, NULL, '2020-04-03 15:20:51'),
(5, '27', 0, NULL, '2020-04-03 15:20:51'),
(6, 'nama_tag', 30, NULL, '2020-04-03 15:22:28'),
(7, 'nama_tag', 30, NULL, '2020-04-03 15:22:28'),
(8, 'nama_tag', 30, NULL, '2020-04-03 15:22:28'),
(9, 'nama_tag', 34, NULL, '2020-04-03 15:39:24'),
(10, 'nama_tag', 34, NULL, '2020-04-03 15:39:24'),
(11, 'nama_tag', 34, NULL, '2020-04-03 15:39:24'),
(12, 'nama_tag', 34, NULL, '2020-04-03 15:39:24'),
(13, 'nama_tag', 34, NULL, '2020-04-03 15:39:24'),
(14, 'haha', 45, NULL, '2020-04-03 15:40:35'),
(15, 'aha', 45, NULL, '2020-04-03 15:40:35'),
(16, 'er', 45, NULL, '2020-04-03 15:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `akses_level` varchar(20) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `update_password_at` bigint(20) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `email`, `username`, `password`, `akses_level`, `gambar`, `id_admin`, `update_password_at`, `tanggal`) VALUES
(21, 'staffcc', 'informatika@gmail.com', 'staffcc', '1c0f5b09b8d9f974112cb67188efb065063ccd21', '1', 'CC6.png', 0, NULL, '2019-09-02 02:24:18'),
(25, 'Super Admin ', 'cc.umrah@gmail.com', 'admincc', '210afa6050cb586af4e3b5003d1e5f20ecf8ed88', '21', 'CC.png', 0, 1529118191, '2020-01-25 17:19:36'),
(26, 'Interviewer CC', 'interviewercc@gmail.com', 'interviewercc', '7cf7eddb174125539dd241cd745391694250e526', '2', 'CC6.png', 0, NULL, '2019-09-06 15:40:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `anggota_diterima`
--
ALTER TABLE `anggota_diterima`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `anggota_karya`
--
ALTER TABLE `anggota_karya`
  ADD PRIMARY KEY (`id_anggota_karya`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `berita_anggota_diterima`
--
ALTER TABLE `berita_anggota_diterima`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `calon_anggota`
--
ALTER TABLE `calon_anggota`
  ADD PRIMARY KEY (`id_peserta`);

--
-- Indexes for table `developer_account`
--
ALTER TABLE `developer_account`
  ADD PRIMARY KEY (`id_developer`);

--
-- Indexes for table `divisi`
--
ALTER TABLE `divisi`
  ADD PRIMARY KEY (`id_divisi`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id_gallery`);

--
-- Indexes for table `gallery_divisi`
--
ALTER TABLE `gallery_divisi`
  ADD PRIMARY KEY (`id_gallery_divisi`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `jetskill_class`
--
ALTER TABLE `jetskill_class`
  ADD PRIMARY KEY (`id_class`);

--
-- Indexes for table `jetskill_member`
--
ALTER TABLE `jetskill_member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `jetskill_member_allowed_class`
--
ALTER TABLE `jetskill_member_allowed_class`
  ADD PRIMARY KEY (`id_member_allowed_class`);

--
-- Indexes for table `jetskill_member_dropout_class`
--
ALTER TABLE `jetskill_member_dropout_class`
  ADD PRIMARY KEY (`id_member_dropout_class`);

--
-- Indexes for table `jetskill_member_join_class`
--
ALTER TABLE `jetskill_member_join_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jetskill_submission_class`
--
ALTER TABLE `jetskill_submission_class`
  ADD PRIMARY KEY (`id_submission`);

--
-- Indexes for table `jetskill_submission_member`
--
ALTER TABLE `jetskill_submission_member`
  ADD PRIMARY KEY (`id_submission_member`);

--
-- Indexes for table `jurusan_in_umrah`
--
ALTER TABLE `jurusan_in_umrah`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `karya`
--
ALTER TABLE `karya`
  ADD PRIMARY KEY (`id_karya`);

--
-- Indexes for table `kategori_artikel`
--
ALTER TABLE `kategori_artikel`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  ADD PRIMARY KEY (`id_konfigurasi`);

--
-- Indexes for table `konfigurasi_kepengurusan`
--
ALTER TABLE `konfigurasi_kepengurusan`
  ADD PRIMARY KEY (`id_konfigurasi_kepengurusan`);

--
-- Indexes for table `konfigurasi_pendaftaran`
--
ALTER TABLE `konfigurasi_pendaftaran`
  ADD PRIMARY KEY (`id_konfigurasi`);

--
-- Indexes for table `konfigurasi_pengumuman`
--
ALTER TABLE `konfigurasi_pengumuman`
  ADD PRIMARY KEY (`id_konfigurasi`);

--
-- Indexes for table `pembina`
--
ALTER TABLE `pembina`
  ADD PRIMARY KEY (`id_pembina`);

--
-- Indexes for table `survery_member_cc`
--
ALTER TABLE `survery_member_cc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id_tag`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id_agenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `anggota_diterima`
--
ALTER TABLE `anggota_diterima`
  MODIFY `id_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `anggota_karya`
--
ALTER TABLE `anggota_karya`
  MODIFY `id_anggota_karya` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `berita_anggota_diterima`
--
ALTER TABLE `berita_anggota_diterima`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `calon_anggota`
--
ALTER TABLE `calon_anggota`
  MODIFY `id_peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `developer_account`
--
ALTER TABLE `developer_account`
  MODIFY `id_developer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `divisi`
--
ALTER TABLE `divisi`
  MODIFY `id_divisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id_gallery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `gallery_divisi`
--
ALTER TABLE `gallery_divisi`
  MODIFY `id_gallery_divisi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `jetskill_class`
--
ALTER TABLE `jetskill_class`
  MODIFY `id_class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jetskill_member`
--
ALTER TABLE `jetskill_member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `jetskill_member_allowed_class`
--
ALTER TABLE `jetskill_member_allowed_class`
  MODIFY `id_member_allowed_class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `jetskill_member_dropout_class`
--
ALTER TABLE `jetskill_member_dropout_class`
  MODIFY `id_member_dropout_class` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jetskill_member_join_class`
--
ALTER TABLE `jetskill_member_join_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jetskill_submission_class`
--
ALTER TABLE `jetskill_submission_class`
  MODIFY `id_submission` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jetskill_submission_member`
--
ALTER TABLE `jetskill_submission_member`
  MODIFY `id_submission_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jurusan_in_umrah`
--
ALTER TABLE `jurusan_in_umrah`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `karya`
--
ALTER TABLE `karya`
  MODIFY `id_karya` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `kategori_artikel`
--
ALTER TABLE `kategori_artikel`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  MODIFY `id_konfigurasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `konfigurasi_kepengurusan`
--
ALTER TABLE `konfigurasi_kepengurusan`
  MODIFY `id_konfigurasi_kepengurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pembina`
--
ALTER TABLE `pembina`
  MODIFY `id_pembina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `survery_member_cc`
--
ALTER TABLE `survery_member_cc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id_tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
