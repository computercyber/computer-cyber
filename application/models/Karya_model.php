<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karya_model extends CI_Model
{

    // halaman home
    public function listing()
    {
        $this->db->select('*');
        $this->db->from('karya');
        $this->db->join('anggota', 'anggota.id_anggota = karya.user_karya', 'left');
        $this->db->where(array('status_karya' => 'Publish',));
        $query = $this->db->get();
        return $query->result_array();
    }

    // halaman home dengan filter
    public function listing_filter($id_divisi)
    {
        $this->db->select('*');
        $this->db->from('karya');
        $this->db->join('anggota', 'anggota.id_anggota = karya.user_karya', 'left');
        $this->db->where(array(
            'status_karya' => 'Publish',
            'karya_divisi' => $id_divisi
        ));
        $query = $this->db->get();
        return $query->result_array();
    }

    // detail karya
    public function detail($url)
    {
        $this->db->select('*');
        $this->db->from('karya');
        $this->db->where(array('status_karya' => 'publish', 'url' => $url));
        $this->db->join('anggota', 'anggota.id_anggota = karya.user_karya', 'left');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function admin_listing()
    {
        $this->db->select('*');
        $this->db->from('karya');
        $this->db->join('anggota', 'anggota.id_anggota = karya.user_karya', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_anggota_karya($id_karya)
    {
        $this->db->select('*');
        $this->db->from('anggota_karya');
        $this->db->join('anggota', 'anggota.id_anggota = anggota_karya.id_anggota');
        $this->db->where(array(
            'id_karya' => $id_karya,
        ));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_data_divisi($id_karya)
    {
        $this->db->select('*');
        $this->db->from('karya');
        $this->db->join('divisi', 'karya.karya_divisi = divisi.id_divisi');
        $this->db->where(array(
            'id_karya' => $id_karya,
        ));
        $query = $this->db->get();
        return $query->row_array();
    }
}
