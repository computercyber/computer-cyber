<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anggota_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing()
	{
		$this->db->select('anggota.*, divisi.nama_divisi, jabatan.*');
		$this->db->from('anggota');
		//relasi database
		$this->db->join('divisi', 'divisi.id_divisi = anggota.id_divisi', 'left');
		$this->db->join('jabatan', 'jabatan.id_jabatan = anggota.id_jabatan', 'left');
		//end relasi database
		$this->db->order_by('id_anggota', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail($id_anggota)
	{
		$this->db->select('anggota.*, divisi.nama_divisi, jabatan.*');
		$this->db->from('anggota');
		//relasi database
		$this->db->join('divisi', 'divisi.id_divisi = anggota.id_divisi');
		$this->db->join('jabatan', 'jabatan.id_jabatan = anggota.id_jabatan');
		// $this->db->join('divisi', 'divisi.id_divisi = anggota.id_divisi', 'left');
		// $this->db->join('jabatan', 'jabatan.id_jabatan = anggota.id_jabatan', 'left');
		//end relasi database
		$this->db->where('id_anggota', $id_anggota);
		$this->db->order_by('id_anggota', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	public function tambah($data)
	{
		$this->db->insert('anggota', $data);
	}

	public function update($data)
	{
		$this->db->where('id_anggota', $data['id_anggota']);
		$this->db->update('anggota', $data);
	}

	public function delete($data)
	{
		$this->db->where('id_anggota', $data['id_anggota']);
		$this->db->delete('anggota', $data);
	}
}

/* End of file anggota_model.php */
/* Location: ./application/models/anggota_model.php */
