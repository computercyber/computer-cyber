<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Pendaftaran_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getConfigRegister()
    {
        return $this->db->get_where('konfigurasi_pendaftaran', array('id_konfigurasi' => 1));
    }

    public function listing()
    {
        $query = $this->db->get('calon_anggota');
        return $query->result();
    }

    public function getDataExport()
    {
        return $this->db->get('calon_anggota');
    }

    public function detail($id_peserta)
    {
        $query = $this->db->get_where('calon_anggota', array(
            'id_peserta' => $id_peserta
        ));
        return $query->result();
    }

    public function resetData()
    {
        $this->db->truncate('calon_anggota');
    }

    public function delete($data)
    {
        $this->db->where('id_peserta', $data['id_peserta']);
        $this->db->delete('calon_anggota', $data);
    }
}
