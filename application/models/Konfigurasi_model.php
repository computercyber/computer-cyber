<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigurasi_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing()
	{
		$query=$this->db->get('konfigurasi');
		return $query->row();
	}

	public function detail($id_konfigurasi)
	{
		$query=$this->db->get_where('konfigurasi',array('id_konfigurasi' => $id_konfigurasi));
		return $query->row();
	}

	public function update($data)
	{
		$this->db->where('id_konfigurasi', $data['id_konfigurasi']);
		$this->db->update('konfigurasi', $data);
	}

	public function delete($data)
	{
		$this->db->where('id_konfigurasi', $data['id_konfigurasi']);
		$this->db->update('konfigurasi', $data);
	}

	public function kategori1()
	{
		$query=$this->db->get('konfigurasi');
		return $query->row();
	}

}

/* End of file konfigurasi_model.php */
/* Location: ./application/models/konfigurasi_model.php */