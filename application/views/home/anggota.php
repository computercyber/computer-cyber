<div class="jumbotron jumbotron-fluid jumbotron-head">
	<div class="container">
		<h1 class="display-4 text-center heading-menu"><?php echo $title_sub ?></h1>
		<p class="lead text-center subheading-menu"><a href="<?php echo base_url(); ?>" target="_blank"><?php echo $title ?> Study Club</a></p>
	</div>
</div>


<section>
	<div class="container">
		<div class="row">
			<?php foreach ($jabatan as $jabatan) : ?>
				<div class="col-lg-4 col-md-4 col-sm-6" data-animate-effect="fadeIn">
					<div class="gtco-staff">
						<img class="rounded-circle" src="<?php echo base_url('assets/upload/image/thumbs/anggota/' . $jabatan->gambar) ?>" width="120" alt="">
						<h3><?php echo $jabatan->nama_anggota ?></h3>
						<strong class="role"><?php echo $jabatan->nama_jabatan ?></strong>
						<p><?php echo $jabatan->email_anggota ?></p>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</section>