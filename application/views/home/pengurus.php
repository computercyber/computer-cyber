<style>
	.jumbotron-pengurus {
		height: 500px;
		background-size: cover;
	}

	.heading-menu {
		font-family: 'Quicksand', sans-serif;
		color: white;
		opacity: .8;
		font-weight: 400;
		margin-top: 120px !important;
	}

	.subheading-menu a {
		color: white;
		opacity: .8;
		transition: .4s;
	}

	.subheading-menu a:hover {
		color: #5E31C2;
	}


	.btn-primary,
	.btn-primary:hover {
		border: none;
		background-color: #5E31C2;
	}

	.btn-primary:hover {
		box-shadow: 0px 0px 20px 0px rgba(94, 49, 194, .3);
	}

	.single-service {
		text-align: center;
		border: 1px rgba(0, 0, 0, .1) solid;
		/* background-color: #f9f9ff; */
		border-radius: 0.25em;
		padding: 40px 0;
		-webkit-transition: all 0.3s ease 0s;
		-moz-transition: all 0.3s ease 0s;
		-o-transition: all 0.3s ease 0s;
		transition: all 0.3s ease 0s;
	}

	.single-service p {
		color: #222;
		transition: all 0.3s ease 0s;
	}

	.single-service:hover {
		cursor: default;
		box-shadow: 0px 20px 20px 0px rgba(126, 90, 206, .1);
		color: #fff;
	}

	.single-service:hover p {
		color: #7E5ACE;
		text-decoration-line: none;
	}

	.single-service img {
		width: 15%;
	}

	.logo {
		filter: grayscale(100%);
		transition: .4s;
	}

	.logo:hover {
		filter: grayscale(0%);
	}

	@media (max-width: 575.98px) {
		.jumbotron-divisi {
			background-position-x: -420px;
		}

		.heading-menu {
			margin-top: 170px !important;
		}

		.single-service {
			margin-top: 30px;
		}
	}
</style>

<div class="jumbotron jumbotron-fluid jumbotron-head jumbotron-pengurus">
	<div class="container">
		<h1 class="display-4 text-center heading-menu"><?php echo $title_sub ?></h1>
		<p class="lead text-center subheading-menu"><a href="<?php echo base_url(); ?>" target="_blank"><?php echo $title ?> Study Club</a></p>
	</div>
</div>

<section>
	<div class="container">
		<div class="row">
			<?php foreach ($jabatan as $jabatan) : ?>
				<div class="col-md-4">
					<span class="icon">
						<i class="ti-agenda"></i>
					</span>
					<div class="feature-copy">
						<h3>Tahun</h3>
						<p><?php echo $jabatan->tahun_jabatan ?></p>
						<a href="<?php echo base_url('pengurus/' . $jabatan->tahun_jabatan); ?>" class="btn btn-primary btn-pengurus">Lihat</a>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</section>