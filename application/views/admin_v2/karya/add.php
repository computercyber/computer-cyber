<div class="pcoded-main-container">
	<div class="pcoded-content">

		<section class="section">

			<div class="page-header breadcumb-sticky">
				<div class="page-block">
					<div class="row align-items-center">
						<div class="col-md-12">
							<div class="page-header-title">
								<h5 class="m-b-10"><?php echo $title; ?></h5>
							</div>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard') ?>"><i class="feather icon-home mr-2"></i>Home</a></li>
								<li class="breadcrumb-item active"><a href="<?php echo site_url('admin/anggota') ?>">Anggota</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="section-body">
				<div class="card">
					<div class="card-body">
						<form action="<?php echo site_url('admin/karya/add'); ?>" method="post" enctype="multipart/form-data">
							<div class="form-row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="judul_karya">Judul Karya</label>
										<input type="text" id="judul_karya" name="judul_karya" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('judul_karya') != null) {
																															echo "is-invalid";
																														} ?>" autofocus autocomplete="off" placeholder="Cth. E-voting" value="<?php echo set_value('judul_karya'); ?>">
										<?php echo form_error('judul_karya', '<div class="invalid-feedback">', '</div>'); ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<div class="form-group">
											<label for="user_karya">Dibuat oleh / Kepala tim</label>
											<input type="text" id="user_karya" name="user_karya" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('user_karya') != null) {
																											echo "is-invalid";
																										} ?>" placeholder="Cth. Dadang Suratang" value="<?php echo set_value('user_karya'); ?>">
											<?php echo form_error('user_karya', '<div class="invalid-feedback">', '</div>'); ?>
										</div>
									</div>
								</div>
                            </div>
                            
							<div class="form-row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="jenis_karya">Jenis Karya</label>
                                        <select name="jenis_karya" id="jenis_karya" class="form-control">
                                            <option value="Perorangan" <?php echo set_value('jenis_karya') == "Perorangan" ? "selected" : null ?>>Perorangan</option>
                                            <option value="Tim" <?php echo set_value('jenis_karya') == 1 ? "Tim" : null ?>>Tim</option>
                                        </select>
										<?php echo form_error('jenis_karya', '<div class="invalid-feedback">', '</div>'); ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="gambar_karya">Gambar</label>
										<i class="ml-1 fas fa-info-circle text-primary" data-toggle="tooltip" data-placement="right" title="Usahakan memilih gambar dengan latar belakang yang bersih">
										</i>
										<div class="input-group input-group-lg" style="margin-bottom:-10px;">
											<input name="gambar_karya" class="custom-file-input <?php if (isset($error)) {
																								echo "is-invalid";
																							} ?>" type="file" class="mt-3" id="gambar">
											<label for="gambar_karya" class="custom-file-label text-muted">Seret atau pilih gambar</label>
											<?php if (isset($error)) { ?>
												<div class="invalid-feedback"><small><?php echo $error ?></small></div>
											<?php } ?>
											<small for="gambar_karya" class="form-text text-muted">
												Gambar harus berukuran 700 x 400
											</small>
										</div>
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="">Status Keanggotaan</label>
										<br>
										<div class="form-radio custom-control custom-radio custom-control-inline">
											<label class="form-check-label">
												<input type="radio" class="form-check-input" name="status_anggota" id="membershipRadios1" value="aktif" <?php if ($anggota->status_anggota == 'aktif') {
																																							echo 'checked';
																																						} ?>>Aktif</label>
										</div>
										<div class="form-radio custom-control custom-radio custom-control-inline">
											<label class="form-check-label">
												<input type="radio" class="form-check-input" name="status_anggota" id="membershipRadios2" value="purna" <?php if ($anggota->status_anggota == 'purna') {
																																							echo 'checked';
																																						} ?>>Purna</label>
										</div>
										<div class="form-radio custom-control custom-radio custom-control-inline">
											<label class="form-check-label">
												<input type="radio" class="form-check-input" name="status_anggota" id="membershipRadios2" value="keluar" <?php if ($anggota->status_anggota == 'keluar') {
																																								echo 'checked';
																																							} ?>>Keluar</label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="divisi">Divisi</label>
										<select class="form-control" name="id_divisi" required>
											<?php foreach ($divisi as $divisi) : ?>
												<option value="<?= $divisi->id_divisi ?>" <?php if ($anggota->id_divisi == $divisi->id_divisi) {
																								echo "selected";
																							} ?>>
													<?php echo $divisi->nama_divisi ?>
												</option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="jabatan">Jabatan</label>
										<select class="form-control" name="id_jabatan" required>
											<?php foreach ($jabatan as $jabatan) : ?>
												<option value="<?php echo $jabatan->id_jabatan ?>" <?php if ($anggota->id_jabatan == $jabatan->id_jabatan) {
																										echo "selected";
																									} ?>>
													<?php echo $jabatan->nama_jabatan ?> - <?php echo $jabatan->tahun_jabatan ?>
												</option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="alamat_anggota">Alamat</label>
										<textarea name="alamat_anggota" id="alamat_anggota" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('alamat_anggota') != null) {
																													echo "is-invalid";
																												} ?>" placeholder="Alamat di Tanjungpinang" rows="4"><?php echo $anggota->alamat_anggota; ?></textarea>
										<?php echo form_error('alamat_anggota', '<div class="invalid-feedback">', '</div>'); ?>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
								<button class="btn btn-primary event-btn" type="submit">
									<span class="spinner-border spinner-border-sm" role="status"></span>
									<span class="load-text">Loading...</span>
									<span class="btn-text">Update data</span>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>