<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header breadcumb-sticky">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title; ?></h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard') ?>"><i class="feather icon-home mr-2"></i>Home</a></li>
                            <li class="breadcrumb-item active">Divisi</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->session->flashdata('status'); ?>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-add-divisi" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses menambahkan divisi
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-edit-divisi" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses merubah data divisi
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-delete-divisi" class="toast hide toast-5s toast-right" style="background-color: #E43329; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #E43329; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses menghapus divisi
                </div>
            </div>
        </div>

        <section class="section">

            <?php

            //di atas list.php

            if ($this->session->flashdata('sukses')) {
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
                echo $this->session->flashdata('sukses');
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>';
            }
            ?>

            <div class="section-body">
                <a href="<?php echo base_url('admin/divisi/add') ?>" class="btn btn-primary" style="margin-bottom: 20px;"><i class="fa fa-plus"></i> Tambah</a>

                <div class="row">
                    <div class="col-md-12">

                        <div class="card user-profile-list">
                            <div class="card-body">
                                <div class="dt-responsive table-responsive">
                                    <table id="divisiTable" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>Logo</th>
                                                <th>Nama Divisi</th>
                                                <th>Keterangan Divisi</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($divisi as $divisi) :
                                            ?>
                                                <tr class="table-bordered">
                                                    <td>
                                                        <img class="align-top m-r-15 lazyload" src="<?php echo base_url() ?>assets/img/img_placeholder_cc.svg" data-src="<?php echo base_url('assets/upload/image/thumbs/divisi/' . $divisi->gambar_divisi); ?>" width="60">
                                                    </td>

                                                    <td><?php echo $divisi->nama_divisi ?></td>
                                                    <td><?php echo character_limiter($divisi->keterangan_divisi, 80); ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('admin/divisi/update/' . $divisi->id_divisi); ?>" class="img-radius btn btn-sm btn-primary"><i class="feather icon-edit-1"></i></a>
                                                        <a href="" class="img-radius btn btn-sm btn-info"><i class="feather icon-zoom-in"></i></a>
                                                        <a href="<?php echo base_url('admin/divisi/delete/' . $divisi->id_divisi); ?>" class="img-radius btn btn-sm btn-danger  tombol-hapus-divisi">
                                                            <i class="feather icon-trash-2"></i>
                                                        </a>

                                                    </td>
                                                </tr>
                                            <?php endforeach  ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#divisiTable').DataTable();
    });
</script>