<div class="pcoded-main-container">
	<div class="pcoded-content">

		<section class="section">

			<div class="page-header breadcumb-sticky">
				<div class="page-block">
					<div class="row align-items-center">
						<div class="col-md-12">
							<div class="page-header-title">
								<h5 class="m-b-10"><?php echo $title; ?></h5>
							</div>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard') ?>"><i class="feather icon-home mr-2"></i>Home</a></li>
								<li class="breadcrumb-item"><a href="<?php echo site_url('admin/gallery') ?>">Gallery</a></li>
								<li class="breadcrumb-item active"><a href="<?php echo site_url('admin/gallery/update/' . $gallery->id_gallery) ?>">Edit</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="section-body">
				<div class="card">
					<div class="card-body">
						<form action="<?php echo site_url('admin/gallery/update/' . $gallery->id_gallery); ?>" method="POST" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-8">
									<div class="form-group form-group-lg">
										<label>Judul Gambar</label>
										<input type="text" name="judul_gallery" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('judul_gallery') != null) {
																										echo "is-invalid";
																									} ?>" placeholder="Judul gambar" value="<?php echo $gallery->judul_gallery ?>">
										<?php echo form_error('judul_gallery', '<div class="invalid-feedback">', '</div>'); ?>
									</div>

									<div class="form-group form-group-sm">
										<label for="">Pilih gambar</label>
										<div class="input-group input-group-lg" style="margin-bottom:-10px;">
											<input name="gambar" class="custom-file-input <?php if (isset($error)) {
																								echo "is-invalid";
																							} ?>" type="file" class="mt-3" id="gambar">
											<label for="gambar" class="custom-file-label text-muted"><?php if ($gallery->gambar != null) {
																											echo $gallery->gambar;
																										} else {
																											echo 'Pilih atau seret gambar ...';
																										} ?></label>
											<?php if (isset($error)) { ?>
												<div class="invalid-feedback"><small><?php echo $error ?></small></div>
											<?php } ?>
											<small for="gambar" class="form-text text-muted">
												Gambar harus berukuran 1366 x 768
											</small>
										</div>
									</div>

									<div class="form-group form-group-lg">
										<label>Keterangan Gambar</label>
										<textarea name="keterangan_gallery" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('keterangan_gallery') != null) {
																									echo "is-invalid";
																								} ?>" placeholder="Keterangan gambar"><?php echo $gallery->keterangan_gallery ?></textarea>
										<?php echo form_error('keterangan_gallery', '<div class="invalid-feedback">', '</div>'); ?>
									</div>

									<div class="form-group">
										<label class="d-block">Status Berita</label>
										<div class="custom-control custom-radio">
											<input type="radio" id="customRadio1" name="status_gallery" class="custom-control-input" value="Publish" <?php if ($gallery->status_gallery == "Publish") {
																																							echo "checked";
																																						} ?>>
											<label class="custom-control-label" for="customRadio1">Publikasikan</label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" id="customRadio2" name="status_gallery" class="custom-control-input" value="Draft" <?php if ($gallery->status_gallery == "Draft") {
																																						echo "checked";
																																					} ?>>
											<label class="custom-control-label" for="customRadio2">Masuk Draft</label>
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Preview gambar sebelumnya</label>
										<img id="img-before-preview" src="<?php echo base_url() ?>assets/img/img_placeholder_cc.svg" alt="img-thumbnail" class="img-thumbnail lazyload" data-src="<?php echo base_url('assets/upload/image/thumbs/gallery/' . $gallery->gambar) ?>">
										<div class="form-text text-muted text-center">
											<?php echo $gallery->gambar; ?>
											<script>
												let img = $("#img-before-preview");

												$("<img>").attr("data-src", $(img).attr("data-src")).load(function() {
													let realWidth = this.width;
													let realHeight = this.height;


												});
												// alert("Original width=" + realWidth + ", " + "Original height=" + realHeight);
											</script>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<button class="btn btn-primary event-btn" type="submit">
											<span class="spinner-border spinner-border-sm" role="status"></span>
											<span class="load-text">Loading...</span>
											<span class="btn-text">Edit data</span>
										</button>
										<a href="<?php echo site_url('admin/gallery'); ?>" type="button" class="btn btn-default">Kembali</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>