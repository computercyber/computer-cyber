<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header breadcumb-sticky">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title; ?></h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard') ?>"><i class="feather icon-home mr-2"></i>Home</a></li>
                            <li class="breadcrumb-item active"><a href="<?php echo site_url('admin/gallery') ?>">Gallery</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->session->flashdata('status'); ?>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-add-gallery" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses menambahkan gambar di gallery
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-edit-gallery" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses mengedit data gambar di gallery
                </div>
            </div>
        </div>

        <section class="section">

            <?php

            //di atas list.php

            if ($this->session->flashdata('sukses')) {
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
                echo $this->session->flashdata('sukses');
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            }
            ?>

            <div class="section-body">
                <a href="#" data-toggle="modal" data-target="#addGalleryModal" class="btn btn-primary" style="margin-bottom: 20px;"><i class="fa fa-plus"></i> Tambah</a>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-sm table-bordered table-striped" id="galleryTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Gambar</th>
                                                <th>Judul Gambar</th>
                                                <th>Keterangan</th>
                                                <th>Tanggal</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody id="show_data">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<?php if ($this->form_validation->run() == FALSE && validation_errors('<div class="alert alert-warning">', '</div>') != null) { ?>
    <script type="text/javascript">
        $(window).on('load', function() {
            $('#addGalleryModal').modal('show');
        });
    </script>
<?php } elseif (isset($error)) { ?>
    <script type="text/javascript">
        $(window).on('load', function() {
            $('#addGalleryModal').modal('show');
        });
    </script>
<?php } ?>

<div class="modal hide fade" tabindex="-1" role="dialog" id="addGalleryModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Gambar</h5>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('admin/gallery') ?>" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Judul Gambar</label>
                                <input type="text" name="judul_gallery" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('judul_gallery') != null) {
                                                                                                echo "is-invalid";
                                                                                            } ?>" placeholder="Judul gambar" value="<?php echo set_value('judul_gallery') ?>">
                                <?php echo form_error('judul_gallery', '<div class="invalid-feedback">', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="attachment">Pilih Gambar</label>
                                <i class="ml-1 fas fa-info-circle text-primary" data-toggle="tooltip" data-placement="right" title="Usahakan memilih gambar dengan latar belakang yang bersih">
                                </i>
                                <div class="input-group input-group-lg" style="margin-bottom:-10px;">
                                    <input name="gambar" class="custom-file-input <?php if (isset($error)) {
                                                                                        echo "is-invalid";
                                                                                    } ?>" type="file" class="mt-3" id="gambar">
                                    <label for="gambar" class="custom-file-label text-muted">Pilih atau seret gambar ...</label>
                                    <?php if (isset($error)) { ?>
                                        <div class="invalid-feedback"><small><?php echo $error ?></small></div>
                                    <?php } ?>
                                    <small for="gambar" class="form-text text-muted">
                                        Gambar harus berukuran 1366 x 768
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="card shadow-none border mt-2" style="border-color: <?php if ($this->form_validation->run() == FALSE && form_error('status_gallery') != null) {
                                                                                                echo "red";
                                                                                            } ?>">
                                <div class="card-body">
                                    <label class="d-block">Status Gambar</label>
                                    <div class="form-group">
                                        <div class="radio radio-primary d-inline">
                                            <input type="radio" id="customRadio1" name="status_gallery" value="Publish">
                                            <label class="cr" for="customRadio1">Publikasikan</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="radio radio-primary d-inline">
                                            <input type="radio" id="customRadio2" name="status_gallery" value="Draft">
                                            <label class="cr" for="customRadio2">Masuk Draft</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_error('status_gallery', '<div class="invalid-feedback">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Keterangan Gambar</label>
                                <textarea name="keterangan_gallery" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('keterangan_gallery') != null) {
                                                                                            echo "is-invalid";
                                                                                        } ?>" placeholder="Keterangan gambar" rows="4"><?php echo set_value('keterangan_gallery') ?></textarea>
                                <?php echo form_error('keterangan_gallery', '<div class="invalid-feedback">', '</div>'); ?>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary event-btn" type="submit">
                    <span class="spinner-border spinner-border-sm" role="status"></span>
                    <span class="load-text">Loading...</span>
                    <span class="btn-text">Tambah gambar</span>
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        show_data_gallery();

        $('#galleryTable').dataTable();

        // fungsi tampil gallery
        function show_data_gallery() {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url(); ?>admin/gallery/data_gallery',
                async: true,
                dataType: 'json',
                success: function(data) {
                    let html = '';
                    let i;
                    for (i = 0; i < data.length; i++) {
                        html += '<tr>' +
                            '<td><img class="img-responsive rounded lazyload" src="<?php echo base_url() ?>assets/img/img_placeholder_cc.svg" data-src="<?php echo base_url(); ?>assets/upload/image/thumbs/gallery/' + data[i].gambar + '" width="60"></td>' +
                            '<td>' + data[i].judul_gallery + '<br><small title="author">' + data[i].nama + '</small></td>' +
                            '<td>' + data[i].keterangan_gallery + '</td>' +
                            '<td>' + data[i].tanggal_gallery + '</td>' +
                            '<td>' + data[i].status_gallery + '</td>' +
                            '<td style="text-align:right;">' +
                            '<a href="javascript:;" class="img-radius btn btn-sm btn-info item_show" data="' + data[i].id_gallery + '"><i class="feather icon-zoom-in"></i></a>' +
                            '<a href="javascript:;" class="ml-1 img-radius btn btn-sm btn-primary item_edit" data="' + data[i].id_gallery + '"><i class="feather icon-edit-1"></i></a>' +
                            '<a href="javascript:;" class="ml-1 img-radius btn btn-sm btn-danger item_delete" data="' + data[i].id_gallery + '"><i class="feather icon-trash-2"></i></a>' +
                            '</td>' +
                            '</tr>';

                    }
                    $('#show_data').html(html);
                }

            });
        }

        $('#show_data').on('click', '.item_show', function() {
            let id = $(this).attr('data');
            $.ajax({
                type: 'GET',
                url: '<?php echo site_url(); ?>admin/gallery/get_gallery'
            });
        });

    });
</script>