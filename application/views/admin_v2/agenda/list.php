<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header breadcumb-sticky">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title; ?></h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard') ?>"><i class="feather icon-home mr-2"></i>Home</a></li>
                            <li class="breadcrumb-item active"><a href="<?php echo site_url('admin/agenda') ?>">Agenda dan Event</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->session->flashdata('status'); ?>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-auto-emailer" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses mengaktifkan Pengiriman email otomatis
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-add-agenda" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses menambahkan agenda baru
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-edit-agenda" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">{ elapsed_time }</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses mengubah info agenda
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-delete-agenda" class="toast hide toast-5s toast-right" style="background-color: #E43329; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #E43329; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses menghapus agenda
                </div>
            </div>
        </div>

        <?php if ($konfigurasi->auto_emailer_agenda == 0) { ?>
            <div class="alert alert-info shadow-sm mt-3 rounded fade show" role="alert">
                <h4 class="alert-heading">Tips efektif <span class="pcoded-micon"><i class="ml-1 feather icon-sun"></i></h4>
                <p>Pengurus sudah membuat agenda tapi terkadang peserta banyak yang tidak datang? Pasti mengesalkan bukan. 8 dari 10 orang mengatakan bahwa mereka lupa akan agenda tersebut atau tidak membaca pengumuman di grup. Maka dari itu Kami akan membantu anda untuk mengingatkan para anggota aktif dengan cara mengirimkan email ke mereka. Mohon untuk mengaktifkan pengiriman email otomatis pada tiap agenda yang akan diselenggarakan</p>
                <hr>
                <a href="<?php echo site_url('admin/agenda/konfigurasi_auto_emailer'); ?>" class="btn btn-info btn-sm">Aktifkan</a>
            </div>
        <?php } ?>


        <section class="section">
            <?php echo $this->session->flashdata('message'); ?>
            <!-- flashdata untuk data berhasil -->

            <div class="section-body">
                <a href="#" data-toggle="modal" data-target="#addModal" class="btn btn-primary" style="margin-bottom: 20px;"><i class="fa fa-plus"></i> Tambah</a>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-sm table-hover" id="dataEvent" width="100%" cellspacing="0">
                                        <caption>All Event</caption>
                                        <thead>
                                            <tr>
                                                <th width="5">No</th>
                                                <th>Aksi</th>
                                                <th>Nama Agenda</th>
                                                <th>Tanggal Mulai</th>
                                                <th>Tanggal Selesai</th>
                                                <th>Undangan</th>
                                                <th>Lokasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($agenda as $agenda) :
                                            ?>
                                                <tr>
                                                    <td><?php echo $no++; ?></td>
                                                    <td>
                                                        <div class="btn-group mb-2 mr-2">
                                                            <button class="btn  btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</button>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item" href="#!"><i class="feather icon-edit-2 mr-1"></i> Edit</a>
                                                                <a class="dropdown-item tombol-hapus-agenda" href="<?php echo site_url('admin/agenda/delete/' . urlencode(encrypt_url($agenda->id_agenda))); ?>">
                                                                    <i class="far fa-trash-alt mr-1"></i> Hapus</a>
                                                                <a class="dropdown-item" href="<?php echo site_url('admin/agenda/changestatus/' . urlencode(encrypt_url('tunda')) . '/' . urlencode(encrypt_url($agenda->id_agenda)));  ?>"><i class="feather icon-pause-circle mr-1"></i> Tunda Acara</a>
                                                            </div>
                                                        </div>
                                                        </a>
                                                    </td>

                                                    <td><?php echo $agenda->nama_agenda ?>
                                                        <br>
                                                        <small><strong><?php echo $agenda->status; ?></strong></small>
                                                    </td>
                                                    <td><?php echo date('d M Y', strtotime($agenda->tanggal_mulai)); ?></td>
                                                    <td><?php echo date('d M Y', strtotime($agenda->tanggal_selesai)); ?></td>
                                                    <td><?php echo $agenda->undangan ?></td>
                                                    <td><?php echo $agenda->lokasi ?></td>

                                                </tr>
                                            <?php endforeach  ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php if ($this->form_validation->run() == FALSE && validation_errors('<div class="alert alert-warning">', '</div>') != null) { ?>
        <script type="text/javascript">
            $(window).on('load', function() {
                $('#addModal').modal('show');
            });
        </script>
    <?php } ?>

    <div class="modal hide fade" tabindex="-1" role="dialog" id="addModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Agenda</h5>
                </div>
                <div class="modal-body">
                    <form class="form" action="<?php echo site_url('admin/agenda') ?>" method="POST">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama_agenda">Nama Agenda</label>
                                    <input type="text" name="nama_agenda" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('nama_agenda') != null) {
                                                                                                    echo "is-invalid";
                                                                                                } ?>" placeholder="Cth. Makrab CC <?php echo date('Y'); ?>" autofocus value="<?php echo set_value('nama_agenda'); ?>">
                                    <?php echo form_error('nama_agenda', '<div class="invalid-feedback">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_mulai">Tanggal Mulai</label>
                                    <input type="date" name="tanggal_mulai" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('tanggal_mulai') != null) {
                                                                                                    echo "is-invalid";
                                                                                                } ?>" value="<?php echo set_value('tanggal_mulai'); ?>">
                                    <?php echo form_error('tanggal_mulai', '<div class="invalid-feedback">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lokasi">Lokasi</label>
                                    <input type="text" name="lokasi" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('lokasi') != null) {
                                                                                                echo "is-invalid";
                                                                                            } ?>" placeholder="Cth. Trikora 4 Km 40" value="<?php echo set_value('lokasi'); ?>">
                                    <?php echo form_error('lokasi', '<div class="invalid-feedback">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_selesai">Tanggal Selesai</label>
                                    <input type="date" name="tanggal_selesai" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('tanggal_selesai') != null) {
                                                                                                        echo "is-invalid";
                                                                                                    } ?>" value="<?php echo set_value('tanggal_selesai'); ?>">
                                    <?php echo form_error('tanggal_selesai', '<div class="invalid-feedback">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="undangan">Undangan untuk</label>
                                    <input type="text" name="undangan" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('undangan') != null) {
                                                                                                echo "is-invalid";
                                                                                            } ?>" placeholder="Cth. Anggota aktif <?php echo date('Y'); ?>" value="<?php echo set_value('undangan'); ?>">
                                    <?php echo form_error('undangan', '<div class="invalid-feedback">', '</div>'); ?>
                                    <small class="form-text text-muted">
                                        Undangan ditujukan untuk peserta yang dapat hadir dalam agenda/event tersebut. Cth. <strong>Semua anggota</strong> berarti semua anggota dapat hadir. Atau bisa juga <strong>Anggota aktif</strong>, <strong>Calon anggota</strong>, dll.
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="keterangan">Keterangan</label>
                                    <textarea name="keterangan" class="form-control" placeholder="Tulis apabila butuh keterangan kegiatan" rows="4"><?php echo set_value('nama_agenda'); ?></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary event-btn" type="submit">
                                <span class="spinner-border spinner-border-sm" role="status"></span>
                                <span class="load-text">Loading...</span>
                                <span class="btn-text">Tambah agenda</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#dataEvent').dataTable();

    });
</script>