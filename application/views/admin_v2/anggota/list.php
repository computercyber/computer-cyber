<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header breadcumb-sticky">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title; ?></h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard') ?>"><i class="feather icon-home mr-2"></i>Home</a></li>
                            <li class="breadcrumb-item active"><a href="<?php echo site_url('admin/anggota') ?>">Anggota</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->session->flashdata('status'); ?>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-add-anggota" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses menambahkan anggota
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-edit-anggota" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses merubah data anggota
                </div>
            </div>
        </div>

        <div class="border-0" style="position:absolute;top:40px;right: 40px">
            <div id="toast-delete-anggota" class="toast hide toast-5s toast-right" style="background-color: #E43329; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                <div class="toast-header border-0" style="background-color: #E43329; opacity:.8;">
                    <strong class="mr-auto text-white">Success</strong>
                    <small class="text-white-50 mr-2">1 second ago</small>
                    <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                        <span class="text-white">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-white">
                    Sukses menghapus anggota
                </div>
            </div>
        </div>

        <section class="section">

            <?php

            //di atas list.php

            if ($this->session->flashdata('sukses')) {
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
                echo $this->session->flashdata('sukses');
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>';
            }
            ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="section-body">

                        <a href="#" data-toggle="modal" data-target="#addAnggotaModal" class="btn btn-primary" style="margin-bottom: 20px;"><i class="fa fa-plus"></i> Tambah</a>

                        <div class="card user-profile-list">
                            <div class="card-body">
                                <div class="dt-responsive table-responsive">
                                    <table id="anggotaTable" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Jabatan</th>
                                                <th>E-mail</th>
                                                <th>No hp</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($anggota as $anggota) :
                                            ?>
                                                <tr class="table-bordered">
                                                    <td>
                                                        <div class="d-inline-block align-middle">
                                                            <img class="img-radius align-top m-r-15 lazyload" src="<?php echo base_url() ?>assets/img/img_placeholder_cc.svg" data-src="<?php echo base_url() ?>assets/upload/image/thumbs/anggota/<?php echo $anggota->gambar; ?>" width="40">
                                                            <div class="d-inline-block">
                                                                <h6 class="m-b-0"><?= $anggota->nama_anggota; ?></h6>
                                                                <small class="m-b-0"><?= $anggota->nama_divisi; ?></small>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><?php echo $anggota->nama_jabatan; ?><br><small><?php echo $anggota->tahun_jabatan; ?></small></td>
                                                    <td><?= $anggota->email_anggota; ?></td>
                                                    <td><?= $anggota->no_hp; ?></td>
                                                    <td><span class="badge <?php if ($anggota->status_anggota == "aktif") {
                                                                                echo 'badge-light-success';
                                                                            } elseif ($anggota->status_anggota == "purna") {
                                                                                echo 'badge-light-warning';
                                                                            } else {
                                                                                echo 'badge-light-danger';
                                                                            } ?> "><?php echo $anggota->status_anggota ?></span>
                                                    <td>
                                                        <a href="<?php echo site_url() ?>admin/anggota/update/<?php echo $anggota->id_anggota ?>" class="img-radius btn btn-sm btn-primary"><i class="feather icon-edit-1"></i></a>
                                                        <a href="<?php echo site_url() ?>admin/anggota/detail/<?php echo $anggota->id_anggota ?>" class="img-radius btn btn-sm btn-info"><i class=" feather icon-zoom-in"></i></a>
                                                        <a href="<?php echo site_url('admin/anggota/delete/' . $anggota->id_anggota) ?>" class="img-radius btn btn-sm btn-danger tombol-hapus-anggota">
                                                            <i class="feather icon-trash-2"></i>
                                                        </a>
                                                    </td>
                                </div>
                                </tr>
                            <?php endforeach  ?>
                            </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<?php if ($this->form_validation->run() == FALSE && validation_errors('<div class="alert alert-warning">', '</div>') != null) { ?>
    <script type="text/javascript">
        $(window).on('load', function() {
            $('#addAnggotaModal').modal('show');
        });
    </script>
<?php } elseif (isset($error)) { ?>
    <script type="text/javascript">
        $(window).on('load', function() {
            $('#addAnggotaModal').modal('show');
        });
    </script>
<?php } ?>


<div class="modal hide fade" tabindex="-1" role="dialog" id="addAnggotaModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Anggota</h5>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('admin/anggota'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama_anggota">Nama Lengkap</label>
                                <input type="text" id="nama_anggota" name="nama_anggota" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('nama_anggota') != null) {
                                                                                                                    echo "is-invalid";
                                                                                                                } ?>" autofocus autocomplete="off" placeholder="Cth. Dadang Suratang" value="<?php echo set_value('nama_anggota'); ?>">
                                <?php echo form_error('nama_anggota', '<div class="invalid-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="nim">NIM</label>
                                    <input type="text" id="nim" name="nim" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('nim') != null) {
                                                                                                    echo "is-invalid";
                                                                                                } ?>" placeholder="170155..." value="<?php echo set_value('nim'); ?>">
                                    <?php echo form_error('nim', '<div class="invalid-feedback">', '</div>'); ?>
                                    <div id="nim-status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tahun_masuk_anggota">Tahun Masuk Anggota</label>
                                <input type="number" id="tahun_masuk_anggota" name="tahun_masuk_anggota" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('tahun_masuk_anggota') != null) {
                                                                                                                                    echo "is-invalid";
                                                                                                                                } ?>" placeholder="20..." maxlength="4" value="<?php echo set_value('tahun_masuk_anggota'); ?>">
                                <?php echo form_error('tahun_masuk_anggota', '<div class="invalid-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input type="date" id="tanggal_lahir" name="tanggal_lahir" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('tanggal_lahir') != null) {
                                                                                                                        echo "is-invalid";
                                                                                                                    } ?>" value="<?php echo set_value('tanggal_lahir'); ?>">
                                    <?php echo form_error('tanggal_lahir', '<div class="invalid-feedback">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Jenis Kelamin Anggota</label>
                                <br>
                                <div class="form-radio custom-control custom-radio custom-control-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="jenis_kelamin_anggota" id="membershipRadios1" value="pria" <?php echo set_value('jenis_kelamin_anggota') == 'pria' ? "checked" : null ?>>Pria</label>
                                </div>
                                <div class="form-radio custom-control custom-radio custom-control-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="jenis_kelamin_anggota" id="membershipRadios2" value="wanita" <?php echo set_value('jenis_kelamin_anggota') == 'wanita' ? "checked" : null ?>>Wanita</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email_anggota">Email Anggota</label>
                                <input type="email" name="email_anggota" id="email_anggota" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('email_anggota') != null) {
                                                                                                                    echo "is-invalid";
                                                                                                                } ?>" placeholder="mail@domain.com" value="<?php echo set_value('email_anggota'); ?>">
                                <?php echo form_error('email_anggota', '<div class="invalid-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jurusan">Jurusan</label>
                                <select class="form-control" required name="jurusan">
                                    <option selected disabled>-- Jurusan --</option>
                                    <optgroup label="Teknik">
                                        <option value="1" <?php echo set_value('jurusan') == 1 ? "selected" : null ?>>Teknik Informatika</option>
                                        <option value="2" <?php echo set_value('jurusan') == 2 ? "selected" : null ?>>Teknik Elektro</option>
                                    </optgroup>
                                    <optgroup label="Ekonomi">
                                        <option value="3" <?php echo set_value('jurusan') == 3 ? "selected" : null ?>>Akuntansi</option>
                                        <option value="4" <?php echo set_value('jurusan') == 4 ? "selected" : null ?>>Manajemen</option>
                                    </optgroup>
                                    <optgroup label="Fakultas Ilmu Kelautan dan Perikanan">
                                        <option value="5" <?php echo set_value('jurusan') == 5 ? "selected" : null ?>>Ilmu Kelautan</option>
                                        <option value="6" <?php echo set_value('jurusan') == 6 ? "selected" : null ?>>Manajemen Sumberdaya Perairan</option>
                                        <option value="7" <?php echo set_value('jurusan') == 7 ? "selected" : null ?>>Budidaya Perairan</option>
                                        <option value="8" <?php echo set_value('jurusan') == 8 ? "selected" : null ?>>Teknologi Hasil Perikanan</option>
                                        <option value="9" <?php echo set_value('jurusan') == 9 ? "selected" : null ?>>Sosial Ekonomi Perikanan</option>
                                    </optgroup>
                                    <optgroup label="Fakultas Keguruan dan Ilmu Pendidikan">
                                        <option value="10" <?php echo set_value('jurusan') == 10 ? "selected" : null ?>>Pendidikan Bahasa dan Sastra Indonesia</option>
                                        <option value="11" <?php echo set_value('jurusan') == 11 ? "selected" : null ?>>Pendidikan Bahasa Inggris</option>
                                        <option value="12" <?php echo set_value('jurusan') == 12 ? "selected" : null ?>>Pendidikan Kimia</option>
                                        <option value="13" <?php echo set_value('jurusan') == 13 ? "selected" : null ?>>Pendidikan Biologi</option>
                                        <option value="14" <?php echo set_value('jurusan') == 14 ? "selected" : null ?>>Pendidikan Matematika</option>
                                    </optgroup>
                                    <optgroup label="Fakultas Ilmu Sosial dan Ilmu Politik">
                                        <option value="15" <?php echo set_value('jurusan') == 15 ? "selected" : null ?>>Imu Administrasi Negara</option>
                                        <option value="16" <?php echo set_value('jurusan') == 16 ? "selected" : null ?>>Ilmu Pemerintahan</option>
                                        <option value="17" <?php echo set_value('jurusan') == 17 ? "selected" : null ?>>Sosiologi</option>
                                        <option value="18" <?php echo set_value('jurusan') == 18 ? "selected" : null ?>>Ilmu Hukum</option>
                                        <option value="19" <?php echo set_value('jurusan') == 19 ? "selected" : null ?>>Hubungan Internasional</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="no_hp">No Hp</label>
                                <input type="number" name="no_hp" id="no_hp" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('no_hp') != null) {
                                                                                                        echo "is-invalid";
                                                                                                    } ?>" maxlength="14" placeholder="+628 ..." value="<?php echo set_value('no_hp'); ?>">
                                <?php echo form_error('no_hp', '<div class="invalid-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="gambar">Gambar</label>
                                <i class="ml-1 fas fa-info-circle text-primary" data-toggle="tooltip" data-placement="right" title="Usahakan memilih gambar dengan latar belakang yang bersih">
                                </i>
                                <div class="input-group input-group-lg" style="margin-bottom:-10px;">
                                    <input name="gambar" class="custom-file-input <?php if (isset($error)) {
                                                                                        echo "is-invalid";
                                                                                    } ?>" type="file" class="mt-3" id="gambar">
                                    <label for="gambar" class="custom-file-label text-muted">Pilih atau seret gambar ...</label>
                                    <?php if (isset($error)) { ?>
                                        <div class="invalid-feedback"><small><?php echo $error ?></small></div>
                                    <?php } ?>
                                    <small for="gambar" class="form-text text-muted">
                                        Gambar harus berukuran 400 x 400
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Status Keanggotaan</label>
                                <br>
                                <div class="form-radio custom-control custom-radio custom-control-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status_anggota" id="membershipRadios1" value="aktif" <?php echo set_value('status_anggota') == 'aktif' ? "checked" : null ?>>Aktif</label>
                                </div>
                                <div class="form-radio custom-control custom-radio custom-control-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status_anggota" id="membershipRadios2" value="purna" <?php echo set_value('status_anggota') == 'purna' ? "checked" : null ?>>Purna</label>
                                </div>
                                <div class="form-radio custom-control custom-radio custom-control-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status_anggota" id="membershipRadios2" value="keluar" <?php echo set_value('status_anggota') == 'keluar' ? "checked" : null ?>>Keluar</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="divisi">Divisi</label>
                                <select class="form-control" name="id_divisi" required>
                                    <option selected disabled>-- Divisi --</option>
                                    <?php foreach ($divisi as $divisi) : ?>
                                        <option value="<?php echo $divisi->id_divisi ?>" <?php echo set_value('id_divisi') == $divisi->id_divisi ? "selected" : null ?>>
                                            <?php echo $divisi->nama_divisi ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jabatan">Jabatan</label>
                                <select class="form-control" name="id_jabatan" required>
                                    <option selected disabled>-- Jabatan --</option>
                                    <?php foreach ($jabatan as $jabatan) : ?>
                                        <option value="<?php echo $jabatan->id_jabatan ?>" <?php echo set_value('id_jabatan') == $jabatan->id_jabatan ? "selected" : null ?>>
                                            <?php echo $jabatan->nama_jabatan ?> - <?php echo $jabatan->tahun_jabatan ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="alamat_anggota">Alamat</label>
                                <textarea name="alamat_anggota" id="alamat_anggota" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('alamat_anggota') != null) {
                                                                                                            echo "is-invalid";
                                                                                                        } ?>" placeholder="Alamat di Tanjungpinang" rows="4"><?php echo set_value('alamat_anggota') ?></textarea>
                                <?php echo form_error('alamat_anggota', '<div class="invalid-feedback">', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary event-btn" type="submit">
                            <span class="spinner-border spinner-border-sm" role="status"></span>
                            <span class="load-text">Loading...</span>
                            <span class="btn-text">Tambah anggota</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        $('#anggotaTable').DataTable();

        $('#nim').on('keyup', function() {

            const keyword = $(this).val();

            $.ajax({
                url: '<?php echo site_url('admin/anggota/check_nim'); ?>',
                data: {
                    nim: keyword
                },
                method: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    if (data.status == 1) {
                        $('#nim').removeClass('is-invalid');
                        $('#nim-status').addClass('invalid-feedback').html('NIM ' + data.nim + ' ' + data.message);
                        console.log(data.status);
                    } else {
                        $('#nim').addClass('is-invalid');
                        $('#nim-status').addClass('invalid-feedback').html('NIM ' + data.nim + ' ' + data.message);
                        console.log(data.status);
                    }
                }
            });
        });
    });
</script>