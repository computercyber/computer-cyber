<div class="pcoded-main-container">
    <div class="pcoded-content">

        <div class="page-header breadcumb-sticky">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title; ?></h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard') ?>"><i class="feather icon-home mr-2"></i>Home</a></li>
                            <li class="breadcrumb-item active"><a href="<?php echo site_url('admin/gallery') ?>">Gallery</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <section class="section">

            <?php echo $this->session->flashdata('status'); ?>

            <?php if ($this->form_validation->run() == FALSE && validation_errors('<div class="alert alert-warning">', '</div>') != null) { ?>
                <script type="text/javascript">
                    $(window).on('load', function() {
                        $('#addPembinaModal').modal('show');
                    });
                </script>
            <?php } ?>

            <div class="border-0" style="position:absolute;top:40px;right: 40px">
                <div id="toast-add-pembina" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                    <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                        <strong class="mr-auto text-white">Success</strong>
                        <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                        <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                            <span class="text-white">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body text-white">
                        Sukses menambahkan pembina
                    </div>
                </div>
            </div>

            <div class="border-0" style="position:absolute;top:40px;right: 40px">
                <div id="toast-edit-pembina" class="toast hide toast-5s toast-right" style="background-color: #00B75B; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                    <div class="toast-header border-0" style="background-color: #00B75B; opacity:.8;">
                        <strong class="mr-auto text-white">Success</strong>
                        <small class="text-white-50 mr-2">{elapsed_time} second ago</small>
                        <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                            <span class="text-white">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body text-white">
                        Sukses mengubah pembina
                    </div>
                </div>
            </div>

            <div class="border-0" style="position:absolute;top:40px;right: 40px">
                <div id="toast-delete-pembina" class="toast hide toast-5s toast-right" style="background-color: #E43329; opacity:.8;" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true">
                    <div class="toast-header border-0" style="background-color: #E43329; opacity:.8;">
                        <strong class="mr-auto text-white">Success</strong>
                        <small class="text-white-50 mr-2">{elapsed_time} seconds ago</small>
                        <button type="button" class="m-l-5 mb-1 mt-1 close text-white" data-dismiss="toast" aria-label="Close">
                            <span class="text-white">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body text-white">
                        Sukses menghapus pembina
                    </div>
                </div>
            </div>

            <div class="section-body">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addPembinaModal" style="margin-bottom: 20px;">
                    <i class="fa fa-plus"></i> Tambah
                </button>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-sm table-bordered table-striped" id="dataPembina" width="auto" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th width="40px">Aksi</th>
                                                <th width="360px">Nama</th>
                                                <th width="100px">Jenis Pembina</th>
                                                <th width="100px">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($pembina as $pembina) :
                                            ?>
                                                <tr class="table-bordered">
                                                    <td><?php echo $no++; ?></td>
                                                    <td width="10">
                                                        <div class="btn-group mb-2 mr-2">
                                                            <button class="btn  btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</button>
                                                            <div class="dropdown-menu">
                                                                <a href="javascript:;" class="dropdown-item edit-pembina" data-toggle="modal" data-target="#editPembinaModal" data-id="<?php echo $pembina->id_pembina; ?>"><i class="feather icon-edit-2 mr-2"></i>Edit</a>
                                                                <a href="<?php echo site_url('admin/jabatan/delete/' . $pembina->id_pembina); ?>" class="dropdown-item"><i class="far fa-trash-alt mr-3"></i>Delete</a>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <div class="media">
                                                            <img src="<?php echo base_url('assets/upload/image/thumbs/pembina/' . $pembina->gambar_pembina); ?>" class="mr-3 rounded-circle" alt="<?php echo $pembina->nama_pembina; ?>" width="60px">
                                                            <div class="media-body mt-2">
                                                                <h6 class="mt-0"><?php echo $pembina->nama_pembina; ?></h6>
                                                                <small><?php echo $pembina->dosen ?></small>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><?php echo $pembina->jenis_pembina ?></td>
                                                    <td><?php if ($pembina->status_pembina == 'Aktif') { ?>
                                                            <span class="badge badge-light-success">Aktif</span>
                                                        <?php } elseif ($pembina->status_pembina == 'Non aktif') { ?>
                                                            <span class="badge badge-light-secondary">Non aktif</span>
                                                        <?php } elseif ($pembina->status_pembina == 'Pindah') { ?>
                                                            <span class="badge badge-light-warning">Pindah</span>
                                                        <?php } elseif ($pembina->status_pembina == 'Mengundurkan diri') { ?>
                                                            <span class="badge badge-light-secondary">Mengundurkan diri</span>
                                                        <?php } ?>

                                                    </td>
                                                </tr>
                                            <?php endforeach  ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <!--End Advanced Tables -->

    <div class="modal fade" tabindex="-1" role="dialog" id="addPembinaModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addPembinaLabel">Tambah Data</h5>
                </div>
                <form action="<?php echo site_url('admin/pembina') ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama_pembina">Nama Pembina</label>
                                    <input type="text" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('nama_pembina') != null) {
                                                                                echo "is-invalid";
                                                                            } ?>" id="nama_pembina" name="nama_pembina" placeholder="Nama pembina" value="<?php echo set_value('nama_pembina'); ?>">
                                    <?php echo form_error('nama_pembina', '<div class="invalid-feedback">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dosen">Dosen</label>
                                    <input type="text" id="dosen" name="dosen" class="form-control" placeholder="Cth. Teknik Informatika" value="<?php echo set_value('dosen'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis_pembina">Jenis Pembina</label>
                                    <input type="text" class="form-control" id="jenis_pembina" name="jenis_pembina" placeholder="Cth. Pembina 1" value="<?php echo set_value('jenis_pembina'); ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="gambar_pembina">Gambar</label>
                                    <i class="ml-1 fas fa-info-circle text-primary" data-toggle="tooltip" data-placement="right" title="Usahakan memilih gambar dengan latar belakang yang bersih">
                                    </i>
                                    <div class="input-group input-group-lg" style="margin-bottom:-10px;">
                                        <input name="gambar_pembina" class="custom-file-input <?php if (isset($error)) {
                                                                                                    echo "is-invalid";
                                                                                                } ?>" type="file" class="mt-3" id="gambar_pembina">
                                        <label for="gambar_pembina" class="custom-file-label text-muted">Pilih atau seret gambar ...</label>
                                        <?php if (isset($error)) { ?>
                                            <div class="invalid-feedback"><small><?php echo $error ?></small></div>
                                        <?php } ?>
                                        <small for="gambar_pembina" class="form-text text-muted">
                                            Gambar harus berukuran 400 x 400
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email_pembina">E-mail</label>
                                    <input type="email" id="email_pembina" name="email_pembina" class="form-control" value="<?php echo set_value('email_pembina'); ?>" placeholder="name@domain.com">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status_pembina">Status Pembina</label>
                                    <select name="status_pembina" id="status_pembina" class="form-control">
                                        <option disabled selected>Status Pembina</option>
                                        <option value="Aktif" <?php echo set_value('status_pembina') == 'Aktif' ? "selected" : null ?>>Aktif</option>
                                        <option value="Non aktif" <?php echo set_value('status_pembina') == 'Non aktif' ? "selected" : null ?>>Non aktif</option>
                                        <option value="Pindah" <?php echo set_value('status_pembina') == 'Pindah' ? "selected" : null ?>>Pindah</option>
                                        <option value="Mengundurkan diri" <?php echo set_value('status_pembina') == 'Mengundurkan diri' ? "selected" : null ?>>Mengundurkan diri</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="modal fade" tabindex="-1" role="dialog" id="editPembinaModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editPembinaLabel">Edit Data</h5>
                </div>
                <form action="<?php echo site_url('admin/pembina/update/' . $this->session->flashdata('id_pembina')) ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row pb-1 pt-1 row-profile-pembina">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <img id="edit-img-profile-pembina" class="rounded-circle" alt="" width="100px">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama_pembina">Nama Pembina</label>
                                    <input type="text" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('nama_pembina') != null) {
                                                                                echo "is-invalid";
                                                                            } ?>" id="edit-nama_pembina" name="nama_pembina" placeholder="Nama pembina" value="<?php echo set_value('nama_pembina'); ?>">
                                    <?php echo form_error('nama_pembina', '<div class="invalid-feedback">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dosen">Dosen</label>
                                    <input type="text" id="edit-dosen" name="dosen" class="form-control" placeholder="Cth. Teknik Informatika" value="<?php echo set_value('dosen'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis_pembina">Jenis Pembina</label>
                                    <input type="text" class="form-control" id="edit-jenis_pembina" name="jenis_pembina" placeholder="Cth. Pembina 1" value="<?php echo set_value('jenis_pembina'); ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="gambar_pembina">Gambar</label>
                                    <i class="ml-1 fas fa-info-circle text-primary" data-toggle="tooltip" data-placement="right" title="Usahakan memilih gambar dengan latar belakang yang bersih">
                                    </i>
                                    <div class="input-group input-group-lg" style="margin-bottom:-10px;">
                                        <input name="gambar_pembina" class="custom-file-input <?php if (isset($error)) {
                                                                                                    echo "is-invalid";
                                                                                                } ?>" type="file" class="mt-3" id="gambar_pembina">
                                        <label for="gambar_pembina" class="custom-file-label text-muted">Pilih atau seret gambar ...</label>
                                        <?php if (isset($error)) { ?>
                                            <div class="invalid-feedback"><small><?php echo $error ?></small></div>
                                        <?php } ?>
                                        <small for="gambar_pembina" class="form-text text-muted">
                                            Gambar harus berukuran 400 x 400
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email_pembina">E-mail</label>
                                    <input type="email" id="edit-email_pembina" name="email_pembina" class="form-control" value="<?php echo set_value('email_pembina'); ?>" placeholder="name@domain.com">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status_pembina">Status Pembina</label>
                                    <select name="status_pembina" id="edit-status_pembina" class="form-control">
                                        <option disabled selected>Status Pembina</option>
                                        <option value="Aktif" <?php echo set_value('status_pembina') == 'Aktif' ? "selected" : null ?>>Aktif</option>
                                        <option value="Non aktif" <?php echo set_value('status_pembina') == 'Non aktif' ? "selected" : null ?>>Non aktif</option>
                                        <option value="Pindah" <?php echo set_value('status_pembina') == 'Pindah' ? "selected" : null ?>>Pindah</option>
                                        <option value="Mengundurkan diri" <?php echo set_value('status_pembina') == 'Mengundurkan diri' ? "selected" : null ?>>Mengundurkan diri</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataPembina').dataTable();

        $('.edit-pembina').on('click', function() {

            const id = $(this).data('id');

            $('.modal-content form').attr('action', "<?php echo site_url('admin/pembina/update/'); ?>" + id);

            $.ajax({
                url: '<?php echo site_url('admin/pembina/get_data_pembina'); ?>',
                data: {
                    id: id
                },
                method: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $('#edit-nama_pembina').val(data.nama_pembina);
                    $('#edit-dosen').val(data.dosen);
                    $('#edit-jenis_pembina').val(data.jenis_pembina);
                    $('#edit-email_pembina').val(data.email_pembina);
                    $('#edit-status_pembina').val(data.status_pembina);
                    $('#edit-img-profile-pembina').attr("src", '<?php echo base_url(); ?>assets/upload/image/thumbs/pembina/' + data.gambar_pembina);
                }
            });
        })
    });
</script>