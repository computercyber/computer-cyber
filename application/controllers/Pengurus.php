<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengurus extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('anggota_model');
		$this->load->model('jabatan_model');
	}

	public function index()
	{
		$site=$this->konfigurasi_model->listing();
		$jabatan=$this->jabatan_model->jabatan_group();
		$data = array(
			'title' => $site->namaweb,
			'title_sub' => 'PENGURUS',
			'isi'	=> 'home/pengurus',
			'jabatan' => $jabatan );
		$this->load->view('layout/wrapper', $data); 
	}

	public function pengurus_anggota($tahun_jabatan)
	{
		$site=$this->konfigurasi_model->kategori1();
		$jabatan=$this->jabatan_model->jabatan_tahun($tahun_jabatan);
		$data = array(
			'title' => $site->namaweb,
			'title_sub' => 'PENGURUS',
			'isi'	=> 'home/anggota',
			'jabatan' => $jabatan );
		$this->load->view('layout/wrapper', $data); 
	}

}

/* End of file pengurus.php */
/* Location: ./application/controllers/pengurus.php */