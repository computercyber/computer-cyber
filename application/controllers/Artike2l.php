<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Berita extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('berita_model');
    }

    public function index()
    {
        // ini adalah list berita
        $site = $this->konfigurasi_model->listing();
        $berita_all = $this->berita_model->berita_all();
        $data = array(
            'title' => $site->namaweb,
            'title_sub' => 'Berita',
            'isi'     => 'home/berita',
            'berita_all' => $berita_all
        );
        $this->load->view('layout/wrapper', $data);
    }

    public function detail($url)
    {
        $site = $this->konfigurasi_model->listing();
        $berita_detail = $this->berita_model->berita_detail($url);
        $id_berita = $this->db->get_where('berita', array('url' => $url))->row()->id_berita;

        $tag = $this->berita_model->tag_berita($id_berita);

        $data = array(
            'title' => $site->namaweb,
            'title_sub' => 'PORTFOLIO',
            'isi'    => 'home/detail_berita',
            'berita_detail' => $berita_detail,
            'tag'   => $tag
        );
        $this->load->view('layout/wrapper', $data);
    }
}
