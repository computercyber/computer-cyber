<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oops extends CI_Controller {

	public function index()
	{
		$site=$this->konfigurasi_model->listing();
		$data = array(
			'title' => $site->namaweb,
			'title_sub' => '404',
			'isi' => 'home/oops' );
		$this->load->view('layout/wrapper', $data, FALSE);
	}

}

/* End of file oops.php */
/* Location: ./application/controllers/oops.php */