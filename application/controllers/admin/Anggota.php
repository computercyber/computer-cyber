<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anggota extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('anggota_model');
		$this->load->model('konfigurasi_model');

		//relasi dengan...
		$this->load->model('divisi_model');
		$this->load->model('jabatan_model');
	}
	public function index()
	{
		// user login
		$id_user = $this->session->userdata('id');
		$user_login = $this->user_model->detail($id_user);

		$site_config = $this->konfigurasi_model->listing();
		$anggota = $this->anggota_model->listing();
		$divisi = $this->divisi_model->listing();
		$jabatan = $this->jabatan_model->listing();

		//validasi
		$v = $this->form_validation;
		$v->set_rules('nama_anggota', 'Nama Anggota', 'required', array('required' => 'Nama Harus diisi'));
		$v->set_rules('nim', 'NIM', 'required|is_unique[anggota.nim]', array('required' => 'Nim Harus di Isi', 'is_unique' => 'NIM <strong>' . $this->input->post('nim') . '</strong> sudah digunakan'));
		$v->set_rules('tahun_masuk_anggota', 'Tahun Masuk Anggota', 'required|numeric', array('required' => 'Tahun Masuk Anggota Harus di Isi'));
		// $v->set_rules('jenis_kelamin_anggota', 'Jenis Kelamin', 'required', array('required' => 'Jenis Kelamin harus diisi'));
		$v->set_rules('email_anggota', 'Email Anggota', 'required|valid_email|is_unique[anggota.email_anggota]', array('required' => 'Email Harus di Isi', 'valid_email' => 'Email tidak valid', 'is_unique' => 'Email <strong>' . $this->input->post('email_anggota') . '</strong> sudah digunakan'));
		$v->set_rules('jurusan', 'Jurusan', 'required', array('required' => 'Jurusan Harus di Isi'));
		$v->set_rules('no_hp', 'no_hp', 'required|numeric', array('required' => 'No Hp harus diisi'));
		// $v->set_rules('status_anggota', 'Status Anggota', 'required', array('required' => 'Status Anggota Harus di Isi'));
		$v->set_rules('id_divisi', 'Divisi', 'required', array('required' => 'Divisi Harus di Isi'));
		$v->set_rules('id_jabatan', 'Jabatan', 'required', array('required' => 'Jabatan Harus di Isi'));
		$v->set_rules('alamat_anggota', 'Alamat Anggota', 'required', array('required' => 'Alamat Harus di Isi'));

		if ($v->run()) {

			$config['upload_path'] 		= './assets/upload/image/original/anggota/';  //lokasi folder upload
			$config['allowed_types'] 	= 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
			$config['max_size']			= '12000'; // KB	
			$config['max_width']            = 400;
			$config['max_height']           = 400;
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('gambar')) {
				$data = array(
					'title'  => 'Halaman Tambah Anggota',
					'isi  '  => 'admin_v2/anggota/list',
					'divisi' =>  $divisi,
					'jabatan' => $jabatan,
					'site_config' => $site_config,
					'user_login' => $user_login,
					'error'  =>  $this->upload->display_errors()
				);
				// $this->load->view('admin_v2/layout/wrapper', $data);
				$this->load->view('admin_v2/layout/head', $data);
				$this->load->view('admin_v2/layout/header', $data);
				$this->load->view('admin_v2/layout/nav', $data);
				$this->load->view('admin_v2/anggota/list', $data);
				$this->load->view('admin_v2/layout/footer', $data);

				// Masuk database 
			} else {
				$upload_data				= array('uploads' => $this->upload->data());
				// Image Editor
				$config['image_library']	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/original/anggota/' . $upload_data['uploads']['file_name'];
				$config['new_image'] 		= './assets/upload/image/thumbs/anggota/';
				$config['create_thumb'] 	= TRUE;
				$config['quality'] 			= "100%";
				$config['maintain_ratio'] 	= TRUE;
				$config['width'] 			= 360; // Pixel
				$config['height'] 			= 360; // Pixel
				$config['x_axis'] 			= 0;
				$config['y_axis'] 			= 0;
				$config['thumb_marker'] 	= '';
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$i = $this->input;
				$data = array(
					'nama_anggota'			=> $i->post('nama_anggota'),
					'nim'					=> $i->post('nim'),
					'tahun_masuk_anggota' => $i->post('tahun_masuk_anggota'),
					'tanggal_lahir'			=> $i->post('tanggal_lahir'),
					'jenis_kelamin_anggota' => $i->post('jenis_kelamin_anggota'),
					'email_anggota'			=> $i->post('email_anggota'),
					'jurusan'				=> $i->post('jurusan'),
					'no_hp'					=> $i->post('no_hp'),
					'gambar'				=> $upload_data['uploads']['file_name'],
					'status_anggota'		=> $i->post('status_anggota'),
					'id_divisi'             => $i->post('id_divisi'),
					'id_jabatan'			=> $i->post('id_jabatan'),
					'alamat_anggota'		=> $i->post('alamat_anggota')
				);

				$this->anggota_model->tambah($data);
				$this->session->set_flashdata('status', "<script>
            	$(window).on('load', function() {
                $('#toast-add-anggota').toast('show');
            	});
            	</script>");
				redirect(base_url('admin/anggota'));
			}
		}
		// End masuk database

		$data = array(
			'title' => 'Halaman Anggota',
			'isi' => 'admin_v2/anggota/list',
			'anggota' => $anggota,
			'divisi'  => $divisi,
			'jabatan' => $jabatan,
			'site_config' => $site_config,
			'user_login' => $user_login
		);
		// $this->load->view('admin_v2/layout/wrapper', $data);

		$this->load->view('admin_v2/layout/head', $data);
		$this->load->view('admin_v2/layout/header', $data);
		$this->load->view('admin_v2/layout/nav', $data);
		$this->load->view('admin_v2/anggota/list', $data);
		$this->load->view('admin_v2/layout/footer', $data);
	}

	public function detail($id_anggota)
	{
		$anggota = $this->anggota_model->detail($id_anggota);
		$data = array(
			'title' => 'Halaman Profil',
			'isi'	=> 'admin_v2/anggota/detail',
			'anggota'	=> $anggota
		);
		$this->load->view('admin_v2/layout/wrapper', $data);
	}

	public function update($id_anggota)
	{
		// user login
		$id_user = $this->session->userdata('id');
		$user_login = $this->user_model->detail($id_user);

		$site_config = $this->konfigurasi_model->listing();
		$divisi = $this->divisi_model->listing();
		$anggota = $this->anggota_model->detail($id_anggota);
		$jabatan = $this->jabatan_model->listing();

		//validasi
		$v = $this->form_validation;
		$v->set_rules('nama_anggota', 'Nama Anggota', 'required', array('required' => 'Nama Harus diisi'));
		$v->set_rules('nim', 'NIM', 'required', array('required' => 'Nim Harus di Isi', 'is_unique' => 'NIM <strong>' . $this->input->post('nim') . '</strong> sudah digunakan'));
		$v->set_rules('tahun_masuk_anggota', 'Tahun Masuk Anggota', 'required|numeric', array('required' => 'Tahun Masuk Anggota Harus di Isi'));
		// $v->set_rules('jenis_kelamin_anggota', 'Jenis Kelamin', 'required', array('required' => 'Jenis Kelamin harus diisi'));
		$v->set_rules('email_anggota', 'Email Anggota', 'required|valid_email', array('required' => 'Email Harus di Isi', 'valid_email' => 'Email tidak valid', 'is_unique' => 'Email <strong>' . $this->input->post('email_anggota') . '</strong> sudah digunakan'));
		$v->set_rules('jurusan', 'Jurusan', 'required', array('required' => 'Jurusan Harus di Isi'));
		$v->set_rules('no_hp', 'no_hp', 'required', array('required' => 'No Hp harus diisi'));
		// $v->set_rules('status_anggota', 'Status Anggota', 'required', array('required' => 'Status Anggota Harus di Isi'));
		$v->set_rules('id_divisi', 'Divisi', 'required', array('required' => 'Divisi Harus di Isi'));
		$v->set_rules('id_jabatan', 'Jabatan', 'required', array('required' => 'Jabatan Harus di Isi'));
		$v->set_rules('alamat_anggota', 'Alamat Anggota', 'required', array('required' => 'Alamat Harus di Isi'));


		if ($v->run()) {
			//kalau ada gambar
			if (!empty($_FILES['gambar']['name'])) {

				$config['upload_path'] 		= './assets/upload/image/original/anggota/';  //lokasi folder upload
				$config['allowed_types'] 	= 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
				$config['max_size']			= '12000'; // KB	
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('gambar')) {
					$data = array(
						'title'   => 'Halaman Edit Anggota',
						'isi  '   => 'admin_v2/anggota/update',
						'divisi'  => $divisi,
						'anggota' => $anggota,
						'jabatan' => $jabatan,
						'site_config' => $site_config,
						'user_login' => $user_login,
						'error'   => $this->upload->display_errors()
					);
					// $this->load->view('admin_v2/layout/wrapper', $data);
					$this->load->view('admin_v2/layout/head', $data);
					$this->load->view('admin_v2/layout/header', $data);
					$this->load->view('admin_v2/layout/nav', $data);
					$this->load->view('admin_v2/anggota/update', $data);
					$this->load->view('admin_v2/layout/footer', $data);

					// Masuk database 
				} else {
					$upload_data				= array('uploads' => $this->upload->data());
					// Image Editor
					$config['image_library']	= 'gd2';
					$config['source_image'] 	= './assets/upload/image/original/anggota/' . $upload_data['uploads']['file_name'];
					$config['new_image'] 		= './assets/upload/image/thumbs/anggota/';
					$config['create_thumb'] 	= TRUE;
					$config['quality'] 			= "100%";
					$config['maintain_ratio'] 	= TRUE;
					$config['width'] 			= 360; // Pixel
					$config['height'] 			= 360; // Pixel
					$config['x_axis'] 			= 0;
					$config['y_axis'] 			= 0;
					$config['thumb_marker'] 	= '';
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					//hapus gambar lama

					if ($anggota->gambar != "") {
						unlink('./assets/upload/image/original/anggota/' . $anggota->gambar);
						unlink('./assets/upload/image/thumbs/anggota/' . $anggota->gambar);
					}

					//end hapus gambar lama

					$i = $this->input;
					$data = array(
						'id_anggota'			=> $id_anggota,
						'nama_anggota'			=> $i->post('nama_anggota'),
						'nim'					=> $i->post('nim'),
						'gambar'				=> $upload_data['uploads']['file_name'],
						'tahun_masuk_anggota' => $i->post('tahun_masuk_anggota'),
						'tanggal_lahir'			=> $i->post('tanggal_lahir'),
						'alamat_anggota'		=> $i->post('alamat_anggota'),
						'no_hp'					=> $i->post('no_hp'),
						'jurusan'				=> $i->post('jurusan'),
						'id_jabatan'			=> $i->post('id_jabatan'),
						'status_anggota'		=> $i->post('status_anggota'),
						'email_anggota'			=> $i->post('email_anggota'),
						'jenis_kelamin_anggota' => $i->post('jenis_kelamin_anggota'),
						'id_divisi'             => $i->post('id_divisi')
					);

					$this->anggota_model->update($data);
					$this->session->set_flashdata('status', "<script>
					$(window).on('load', function() {
					$('#toast-edit-anggota').toast('show');
					});
					</script>");
					redirect(base_url('admin/anggota'));
				}
			} else {
				//tanpa update gambar
				$i = $this->input;
				$data = array(
					'id_anggota'			=> $id_anggota,
					'nama_anggota'			=> $i->post('nama_anggota'),
					'nim'					=> $i->post('nim'),
					'tanggal_lahir'			=> $i->post('tanggal_lahir'),
					'tahun_masuk_anggota'   => $i->post('tahun_masuk_anggota'),
					'alamat_anggota'		=> $i->post('alamat_anggota'),
					'no_hp'					=> $i->post('no_hp'),
					'jurusan'				=> $i->post('jurusan'),
					'id_jabatan'			=> $i->post('id_jabatan'),
					'status_anggota'		=> $i->post('status_anggota'),
					'email_anggota'			=> $i->post('email_anggota'),
					'jenis_kelamin_anggota' => $i->post('jenis_kelamin_anggota'),
					'id_divisi'             => $i->post('id_divisi')
				);

				$this->anggota_model->update($data);
				$this->session->set_flashdata('status', "<script>
				$(window).on('load', function() {
				$('#toast-edit-anggota').toast('show');
				});
				</script>");
				redirect('admin/anggota');
			}
		}
		// End masuk database

		$data = array(
			'title'   => 'Halaman Edit Anggota',
			'isi'     => 'admin_v2/anggota/update',
			'divisi'  => $divisi,
			'jabatan' => $jabatan,
			'site_config' => $site_config,
			'user_login' => $user_login,
			'anggota' => $anggota
		);

		// $this->load->view('admin_v2/layout/wrapper', $data);
		$this->load->view('admin_v2/layout/head', $data);
		$this->load->view('admin_v2/layout/header', $data);
		$this->load->view('admin_v2/layout/nav', $data);
		$this->load->view('admin_v2/anggota/update', $data);
		$this->load->view('admin_v2/layout/footer', $data);
	}

	public function delete($id_anggota)
	{
		$anggota = $this->anggota_model->detail($id_anggota);

		//hapus gambar
		if ($anggota->gambar != "") {
			unlink('./assets/upload/image/original/anggota/' . $anggota->gambar);
			unlink('./assets/upload/image/thumbs/anggota/' . $anggota->gambar);
		}
		//end hapus gambar

		$data = array('id_anggota' => $id_anggota);
		$this->anggota_model->delete($data);
		$this->session->set_flashdata('status', "<script>
		$(window).on('load', function() {
		$('#toast-delete-anggota').toast('show');
		});
		</script>");
		redirect('admin/anggota', 'refresh');
	}

	public function check_nim()
	{
		$check_nim_from_input = $this->db->get_where('anggota', array(
			'nim' => $this->input->post('nim')
		))->row();

		if ($check_nim_from_input) {
			echo json_encode(array(
				'status' 	=> 0,
				'nim'		=> $this->input->post('nim'),
				'message' 	=> 'sudah dipakai'
			));
		} else {
			echo json_encode(array(
				'status' 	=> 1,
				'nim'		=> $this->input->post('nim'),
				'message' 	=> 'belum dipakai'
			));
		}
	}
}

/* End of file anggota.php */
/* Location: ./application/controllers/admin/anggota.php */
