<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembina extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pembina_model');
    }

    public function index()
    {

        $i = $this->input;
        $v = $this->form_validation;
        $pembina = $this->Pembina_model->admin_listing();

        $v->set_rules('nama_pembina', 'Nama Pembina', 'required', array(
            'required' => 'Nama Pembina tidak boleh kosong'
        ));

        if ($v->run()) {
            $config['upload_path']         = './assets/upload/image/original/pembina/';  //lokasi folder upload
            $config['allowed_types']     = 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
            $config['max_size']            = '12000'; // KB	
            $config['max_width']            = 800;
            $config['max_height']           = 800;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('gambar_pembina')) {
                $data = array(
                    'title' => 'Halaman Pembina',
                    'isi'   => 'admin_v2/pembina/list',
                    'pembina' => $pembina,
                    'error' => $this->upload->display_errors()
                );
                $this->load->view('admin_v2/layout/wrapper', $data);

                // Masuk database 
            } else {
                $upload_data                = array('uploads' => $this->upload->data());
                // Image Editor
                $config['image_library']    = 'gd2';
                $config['source_image']     = './assets/upload/image/original/pembina/' . $upload_data['uploads']['file_name'];
                $config['new_image']         = './assets/upload/image/thumbs/pembina/';
                $config['create_thumb']     = TRUE;
                $config['quality']             = "100%";
                $config['maintain_ratio']     = TRUE;
                $config['width']             = 360; // Pixel
                $config['height']             = 360; // Pixel
                $config['x_axis']             = 0;
                $config['y_axis']             = 0;
                $config['thumb_marker']     = '';
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $data = array(
                    'nama_pembina' => $i->post('nama_pembina'),
                    'dosen'        => $i->post('dosen'),
                    'email_pembina' => $i->post('email_pembina'),
                    'jenis_pembina' => $i->post('jenis_pembina'),
                    'status_pembina' => $i->post('status_pembina'),
                    'gambar_pembina' => $upload_data['uploads']['file_name']
                );

                $this->db->insert('pembina', $data);
                $this->session->set_flashdata('status', "<script>
            	$(window).on('load', function() {
                $('#toast-add-pembina').toast('show');
            	});
            	</script>");
                redirect(base_url('admin/pembina'));
            }
        } else {
            $data = array(
                'title' => 'Halaman Pembina',
                'isi'   => 'admin_v2/pembina/list',
                'pembina' => $pembina
            );
            $this->load->view('admin_v2/layout/wrapper', $data);
        }
    }

    public function update($id_pembina)
    {
        $i = $this->input;
        $pembina = $this->Pembina_model->admin_listing();

        $config['upload_path']         = './assets/upload/image/original/pembina/';  //lokasi folder upload
        $config['allowed_types']     = 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
        $config['max_size']            = '12000'; // KB	
        $config['max_width']            = 800;
        $config['max_height']           = 800;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('gambar_pembina')) {
            $data = array(
                'title' => 'Halaman Pembina',
                'isi'   => 'admin_v2/pembina/list',
                'pembina' => $pembina,
                'error' => $this->upload->display_errors()
            );
            $this->session->set_flashdata('status', "<script>
            $(window).on('load', function() {
            $('#editPembinaModal').modal('show');
            });
            </script>");
            $this->session->set_flashdata('id_pembina', $id_pembina);
            $this->load->view('admin_v2/layout/wrapper', $data);

            // Masuk database 
        } else {
            $upload_data                = array('uploads' => $this->upload->data());
            // Image Editor
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/upload/image/original/pembina/' . $upload_data['uploads']['file_name'];
            $config['new_image']         = './assets/upload/image/thumbs/pembina/';
            $config['create_thumb']     = TRUE;
            $config['quality']             = "100%";
            $config['maintain_ratio']     = TRUE;
            $config['width']             = 360; // Pixel
            $config['height']             = 360; // Pixel
            $config['x_axis']             = 0;
            $config['y_axis']             = 0;
            $config['thumb_marker']     = '';
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            $data = array(
                'nama_pembina' => $i->post('nama_pembina'),
                'dosen'        => $i->post('dosen'),
                'email_pembina' => $i->post('email_pembina'),
                'jenis_pembina' => $i->post('jenis_pembina'),
                'status_pembina' => $i->post('status_pembina'),
                'gambar_pembina' => $upload_data['uploads']['file_name']
            );

            $this->db->where('id_pembina', $id_pembina);
            $this->db->update('pembina', $data);
            $this->session->set_flashdata('status', "<script>
            $(window).on('load', function() {
            $('#toast-edit-pembina').toast('show');
            });
            </script>");
            redirect(base_url('admin/pembina'));
        }
    }

    // AJAX REQUEST
    public function get_data_pembina()
    {
        echo json_encode($this->db->get_where('pembina', array(
            'id_pembina' =>  $this->input->post('id')
        ))->row_array());
    }
}
