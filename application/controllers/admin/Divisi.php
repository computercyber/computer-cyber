<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Divisi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('divisi_model');
		$this->load->model('konfigurasi_model');
	}

	public function index()
	{
		$divisi = $this->divisi_model->listing();

		// user login
		$id_user = $this->session->userdata('id');
		$user_login = $this->user_model->detail($id_user);
		$site_config = $this->konfigurasi_model->listing();

		$data = array(
			'title' => 'Halaman Divisi',
			'isi'	=> 'admin_v2/divisi/list',
			'site_config' => $site_config,
			'user_login' => $user_login,
			'divisi' => $divisi
		);
		// $this->load->view('admin_v2/layout/wrapper', $data);
		$this->load->view('admin_v2/layout/head', $data);
		$this->load->view('admin_v2/layout/header', $data);
		$this->load->view('admin_v2/layout/nav', $data);
		$this->load->view('admin_v2/divisi/list', $data);
		$this->load->view('admin_v2/layout/footer', $data);
	}

	public function add()
	{
		// user login
		$id_user = $this->session->userdata('id');
		$user_login = $this->user_model->detail($id_user);
		$site_config = $this->konfigurasi_model->listing();

		//validasi
		$v = $this->form_validation;
		$v->set_rules('nama_divisi', 'Nama Divisi', 'required|is_unique[divisi.nama_divisi]', array(
			'required' => 'Nama Divisi harus diisi',
			'is_unique' => 'Nama Divisi <strong>' . $this->input->post('nama_divisi') . '</strong> sudah digunakan'
		));
		$v->set_rules('keterangan_divisi', 'Keterangan Divisi', 'required', array('required' => 'Keterangan Divisi Harus diisi'));

		if ($v->run()) {

			$config['upload_path'] 		= './assets/upload/image/';  //lokasi folder upload
			$config['allowed_types'] 	= 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
			$config['max_size']			= '12000'; // KB	
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('gambar_divisi')) {
				$data = array(
					'title' 	=> 'Halaman Tambah Divisi',
					'isi' 		=> 'admin_v2/divisi/tambah',
					'site_config' => $site_config,
					'user_login' => $user_login,
					'error' 	=>  $this->upload->display_errors()
				);
				// $this->load->view('admin_v2/layout/wrapper', $data);
				$this->load->view('admin_v2/layout/head', $data);
				$this->load->view('admin_v2/layout/header', $data);
				$this->load->view('admin_v2/layout/nav', $data);
				$this->load->view('admin_v2/divisi/tambah', $data);
				$this->load->view('admin_v2/layout/footer', $data);

				// Masuk database 
			} else {
				$upload_data				= array('uploads' => $this->upload->data());
				// Image Editor
				$config['image_library']	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/original/divisi/' . $upload_data['uploads']['file_name'];
				$config['new_image'] 		= './assets/upload/image/thumbs/divisi/';
				$config['create_thumb'] 	= TRUE;
				$config['quality'] 			= "100%";
				$config['maintain_ratio'] 	= TRUE;
				$config['width'] 			= 360; // Pixel
				$config['height'] 			= 360; // Pixel
				$config['x_axis'] 			= 0;
				$config['y_axis'] 			= 0;
				$config['thumb_marker'] 	= '';
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$i = $this->input;
				$data = array(
					'nama_divisi'			=> $i->post('nama_divisi'),
					'keterangan_divisi'		=> $i->post('keterangan_divisi'),
					'gambar_divisi'			=> $upload_data['uploads']['file_name'],
				);

				$this->divisi_model->add($data);
				$this->session->set_flashdata('status', "<script>
            	$(window).on('load', function() {
                $('#toast-add-divisi').toast('show');
            	});
            	</script>");
				redirect(base_url('admin/divisi'));
			}
		}
		// End masuk database

		$data = array(
			'title' => 'Halaman Tambah Divisi',
			'isi'   => 'admin_v2/divisi/tambah',
			'site_config' => $site_config,
			'user_login' => $user_login,
		);
		// $this->load->view('admin_v2/layout/wrapper', $data);
		$this->load->view('admin_v2/layout/head', $data);
		$this->load->view('admin_v2/layout/header', $data);
		$this->load->view('admin_v2/layout/nav', $data);
		$this->load->view('admin_v2/divisi/tambah', $data);
		$this->load->view('admin_v2/layout/footer', $data);
	}

	public function update($id_divisi)
	{
		$divisi = $this->divisi_model->detail($id_divisi);
		// user login
		$id_user = $this->session->userdata('id');
		$user_login = $this->user_model->detail($id_user);
		$site_config = $this->konfigurasi_model->listing();

		$v = $this->form_validation;
		$v->set_rules('nama_divisi', 'Nama Divisi', 'required', array('required' => 'Nama Divisi harus diisi'));
		$v->set_rules('keterangan_divisi', 'Keterangan Divisi', 'required', array('required' => 'Keterangan Divisi Harus diisi'));

		if ($v->run()) {

			//kalau ada gambar
			if (!empty($_FILES['gambar_divisi']['name'])) {

				$config['upload_path'] 		= './assets/upload/image/original/divisi/';  //lokasi folder upload
				$config['allowed_types'] 	= 'gif|jpg|png|svg|tiff'; //format file yang di-upload
				$config['max_size']			= '12000'; // KB	
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('gambar_divisi')) {
					$data = array(
						'title' 	=> 'Halaman Edit Divisi',
						'isi' 		=> 'admin_v2/divisi/edit',
						'divisi'	=> $divisi,
						'site_config' => $site_config,
						'user_login' => $user_login,
						'error' 	=>  $this->upload->display_errors()
					);
					// $this->load->view('admin_v2/layout/wrapper', $data);
					$this->load->view('admin_v2/layout/head', $data);
					$this->load->view('admin_v2/layout/header', $data);
					$this->load->view('admin_v2/layout/nav', $data);
					$this->load->view('admin_v2/divisi/edit', $data);
					$this->load->view('admin_v2/layout/footer', $data);

					// Masuk database 
				} else {
					$upload_data				= array('uploads' => $this->upload->data());
					// Image Editor
					$config['image_library']	= 'gd2';
					$config['source_image'] 	= './assets/upload/image/original/divisi/' . $upload_data['uploads']['file_name'];
					$config['new_image'] 		= './assets/upload/image/thumbs/divisi';
					$config['create_thumb'] 	= TRUE;
					$config['quality'] 			= "100%";
					$config['maintain_ratio'] 	= TRUE;
					$config['width'] 			= 360; // Pixel
					$config['height'] 			= 360; // Pixel
					$config['x_axis'] 			= 0;
					$config['y_axis'] 			= 0;
					$config['thumb_marker'] 	= '';
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					//hapus gambar
					if ($divisi->gambar_divisi != "") {
						unlink('./assets/upload/image/original/divisi/' . $divisi->gambar_divisi);
						unlink('./assets/upload/image/thumbs/divisi/' . $divisi->gambar_divisi);
					}
					//end hapus gambar

					$i = $this->input;
					$data = array(
						'id_divisi'				=> $id_divisi,
						'nama_divisi'			=> $i->post('nama_divisi'),
						'keterangan_divisi'		=> $i->post('keterangan_divisi'),
						'gambar_divisi'			=> $upload_data['uploads']['file_name'],
					);

					$this->divisi_model->update($data);
					$this->session->set_flashdata('status', "<script>
					$(window).on('load', function() {
					$('#toast-edit-divisi').toast('show');
					});
					</script>");
					redirect(base_url('admin/divisi'));
				}
			} else {
				$i = $this->input;
				$data = array(
					'id_divisi'				=> $id_divisi,
					'nama_divisi'			=> $i->post('nama_divisi'),
					'keterangan_divisi'		=> $i->post('keterangan_divisi')
				);

				$this->divisi_model->update($data);
				$this->session->set_flashdata('status', "<script>
				$(window).on('load', function() {
				$('#toast-edit-divisi').toast('show');
				});
				</script>");
				redirect(base_url('admin/divisi'));
			}
		}
		// End masuk database

		$data = array(
			'title' => 'Halaman Edit Divisi',
			'isi'	=> 'admin_v2/divisi/edit',
			'divisi' => $divisi,
			'site_config' => $site_config,
			'user_login' => $user_login
		);
		// $this->load->view('admin_v2/layout/wrapper', $data);
		$this->load->view('admin_v2/layout/head', $data);
		$this->load->view('admin_v2/layout/header', $data);
		$this->load->view('admin_v2/layout/nav', $data);
		$this->load->view('admin_v2/divisi/edit', $data);
		$this->load->view('admin_v2/layout/footer', $data);
	}

	public function delete($id_divisi)
	{
		$divisi = $this->divisi_model->detail($id_divisi);

		//hapus gambar
		if ($divisi->gambar_divisi != "") {
			unlink('./assets/upload/image/original/divisi/' . $divisi->gambar_divisi);
			unlink('./assets/upload/image/thumbs/divisi/' . $divisi->gambar_divisi);
		}
		//end hapus gambar


		$data = array('id_divisi' => $id_divisi);
		$this->divisi_model->delete($data);

		$this->session->set_flashdata('status', "<script>
		$(window).on('load', function() {
		$('#toast-delete-divisi').toast('show');
		});
		</script>");
		redirect('admin/divisi', 'refresh');
	}
}

/* End of file divisi.php */
/* Location: ./application/controllers/admin/divisi.php */
