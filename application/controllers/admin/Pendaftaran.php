<?php
defined('BASEPATH') or exit('No direct script access allowed');

// Load library phpspreadsheet
require('./excel/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// End load library phpspreadsheet


class Pendaftaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pendaftaran_model');
    }

    public function index()
    {
        $pendaftaran = $this->Pendaftaran_model->listing();

        $data = array('title' => 'Halaman List Pendaftar', 'isi' => 'admin/pendaftaran/list', 'pendaftaran' => $pendaftaran);
        $this->load->view('admin_v2/layout/wrapper', $data);
    }

    public function konfigurasi()
    {
        $pendaftaran = $this->Pendaftaran_model->listing();
        $konfigurasi_pendaftaran = $this->Pendaftaran_model->getConfigRegister()->row_array();

        $this->form_validation->set_rules('tanggal_pendaftaran_mulai', 'Tanggal Pendaftaran', 'required', array('required' => 'Tanggal Pendafataran Mulai harus diisi'));
        $this->form_validation->set_rules('tanggal_pendaftaran_selesai', 'Tanggal Pendaftaran', 'required', array('required' => 'Tanggal Pendafataran selesai harus diisi'));

        $this->form_validation->set_rules('tanggal_pengumuman_mulai', 'Tanggal Pengumuman', 'required', array('required' => 'Tanggal Pengumuman Mulai harus diisi'));
        $this->form_validation->set_rules('tanggal_pengumuman_selesai', 'Tanggal Pengumuman', 'required', array('required' => 'Tanggal Pengumuman selesai harus diisi'));


        $type = htmlspecialchars($this->input->post('type', TRUE));

        echo $type;
        // die;

        if ($this->form_validation->run()) {


            if ($type == 'pendaftaran') {
                $tanggal_pendaftaran_mulai = htmlspecialchars($this->input->post('tanggal_pendaftaran_mulai', TRUE));
                $tanggal_pendaftaran_selesai = htmlspecialchars($this->input->post('tanggal_pendaftaran_selesai', TRUE));

                echo $type;
                die;
            } elseif ($type == 'pengumuman') {
                $tanggal_pengumuman_mulai = htmlspecialchars($this->input->post('tanggal_pengumuman_mulai', TRUE));
                $tanggal_pengumuman_selesai = htmlspecialchars($this->input->post('tanggal_pengumuman_selesai', TRUE));

                echo $type;
                die;
            }
        } else {
            $data = array(
                'title' => 'Konfigurasi Web Pendaftaran',
                'isi' => 'admin_v2/pendaftaran/konfigurasi',
                'pendaftaran' => $pendaftaran,
                'konfigurasi_pendaftaran' => $konfigurasi_pendaftaran
            );
            $this->load->view('admin_v2/layout/wrapper', $data);
        }
    }

    public function resetData()
    {
        $pendaftaran = $this->Pendaftaran_model->resetData();
        $this->session->set_flashdata('sukses', 'Data Berhasil di Reset');
        redirect('admin/pendaftaran');
    }

    public function detail($id_peserta)
    {
        $pendaftaran = $this->Pendaftaran_model->detail($id_peserta);
        $data = array(
            'title' => 'Halaman Detail Pendaftar',
            'isi' => 'admin/pendaftaran/detail',
            'pendaftaran' => $pendaftaran
        );
        $this->load->view('admin/layout/wrapper', $data);
    }

    public function delete($id_peserta)
    {
        $pendaftaran = $this->Pendaftaran_model->delete($id_peserta);

        //hapus gambar
        // if ($pendaftaran->gambar != "") {
        //     unlink('./assets/upload/image/' . $anggota->gambar);
        // }
        //end hapus gambar

        $data = array('id_peserta' => $id_peserta);
        $this->Pendaftaran_model->delete($data);
        $this->session->set_flashdata('hapus_anggota', 'Anggota telah dihapus');
        redirect('admin/pendaftaran');
    }

    public function export_excel()
    {
        $data['calon_anggota'] = $this->Pendaftaran_model->getDataExport('calon_anggota')->result();

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Computer Cyber')
            ->setLastModifiedBy('Computer Cyber')
            ->setTitle('List Pendaftar Anggota Baru Computer Cyber 2019')
            ->setSubject('Pendaftar')
            ->setDescription('List Pendaftar Anggota Baru Computer Cyber 2019')
            ->setKeywords('')
            ->setCategory('list');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('B1', 'NAMA')
            ->setCellValue('C1', 'NIM')
            ->setCellValue('D1', 'E-MAIL')
            ->setCellValue('E1', 'NO HP')
            ->setCellValue('F1', 'JENIS KELAMIN')
            ->setCellValue('G1', 'TANGGAL_LAHIR')
            ->setCellValue('H1', 'JURUSAN')
            ->setCellValue('I1', 'PENGALAMAN ORGANISASI')
            ->setCellValue('J1', 'ALAMAT')
            ->setCellValue('K1', 'ALASAN MASUK')
            ->setCellValue('L1', 'DIVISI TUJUAN')
            ->setCellValue('M1', 'TANGGAL DAFTAR');

        $i = 2;
        $no = 1;

        foreach ($data['calon_anggota'] as $a) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $no++)
                ->setCellValue('B' . $i, $a->nama)
                ->setCellValue('C' . $i, $a->nim)
                ->setCellValue('D' . $i, $a->email)
                ->setCellValue('E' . $i, $a->jenis_kelamin)
                ->setCellValue('F' . $i, $a->tanggal_lahir)
                ->setCellValue('G' . $i, $a->jurusan)
                ->setCellValue('H' . $i, $a->pengalaman_organisasi)
                ->setCellValue('I' . $i, $a->alamat)
                ->setCellValue('J' . $i, $a->alasan_masuk)
                ->setCellValue('K' . $i, $a->divisi)
                ->setCellValue('L' . $i, $p->date_created);

            $i++;

            $spreadsheet->getActiveSheet()->setTitle('List Pendaftar Anggota Baru Computer Cyber 2019 ' . date('d-m-Y H'));

            $spreadsheet->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');

            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
            exit;
        }
    }
}
