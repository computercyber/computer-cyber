z<?php
	defined('BASEPATH') or exit('No direct script access allowed');

	class Gallery extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();
			$this->load->model('gallery_model');
		}

		public function data_gallery()
		{
			$data = $this->gallery_model->listing();
			echo json_encode($data);
		}

		public function get_gallery()
		{
			$data = $this->gallery_model->listing();
			echo json_encode($data);
		}

		public function index()
		{
			$gallery = $this->gallery_model->admin_listing();
			$v = $this->form_validation;
			$v->set_rules('judul_gallery', 'Judul Gambar', 'required', array('required' => 'Judul gambar harus diisi'));
			$v->set_rules('status_gallery', 'Status Gambar', 'required', array('required' => 'Status gambar harus diisi'));
			$v->set_rules('keterangan_gallery', 'Keterangan Gambar', 'required', array('required' => 'Keterangan harus diisi'));


			if ($v->run()) {

				$config['upload_path'] 		= './assets/upload/image/original/gallery/';  //lokasi folder upload
				$config['allowed_types'] 	= 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
				$config['max_size']			= '12000'; // KB	
				$config['max_width']            = 1366;
				$config['max_height']           = 768;
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('gambar')) {
					$data = array(
						'title' 	=> 'Halaman Tambah Gambar',
						'isi' 		=> 'admin_v2/gallery/list',
						'gallery' 	=> $gallery,
						'error' 	=>  $this->upload->display_errors()
					);
					$this->load->view('admin_v2/layout/wrapper', $data);

					// Masuk database 
				} else {
					$upload_data				= array('uploads' => $this->upload->data());
					// Image Editor
					$config['image_library']	= 'gd2';
					$config['source_image'] 	= './assets/upload/image/original/gallery/' . $upload_data['uploads']['file_name'];
					$config['new_image'] 		= './assets/upload/image/thumbs/gallery/';
					$config['create_thumb'] 	= TRUE;
					$config['quality'] 			= "100%";
					$config['maintain_ratio'] 	= TRUE;
					$config['width'] 			= 360; // Pixel
					$config['height'] 			= 360; // Pixel
					$config['x_axis'] 			= 0;
					$config['y_axis'] 			= 0;
					$config['thumb_marker'] 	= '';
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$i = $this->input;
					$data = array(
						'judul_gallery'			=> $i->post('judul_gallery'),
						'keterangan_gallery'	=> $i->post('keterangan_gallery'),
						'gambar'				=> $upload_data['uploads']['file_name'],
						'tanggal_gallery'		=> $i->post('tanggal_gallery'),
						'status_gallery'		=> $i->post('status_gallery'),
						'id_user'				=> $this->session->userdata('id'),
					);

					$this->gallery_model->add($data);
					$this->session->set_flashdata('status', "<script>
            	$(window).on('load', function() {
                $('#toast-add-gallery').toast('show');
            	});
            	</script>");
					redirect(base_url('admin/gallery'));
				}
			}
			// End masuk database

			$data = array(
				'title' => 'Halaman Gallery',
				'isi'	=> 'admin_v2/gallery/list',
				'gallery' => $gallery
			);
			$this->load->view('admin_v2/layout/wrapper', $data);
		}

		public function add()
		{
			//validasi
			$v = $this->form_validation;
			$v->set_rules('judul_gallery', 'Judul Gambar', 'required', array('required' => 'Judul gambar harus diisi'));
			$v->set_rules('keterangan_gallery', 'Keterangan Gambar', 'required', array('required' => 'Keterangan harus diisi'));

			if ($v->run()) {

				$config['upload_path'] 		= './assets/upload/image/original/gallery/';  //lokasi folder upload
				$config['allowed_types'] 	= 'gif|jpg|png|svg|tiff'; //format file yang di-upload
				$config['max_size']			= '12000'; // KB	
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('gambar')) {
					$data = array(
						'title' 	=> 'Halaman Tambah Gambar',
						'isi' 		=> 'admin_v2/gallery/tambah',
						'error' 	=>  $this->upload->display_errors()
					);
					$this->load->view('admin/layout/wrapper', $data);

					// Masuk database 
				} else {
					$upload_data				= array('uploads' => $this->upload->data());
					// Image Editor
					$config['image_library']	= 'gd2';
					$config['source_image'] 	= './assets/upload/image/original/gallery/' . $upload_data['uploads']['file_name'];
					$config['new_image'] 		= './assets/upload/image/thumbs/gallery/';
					$config['create_thumb'] 	= TRUE;
					$config['quality'] 			= "100%";
					$config['maintain_ratio'] 	= TRUE;
					$config['width'] 			= 360; // Pixel
					$config['height'] 			= 360; // Pixel
					$config['x_axis'] 			= 0;
					$config['y_axis'] 			= 0;
					$config['thumb_marker'] 	= '';
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$i = $this->input;
					$data = array(
						'judul_gallery'			=> $i->post('judul_gallery'),
						'keterangan_gallery'	=> $i->post('keterangan_gallery'),
						'gambar'				=> $upload_data['uploads']['file_name'],
						'tanggal_gallery'		=> $i->post('tanggal_gallery'),
						'status_gallery'		=> $i->post('status_gallery'),
						'id_user'				=> $this->session->userdata('id'),
					);

					$this->gallery_model->add($data);
					$this->session->set_flashdata('sukses', 'Gambar telah ditambah');
					redirect(base_url('admin/gallery'));
				}
			}
			// End masuk database


			$data = array(
				'title' => 'Halaman Tambah Gambar',
				'isi'	=> 'admin_v2/gallery/tambah'
			);
			$this->load->view('admin_v2/layout/wrapper', $data);
		}

		public function update($id_gallery)
		{
			$gallery = $this->gallery_model->detail($id_gallery);

			//validasi
			$v = $this->form_validation;
			$v->set_rules('judul_gallery', 'Judul Gambar', 'required', array('required' => 'Judul gambar harus diisi'));
			$v->set_rules('keterangan_gallery', 'Keterangan Gambar', 'required', array('required' => 'Keterangan harus diisi'));

			if ($v->run()) {

				//kalau ada gambar
				if (!empty($_FILES['gambar']['name'])) {

					$config['upload_path'] 		= './assets/upload/image/original/gallery/';  //lokasi folder upload
					$config['allowed_types'] 	= 'gif|jpg|jpeg|png|svg|tiff'; //format file yang di-upload
					$config['max_size']			= '12000'; // KB	
					$config['max_width']            = 1366;
					$config['max_height']           = 768;
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('gambar')) {
						$data = array(
							'title' 	=> 'Halaman Edit Gambar',
							'isi' 		=> 'admin_v2/gallery/edit',
							'gallery'	=> $gallery,
							'error' 	=>  $this->upload->display_errors()
						);
						$this->load->view('admin_v2/layout/wrapper', $data);

						// Masuk database 
					} else {
						$upload_data				= array('uploads' => $this->upload->data());
						// Image Editor
						$config['image_library']	= 'gd2';
						$config['source_image'] 	= './assets/upload/image/original/gallery/' . $upload_data['uploads']['file_name'];
						$config['new_image'] 		= './assets/upload/image/thumbs/gallery/';
						$config['create_thumb'] 	= TRUE;
						$config['quality'] 			= "100%";
						$config['maintain_ratio'] 	= TRUE;
						$config['width'] 			= 360; // Pixel
						$config['height'] 			= 360; // Pixel
						$config['x_axis'] 			= 0;
						$config['y_axis'] 			= 0;
						$config['thumb_marker'] 	= '';
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();

						//hapus gambar
						if ($gallery->gambar != "") {
							unlink('./assets/upload/image/original/gallery/' . $gallery->gambar);
							unlink('./assets/upload/image/thumbs/gallery/' . $gallery->gambar);
						}
						//end hapus gambar

						$i = $this->input;
						$data = array(
							'id_gallery'			=> $id_gallery,
							'judul_gallery'			=> $i->post('judul_gallery'),
							'keterangan_gallery'	=> $i->post('keterangan_gallery'),
							'gambar'				=> $upload_data['uploads']['file_name'],
							'tanggal_gallery'		=> $i->post('tanggal_gallery'),
							'status_gallery'		=> $i->post('status_gallery'),
							'id_user'				=> $this->session->userdata('id'),
						);

						$this->gallery_model->update($data);
						$this->session->set_flashdata('sukses', 'Gambar telah diedit');
						redirect(base_url('admin/gallery'));
					}
				} else {
					//tanpa ganti gambar
					$i = $this->input;
					$data = array(
						'id_gallery'			=> $id_gallery,
						'judul_gallery'			=> $i->post('judul_gallery'),
						'keterangan_gallery'	=> $i->post('keterangan_gallery'),
						'tanggal_gallery'		=> $i->post('tanggal_gallery'),
						'status_gallery'		=> $i->post('status_gallery'),
						'id_user'				=> $this->session->userdata('id'),
					);

					$this->gallery_model->update($data);
					$this->session->set_flashdata('status', "<script>
            	$(window).on('load', function() {
                $('#toast-edit-gallery').toast('show');
            	});
            	</script>");
					redirect(base_url('admin/gallery'));
				}
			}
			// End masuk database

			$data = array(
				'title'  => 'Halaman Edit Gambar',
				'isi'	 => 'admin_v2/gallery/edit',
				'gallery' => $gallery
			);
			$this->load->view('admin_v2/layout/wrapper', $data);
		}

		public function delete($id_gallery)
		{
			$gallery = $this->gallery_model->detail($id_gallery);

			//hapus gambar
			if ($gallery->gambar != "") {
				unlink('./assets/upload/image/original/gallery/' . $gallery->gambar);
				unlink('./assets/upload/image/thumbs/gallery/' . $gallery->gambar);
			}
			//end hapus gambar

			$data = array('id_gallery' => $id_gallery,);
			$this->gallery_model->delete($data);
			$this->session->set_flashdata('sukses', 'Gambar telah dihapus');
			redirect('admin/gallery', 'refresh');
		}
	}

/* End of file gallery.php */
/* Location: ./application/controllers/admin/gallery.php */
