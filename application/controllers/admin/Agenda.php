<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agenda extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Agenda_model');
        $this->load->model('Konfigurasi_model');
    }

    public function index()
    {
        $agenda = $this->Agenda_model->listing();
        $konfigurasi = $this->Konfigurasi_model->listing();

        $this->form_validation->set_rules('nama_agenda', 'Nama agenda', 'required', array('required' => 'Nama agenda harus diisi'));
        $this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'required', array('required' => 'Tanggal mulai harus diisi'));
        $this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'required', array('required' => 'Tanggal selesai harus diisi'));
        $this->form_validation->set_rules('undangan', 'Undangan', 'required', array('required' => 'Undangan yang ditujukan harus diisi'));
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required', array('required' => 'Lokasi kegiatan harus diisi'));

        if ($this->form_validation->run()) {
            $nama_agenda = $this->input->post('nama_agenda');
            $tanggal_mulai = $this->input->post('tanggal_mulai');
            $tanggal_selesai = $this->input->post('tanggal_selesai');
            $undangan = $this->input->post('undangan');
            $lokasi = $this->input->post('lokasi');
            $keterangan = $this->input->post('keterangan');

            // data masuk ke array
            $data = array(
                'nama_agenda'    => $nama_agenda,
                'tanggal_mulai'        => $tanggal_mulai,
                'tanggal_selesai'   => $tanggal_selesai,
                'keterangan'    => $keterangan,
                'undangan'      => $undangan,
                'lokasi'        => $lokasi
            );

            // masuk ke tabel agenda
            $this->Agenda_model->add($data, 'agenda');

            $this->session->set_flashdata('status', "<script>
            $(window).on('load', function() {
                $('#toast-add-agenda').toast('show');
            });
            </script>");
            redirect('admin/agenda');
        }

        $data = array(
            'title' => 'Halaman Agenda',
            'isi' => 'admin_v2/agenda/list',
            'agenda' => $agenda,
            'konfigurasi' => $konfigurasi
        );
        $this->load->view('admin_v2/layout/wrapper', $data);
    }

    public function konfigurasi_auto_emailer()
    {
        $this->db->set('auto_emailer_agenda', 1);
        $this->db->where('id_konfigurasi', 1);
        $this->db->update('konfigurasi');

        $this->session->set_flashdata('status', "<script>
        $(window).on('load', function() {
            $('#toast-auto-emailer').toast('show');
        });
        </script>");
        redirect('admin/agenda');
    }

    public function delete($id_agenda)
    {
        // hapus anggota berdasarkan id
        $get_agenda = urldecode(decrypt_url($id_agenda));
        $where = array('id_agenda' => $get_agenda);

        $this->Agenda_model->deleteAgenda($where, 'agenda');

        $this->session->set_flashdata('status', "<script>
        $(window).on('load', function() {
            $('#toast-delete-agenda').toast('show');
        });
        </script>");
        redirect('admin/agenda');
    }

    public function changestatus($type, $id_agenda)
    {
        $get_type = urldecode(decrypt_url($type));
        $get_id_agenda = urldecode(decrypt_url($id_agenda));

        $this->db->where('id_agenda', $get_id_agenda);
        $this->db->set('status', $get_type);
        $this->db->update('agenda');

        $this->session->set_flashdata('status', "<script>
        $(window).on('load', function() {
            $('#toast-edit-agenda').toast('show');
        });
        </script>");
        redirect('admin/agenda');
    }
}
