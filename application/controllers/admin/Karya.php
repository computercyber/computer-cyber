<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karya extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Karya_model');
    }

    public function index()
    {
        $karya = $this->Karya_model->admin_listing();
        $data = array(
            'title' => 'Halaman Karya',
            'isi'    => 'admin_v2/karya/list',
            'karya' => $karya
        );
        $this->load->view('admin_v2/layout/wrapper', $data);
    }

    public function add()
    {

        $karya = $this->Karya_model->admin_listing();
        $data = array(
            'title' => 'Halaman Karya',
            'isi'    => 'admin_v2/karya/add',
            'karya' => $karya
        );
        $this->load->view('admin_v2/layout/wrapper', $data);
    }
}
