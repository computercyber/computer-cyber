// hapus agenda
$('.tombol-hapus-agenda').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// hapus user
$('.tombol-hapus-user').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// hapus keanggotaan
$('.tombol-hapus-anggota').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// hapus pengumuman diterima
$('.tombol-hapus-anggota_diterima').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// reset data calon anggota
$('.tombol-reset-calon_anggota').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin reset data',
        text: "Apa kamu yakin ingin mereset data?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Reset data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// reset data flashdata
// const flashData = $.('flash-data').data('flashdata');
// if (flashData) {
//     Swal.fire({
//         title: 'Data',
//         text: 'Berhasil' + flashData,
//         type: 'success'
//     });
// }

// hapus berita
$('.tombol-hapus-berita').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// hapus gallery
$('.tombol-hapus-gallery').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// hapus divisi
$('.tombol-hapus-divisi').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// hapus jabatan
$('.tombol-hapus-jabatan').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// hapus jabatan
$('.tombol-hapus-calon_anggota').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Yakin ingin menghapus',
        text: "Apa kamu yakin ingin menghapus?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});